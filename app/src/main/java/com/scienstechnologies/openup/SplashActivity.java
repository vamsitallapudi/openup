package com.scienstechnologies.openup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.scienstechnologies.openup.activities.MainActivity;
import com.scienstechnologies.openup.activities.OpenupFirstActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread timerThread = new Thread(){
            public void run(){
                try {
                    sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    SharedPreferences prefs = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
                    boolean haveWeShownPreferences = prefs.getBoolean("HaveShownPrefs", false);
//                    if (prefs.contains("emailId") && prefs.contains("password")) {
//                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
//                        startActivity(i);
//                    }else {
//                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
//                        startActivity(i);
//                    }

                    if (!haveWeShownPreferences) {
                        // launch the preferences activity


                        Intent i = new Intent(SplashActivity.this,OpenupFirstActivity.class);
                        startActivity(i);

                    } else {
                        // we have already shown the preferences activity before
                        Intent i = new Intent(SplashActivity.this,LoginActivity.class);
                        startActivity(i);
                    }



                }
            }
        };
        timerThread.start();
    }
    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
