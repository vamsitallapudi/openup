package com.scienstechnologies.openup.models;

/**
 * Created by Santhu on 3/3/2016.
 */
public class ChatMain {
    private String profile,profile_name,chat_text,footer_name,time,comments,share;
    public ChatMain(){
    }

    public ChatMain(String profile, String profile_name, String chat_text, String footer_name, String time, String comments, String share) {
        this.profile = profile;
        this.profile_name = profile_name;
        this.chat_text = chat_text;
        this.footer_name = footer_name;
        this.time= time;
        this.comments = comments;
        this.share = share;
    }
    public String getProfile(){
        return this.profile;
    }
    public  void setProfile(String profile){
        this.profile = profile;
    }
    public String getProfile_name(){
        return this.profile_name;
    }
    public  void setProfile_name(String profile_name){
        this.profile_name = profile_name;
    }
    public String getChat_text(){
        return this.chat_text;
    }
    public  void setChat_text(String chat_text){
        this.chat_text = chat_text;
    }
    public String getFooter_name(){
        return this.footer_name;
    }
    public  void setFooter_name(String footer_name){
        this.footer_name = footer_name;
    }
    public String getTime(){
        return this.time;
    }
    public  void setTime(String time){
        this.time = time;
    }
    public String getComments(){
        return this.comments;
    }
    public  void setComments(String comments){
        this.comments = comments;
    }
    public String getShare(){
        return this.share;
    }
    public  void setShare(String share){
        this.share = share;
    }
}
