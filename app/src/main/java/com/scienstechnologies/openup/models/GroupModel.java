package com.scienstechnologies.openup.models;

/**
 * Created by vamsi on 21-May-16.
 */
public class GroupModel {

    private String name;
    private String score;
    private String profilePic;
    private String userId;
    private int coin;
    private boolean selected;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public String getUserId(int position) {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
