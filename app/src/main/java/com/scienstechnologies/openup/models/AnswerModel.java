package com.scienstechnologies.openup.models;

/**
 * Created by Sciens on 5/24/2016.
 */
public class AnswerModel {
    private String answer,profilePic,userName,hashtag;

    public AnswerModel(){
    }

    public AnswerModel(String answer,String profilePic,String userName,String hashtag){
        this.answer = answer;
        this.userName = userName;
        this.profilePic = profilePic;
        this.hashtag = hashtag;

    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }
}
