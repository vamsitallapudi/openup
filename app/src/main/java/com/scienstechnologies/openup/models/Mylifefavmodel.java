package com.scienstechnologies.openup.models;

/**
 * Created by Dell on 5/16/2016.
 */
public class Mylifefavmodel {
    private String userName,chat_text,centre_txt,time,comment,likes;
    private int dp,person;


    public Mylifefavmodel(){
    }
    public Mylifefavmodel(String userName,String chat_text,String centre_txt,String time,String comment,String likes,int dp,int person){
        this.userName = userName;this.chat_text = chat_text;this.centre_txt = centre_txt;
        this.time = time;this.comment = comment;this.likes = likes;}

    public String getUserName(){return userName;}
    public String getChat_text(){return chat_text;}
    public String getCentre_txt(){return centre_txt;}
    public String getTime(){return time;}
    public String getComment(){return comment;}
    private String getLikes(){return  likes;}
    public int getDp(){return dp;}
    public int getPerson(){return person;}
    public void setUserName(String userName){this.userName = userName;}
    public void setChat_text(String chat_text){this.chat_text = chat_text;}
    public void setCentre_txt(String centre_txt){this.centre_txt = centre_txt;}
    public void setTime(String time){this.time = time;}
    public void setComment(String comment){this.comment = comment;}
    public void setLikes(String like){this.likes = like;}
    public void setDp(int dp){this.dp = dp;}
    public void setPerson(int person){this.person = person;}

}
