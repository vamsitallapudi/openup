package com.scienstechnologies.openup.models;

/**
 * Created by Sciens on 6/22/2016.
 */
public class CategoriesModel {
    private  String categoryid,name,color,id;
    public  CategoriesModel(){}
    public CategoriesModel(String name,String color,String id,String categoryid){
        this.name = name;
        this.color = color;
        this.id =id;
        this.categoryid =  categoryid;
    }

    public String getName(int position) {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getId(int position) {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
