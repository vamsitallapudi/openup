package com.scienstechnologies.openup.models;

/**
 * Created by Santhu on 6/11/2016.
 */
public class FollowersModel {
    String profilePic,userName;

    public String getProfilePic(){
        return profilePic;
    }
    public String getUserName(){
        return userName;
    }
    public void setProfilePic(String profilePic){
        this.profilePic = profilePic;
    }
    public void setUserName(String userName){
        this.userName = userName;
    }
}
