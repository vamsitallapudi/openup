package com.scienstechnologies.openup.models;

/**
 * Created by Dell on 5/16/2016.
 */
public class NotificationModel  {
    private String image;
    private String notification,userName,time,notificationId,status,postId,userId;

    public NotificationModel(){
    }
    public NotificationModel(String image,String notification,String userName,String time,String notificationId,String status,String postId,String userId){
        this.image = image;
        this.notification = notification;
        this.userName = userName;
        this.time = time;
        this.notificationId = notificationId;
        this.status = status;
        this.postId = postId;
        this.userId = userId;

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNotificationId(int position) {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getStatus(String s) {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus(int position) {
        return status;
    }

    public String getPostId(int position) {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserId(int position) {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
