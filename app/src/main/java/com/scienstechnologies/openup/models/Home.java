package com.scienstechnologies.openup.models;

/**
 * Created by Home on 3/15/2016.
 */
public class Home {
    private String userName,inspire_words,feeling,chat_text,centre_txt,time,comment,share,id;
    private int dp,person,postCategory;
    private String postTypeId;

    public Home(){
    }
    public Home(String userName,String inspire_words,String feeling,String chat_text,String centre_txt,String time,String comment,String share,int dp,int person,String id){
        this.userName = userName;this.inspire_words = inspire_words;this.feeling = feeling;this.chat_text = chat_text;this.centre_txt = centre_txt;
        this.time = time;this.comment = comment;this.share = share;this.id=id;}
    public  String getId(){return  id;}
    public String getUserName(){return userName;}
    public int getPostCategory(){return postCategory;}
    public String getInspire_words(){return inspire_words;}
    public String getFeeling(){return feeling;}
    public String getChat_text(){return chat_text;}
    public String getCentre_txt(){return centre_txt;}
    public String getTime(){return time;}
    public String getComment(){return comment;}
    public String getShare(){return share;}
    public int getDp(){return dp;}
    public int getPerson(){return person;}
    public void setUserName(String userName){this.userName = userName;}
    public void setInspire_words(String inspire_words){this.inspire_words = inspire_words;}
    public void setFeeling(String feeling){this.feeling = feeling;}
    public void setChat_text(String chat_text){this.chat_text = chat_text;}
    public void setCentre_txt(String centre_txt){this.centre_txt = centre_txt;}
    public void setTime(String time){this.time = time;}
    public void setComment(String comment){this.comment = comment;}
    public void setShare(String share){this.share = share;}
    public void setDp(int dp){this.dp = dp;}
    public void setPerson(int person){this.person = person;}
    public void setPostCategory(int postCategory){this.postCategory = postCategory;}
    public void setId(String id){this.id = id;}

    public void setpostTypeId(String postTypeId) {
        this.postTypeId = postTypeId;
    }
}
