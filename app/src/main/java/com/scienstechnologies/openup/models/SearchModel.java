package com.scienstechnologies.openup.models;

/**
 * Created by Sciens on 6/22/2016.
 */
public class SearchModel {
    private String name,profilePic,id,followtype,hashtag;
    public SearchModel(){}
    public  SearchModel(String name,String profilePic,String id,String followtype,String hashtag){
        this.name = name;
        this.profilePic = profilePic;
        this.id = id;
        this.followtype = followtype;
        this.hashtag = hashtag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getId(int position) {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFollowtype(int position) {
        return followtype;
    }

    public void setFollowtype(String followtype) {
        this.followtype = followtype;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }
}
