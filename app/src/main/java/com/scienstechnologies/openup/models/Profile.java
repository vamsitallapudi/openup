package com.scienstechnologies.openup.models;

/**
 * Created by Sciens on 5/16/2016.
 */
public class Profile {
    private String userName,content,subCategory,gold,silver,copper,postTypeId,image,postCategoryId,title,timer,postId,hashtag,subCatName;
    private String profilePic,video,commentCount,likeCount,userId,answer,sharedname,shareType,quoteType;
    private int length,goldLike;
    private boolean selected;
    private boolean favStatus;
    private boolean likeStatus;
    public Profile(){

    }

    public boolean isFavStatus() {
        return favStatus;
    }
    public boolean isLikeStatus() {
        return likeStatus;
    }

    public void setFavStatus(boolean favStatus) {
        this.favStatus = favStatus;
    }
    public void setLikeStatus(boolean likeStatus){
        this.likeStatus = likeStatus;
    }

    public Profile(String userName, String content, String subCategory, String gold, String silver,
                   String copper, String profilePic, String postTypeId, String image, String postCategoryId,
                   String title, String video, String timer, String postId, String likeCount, String commentCount,
                   String userId,String answer,int length,int goldLike,String hashtag,String subCatName,String sharedname,
                   String shareType,String quoteType){
        this.userName = userName;
        this.content = content;
        this.subCategory = subCategory;
        this.profilePic = profilePic;
        this.gold = gold;
        this.silver = silver;
        this.copper = copper;
        this.postTypeId = postTypeId;
        this.image = image;
        this.postCategoryId = postCategoryId;
        this.title = title;
        this.video = video;
        this.timer = timer;
        this.postId = postId;
        this.likeCount = likeCount;
        this.commentCount = commentCount;
        this.userId = userId;
        this.answer = answer;
        this.length = length;
        this.goldLike = goldLike;
        this.hashtag = hashtag;
        this.subCatName = subCatName;
        this.sharedname = sharedname;
        this.shareType = shareType;
        this.quoteType = quoteType;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String contect) {
        this.content = contect;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getGold() {
        return gold;
    }

    public void setGold(String gold) {
        this.gold = gold;
    }

    public String getSilver() {
        return silver;
    }

    public void setSilver(String silver) {
        this.silver = silver;
    }

    public String getCopper() {
        return copper;
    }

    public void setCopper(String copper) {
        this.copper = copper;
    }

    public String getPostTypeId() {
        return postTypeId;
    }

    public void setPostTypeId(String postTypeId) {
        this.postTypeId = postTypeId;
    }


    public String getImage(int position) {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPostCategoryId() {
        return postCategoryId;
    }

    public void setPostCategoryId(String postCategoryId) {
        this.postCategoryId = postCategoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getVideo(int position) {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getPostId(int position) {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getUserId(int position) {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getGoldLike() {
        return goldLike;
    }

    public void setGoldLike(int goldLike) {
        this.goldLike = goldLike;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public String getSharedname() {
        return sharedname;
    }

    public void setSharedname(String sharedname) {
        this.sharedname = sharedname;
    }

    public String getShareType() {
        return shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public String getQuoteType() {
        return quoteType;
    }

    public void setQuoteType(String quoteType) {
        this.quoteType = quoteType;
    }
}
