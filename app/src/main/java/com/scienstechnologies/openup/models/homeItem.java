package com.scienstechnologies.openup.models;

/**
 * Created by vamsi on 17-May-16.
 */
public class HomeItem {

    String postTypeId,postCategory,postSubCategory,senderId,title,content,url;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getPostSubCategory() {
        return postSubCategory;
    }

    public void setPostSubCategory(String postSubCategory) {
        this.postSubCategory = postSubCategory;
    }

    public String getPostCategory() {
        return postCategory;
    }

    public void setPostCategory(String postCategory) {
        this.postCategory = postCategory;
    }

    public String getPostTypeId() {
        return postTypeId;
    }

    public void setPostTypeId(String postTypeId) {
        this.postTypeId = postTypeId;
    }
}
