package com.scienstechnologies.openup.models;

/**
 * Created by Sciens on 5/21/2016.
 */
public class Comments {
    private String profilePic,userName,comment,hashtag;
    public Comments(){
    }
    public Comments(String profilePic,String comment,String userName,String hashtag) {
        this.userName = userName;
        this.profilePic = profilePic;
        this.comment = comment;
        this.hashtag = hashtag;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }
}
