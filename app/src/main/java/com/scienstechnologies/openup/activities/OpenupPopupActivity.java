package com.scienstechnologies.openup.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.scienstechnologies.openup.R;

public class OpenupPopupActivity extends Activity {
    TextView textView;
    String type;
    String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_openup_popup);

        value = "Here we findFriends relevent content text";


        textView = (TextView)findViewById(R.id.tv_help);
        textView.setText(value);
    }
}
