package com.scienstechnologies.openup.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.ImagesAdapter;
import com.scienstechnologies.openup.models.ShareModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdminImagesActivity extends FragmentActivity {
    ProgressDialog mProgressDialog;
    private ListView listView;
    ImagesAdapter imagesAdapter;
    private List<ShareModel> shareModelList = new ArrayList<>();
    String Id;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_images);
        back = (ImageView)findViewById(R.id.iv_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        listView = (ListView) findViewById(R.id.admin_images_list);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);


        if (isNetworkConnected()) {
            mProgressDialog.show();
            String URL = StringConstants.BASEURL+"imagesList";

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            JSONArray array = jsonObject.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                ShareModel shareModel = new ShareModel();
                                try{
                                    shareModel.setImageId(object.getString("url"));
                                }catch (Exception e){

                                }
                                shareModelList.add(shareModel);
                            }
                            imagesAdapter = new ImagesAdapter(AdminImagesActivity.this, shareModelList, getSupportFragmentManager(), Id);
                            listView.setAdapter(imagesAdapter);
                            listView.deferNotifyDataSetChanged();
                            mProgressDialog.dismiss();
                        } else if (status == 0) {
                            Toast.makeText(AdminImagesActivity.this, "null", Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        try {
                            Toast.makeText(AdminImagesActivity.this, "json exception", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            mProgressDialog.dismiss();
                        } catch (Exception e1) {
                        }

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Toast.makeText(AdminImagesActivity.this, "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mProgressDialog.dismiss();

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(AdminImagesActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
