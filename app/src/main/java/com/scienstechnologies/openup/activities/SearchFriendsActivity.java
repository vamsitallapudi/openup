package com.scienstechnologies.openup.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.SearchFriendsAdapter;
import com.scienstechnologies.openup.models.SearchModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by vamsi on 21-Jun-16.
 */

public class SearchFriendsActivity extends Activity {
    List<SearchModel> shareModels = new ArrayList<>();
    EditText etSearchFriends;
    ImageView back;
    String userId, searchKey;
    public static final String URL = StringConstants.BASEURL + "search_friends";
    SearchFriendsAdapter searchFriendsAdapter;
    ListView listView;
    ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_friends);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        etSearchFriends = (EditText) findViewById(R.id.et_search_friends);
        back = (ImageView) findViewById(R.id.back_arrow_iv);
        listView = (ListView) findViewById(R.id.lv_usersList);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        etSearchFriends.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                searchKey = s.toString();
                if (!searchKey.equals("")) {
                    callSearchFunction(searchKey);
                } else {
                    shareModels.clear();
                    searchFriendsAdapter.notifyDataSetChanged();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });


        SharedPreferences sharedPreferences = this.getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");


    }

    private void callSearchFunction(final String searchKey) {
        StringRequest sr = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            int status = jsonObject.getInt("status");

                            if (status == 1) {
                                shareModels.clear();
                                JSONArray dataArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < dataArray.length(); i++) {

                                    JSONObject userObj = dataArray.getJSONObject(i);
                                    SearchModel searchModel = new SearchModel();
                                    searchModel.setName(userObj.getString("firstName"));
                                    searchModel.setId(userObj.getString("userId"));
                                    searchModel.setProfilePic(userObj.getString("profile_pic"));
                                    searchModel.setHashtag(userObj.getString("hashtag"));
                                    try {
                                        searchModel.setFollowtype(userObj.getString("followtype"));
                                    } catch (Exception e) {
                                        searchModel.setFollowtype("");
                                    }
                                    shareModels.add(searchModel);
                                }
                                searchFriendsAdapter = new SearchFriendsAdapter(SearchFriendsActivity.this, shareModels);
                                listView.setAdapter(searchFriendsAdapter);
                            } else {
                                Toast.makeText(SearchFriendsActivity.this, "Error Fetching data", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            Toast.makeText(SearchFriendsActivity.this, "volly error", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap<String, String> paramMap = new HashMap<>();
                paramMap.put("name", searchKey);
                paramMap.put("UserId", userId);
                return paramMap;


            }
        };

        AppController.getInstance().addToRequestQueue(sr);
    }
}
