package com.scienstechnologies.openup.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PopupGoldActivity extends Activity {
    ImageView cancel;
    Button ok;
    ProgressDialog mProgressDialog;
    String url = "http://openup.sciensdemos.in/Openup_services/goldmedal_popup_read_status_update?";
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_gold);

        SharedPreferences sharedPreferences = this.getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        ok = (Button) findViewById(R.id.iv_ok_popup);
        cancel = (ImageView)findViewById(R.id.iv_cancel_popup);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postMethode();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postMethode();
            }
        });
    }

    private void postMethode() {

        if (isNetworkConnected()) {
            mProgressDialog.show();
            StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        if (status.equals("1")) {
                            String message = object.getString("message");
                            Toast.makeText(PopupGoldActivity.this, message, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(PopupGoldActivity.this, MainActivity.class);
                            startActivity(i);
                            mProgressDialog.dismiss();

                        } else if (status.equals("0")) {
                            String message = object.getString("message");
                            Toast.makeText(PopupGoldActivity.this, message, Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Toast.makeText(PopupGoldActivity.this, "volly error", Toast.LENGTH_LONG).show();
                        mProgressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                      VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> map = new HashMap<>();
                    map.put("UserId", userId);

                    return map;

                }
            };
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(PopupGoldActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
