package com.scienstechnologies.openup.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.AnswerAdapter;
import com.scienstechnologies.openup.adapters.CommentAdapter;
import com.scienstechnologies.openup.models.AnswerModel;
import com.scienstechnologies.openup.models.Comments;
import com.scienstechnologies.openup.others.ScrollViewExt;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AnswerActivity extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    AnswerAdapter answerAdapter;
    private ListView listView;
    ScrollViewExt scrollView;
    EditText answerPost;
    private ImageView send;
    String postId, userId, pic, uname, hashtag;
    TextView question;
    ImageView back;
    List<AnswerModel> answerModelList = new ArrayList<>();

    private final static String url = StringConstants.BASEURL + "AnswersInsertion";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);
        back = (ImageView) findViewById(R.id.iv_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        scrollView = (ScrollViewExt) findViewById(R.id.answer_activity_scrollview);

        SharedPreferences sharedPreferences = this.getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");
        pic = sharedPreferences.getString("profilePic", "");
        uname = sharedPreferences.getString("userName", "");
        hashtag = sharedPreferences.getString("hashTag", "");


        Intent intent = getIntent();
        postId = intent.getStringExtra("postId");

        question = (TextView) findViewById(R.id.tv_question);
        answerPost = (EditText) findViewById(R.id.tv_send_answer);
        send = (ImageView) findViewById(R.id.iv_send);

        listView = (ListView) findViewById(R.id.lv_answer);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        answerAdapter = new AnswerAdapter(AnswerActivity.this, answerModelList);
        listView.setAdapter(answerAdapter);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String answer = answerPost.getText().toString();
                if (answer.equals("")) {
                    Toast.makeText(getApplicationContext(), "please enter your answer", Toast.LENGTH_LONG).show();
                } else {
                    postDataToNetwork();
                }
            }
        });

        getPostDataFromNetwork();

    }

    private void postDataToNetwork() {
        if (isNetworkConnected()) {
            mProgressDialog.show();
            mProgressDialog.setCancelable(false);
            StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        if (status.equals("1")) {
                            String message = object.getString("message");
                            Toast.makeText(AnswerActivity.this, message, Toast.LENGTH_LONG).show();
                            AnswerModel answerModel = new AnswerModel();
                            answerModel.setAnswer(answerPost.getText().toString());
                            answerModel.setHashtag(hashtag);
                            try {
                                answerModel.setProfilePic(pic);
                            } catch (Exception e) {

                            }
                            answerModel.setUserName(uname);
                            answerModelList.add(answerModel);
                            answerAdapter.notifyDataSetChanged();
                            answerPost.setText("");
                            setListViewHeightBasedOnChildren(listView);
                            scrollView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    scrollView.fullScroll(View.FOCUS_UP);
                                    scrollView.scrollTo(0, 0);
                                    mProgressDialog.dismiss();
                                }
                            }, 1000);

                        } else if (status.equals("0")) {
                            mProgressDialog.dismiss();
                            String message = object.getString("message");
                            Toast.makeText(AnswerActivity.this, message, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        e.printStackTrace();
                        Intent i = new Intent(AnswerActivity.this, AnswerActivity.class);
                        i.putExtra("userId", userId);
                        startActivity(i);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //  VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> map = new HashMap<>();
                    map.put("PostId", postId);
                    map.put("Answer", answerPost.getText().toString());
                    map.put("UserId", userId);
                    return map;

                }
            };
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            mProgressDialog.dismiss();
            Toast.makeText(AnswerActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
        }
    }


    private void getPostDataFromNetwork() {
        String URL = StringConstants.BASEURL + "answersListBasedOnPostId?PostId=" + postId;
        if (isNetworkConnected()) {
            mProgressDialog.show();
            mProgressDialog.setCancelable(false);

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            String msg = jsonObject.getString("message");
                            Toast.makeText(AnswerActivity.this, msg, Toast.LENGTH_LONG).show();

                            JSONArray array = jsonObject.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                question.setText(object.getString("content"));
                                JSONArray array1 = object.getJSONArray("aswers");
                                for (int j = 0; j < array1.length(); j++) {
                                    JSONObject object1 = array1.getJSONObject(j);
                                    AnswerModel answerModel = new AnswerModel();
                                    answerModel.setAnswer(object1.getString("aswers"));
                                    answerModel.setHashtag(object1.getString("hashtag"));
                                    try {
                                        answerModel.setProfilePic(object1.getString("picture"));
                                    } catch (Exception e) {

                                    }
                                    answerModel.setUserName(object1.getString("name"));
                                    answerModelList.add(answerModel);
                                }
                                answerAdapter.notifyDataSetChanged();
                            }

                            setListViewHeightBasedOnChildren(listView);
                            scrollView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    scrollView.fullScroll(View.FOCUS_UP);
                                    scrollView.scrollTo(0, 0);
                                    mProgressDialog.dismiss();
                                }
                            }, 1000);
                        } else if (status == 0) {
                            mProgressDialog.dismiss();
                            Toast.makeText(AnswerActivity.this, "null", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {

                        mProgressDialog.dismiss();
                        Toast.makeText(AnswerActivity.this, "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressDialog.dismiss();
                    try {
                        Toast.makeText(AnswerActivity.this, "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(AnswerActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);

    }
}
