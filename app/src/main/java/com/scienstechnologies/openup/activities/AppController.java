package com.scienstechnologies.openup.activities;

/**
 * Created by vamsi on 24-Feb-16.
 */

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppController extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "ZmyXrseusvOSk4H2SLax62VlM";
    private static final String TWITTER_SECRET = "SNRlCwO9xHvFEcFILxdNZdHtZ1FCg5KcoFMUheTMFkGQ7hjb2h";


    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        mInstance = this;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

//    public ImageLoader getImageLoader() {
//        getRequestQueue();
//        if (mImageLoader == null) {
//            mImageLoader = new ImageLoader(this.mRequestQueue,
//                    new BitmapCache());
//        }
//        return this.mImageLoader;
//    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    public static boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 7 && pass.length() < 17) {
            return true;
        }
        return false;

    }
    public static boolean isValidPhone(String phn)
    {
        if (phn != null && phn.length() == 10)
        {
            if (phn.startsWith("9") || phn.startsWith("8") || phn.startsWith("7"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }
    public static boolean isValidFirstname(String name)
    {
        if (name != null && name.length() > 2)
        {
            return true;
        }
        return false;
    }
    public static boolean isValidLastname(String name)
    {
        if (name != null && name.length() > 2)
        {
            return true;
        }
        return false;
    }
    public static boolean isValidCountryname(String name)
    {
        if (name != null && name.length() > 2)
        {
            return true;
        }
        return false;
    }

    public static boolean isConnectedToInternet(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo[] netInfo = cm.getAllNetworkInfo();
                if (netInfo != null) {
                    for (NetworkInfo ni : netInfo) {
                        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                            if (ni.isConnected())
                                haveConnectedWifi = true;
                        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                            if (ni.isConnected())
                                haveConnectedMobile = true;
                    }
                    return haveConnectedWifi || haveConnectedMobile;
                } else {
                    Toast.makeText(context, "netInfo = NULL", Toast.LENGTH_LONG).show();
                    return false;
                }
            } else {
                Toast.makeText(context, "cm = NULL", Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}