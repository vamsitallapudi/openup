package com.scienstechnologies.openup.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.other.AboutFragment;
import com.scienstechnologies.openup.fragments.other.MyPosts;
import com.scienstechnologies.openup.fragments.other.MyProfileFragment;
import com.scienstechnologies.openup.fragments.quoteFuzz.QuoteFragment;
import com.scienstechnologies.openup.fragments.other.TermsAndConditions;
import com.scienstechnologies.openup.others.CustomDialogActivity;

public class QuoteFuzzActivity extends AppCompatActivity {
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    ImageView help;
    private ImageView text, image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_fuzz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#eeb447"));

        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container_quote, new QuoteFragment());
        fragmentTransaction.commit();

        help = (ImageView)findViewById(R.id.iv_help_quote);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(QuoteFuzzActivity.this, CustomDialogActivity.class);
                i.putExtra("title","Quote Fuzz");
                i.putExtra("des",getResources().getString(R.string.quotedes));
                i.putExtra("modaltype","quote");
                startActivity(i);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.quote_fuzz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
