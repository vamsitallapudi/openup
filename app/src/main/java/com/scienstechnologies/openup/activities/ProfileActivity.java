package com.scienstechnologies.openup.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.other.HomeFragment;
import com.scienstechnologies.openup.fragments.other.MyProfileFragment;

public class ProfileActivity extends AppCompatActivity {
    FragmentTransaction fragmentTransaction;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        back = (ImageView)findViewById(R.id.iv_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        MyProfileFragment myProfileFragment = new MyProfileFragment();
        Intent intent = getIntent();
        String userId = intent.getStringExtra("userId");
        Bundle b= new Bundle();

        b.putString("userId",userId);
        myProfileFragment.setArguments(b);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.profile_container, myProfileFragment);
        fragmentTransaction.commit();


    }


}
