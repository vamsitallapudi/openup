package com.scienstechnologies.openup.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.other.AboutFragment;
import com.scienstechnologies.openup.fragments.inspire.InspireFragment;
import com.scienstechnologies.openup.fragments.other.MyPosts;
import com.scienstechnologies.openup.fragments.other.TermsAndConditions;
import com.scienstechnologies.openup.others.CustomDialogActivity;

public class InspireActivity extends AppCompatActivity {
    FragmentTransaction fragmentTransaction;
    ImageView iv_help, wrire, video, close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspire2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#2598c8"));

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.inspire_container, new InspireFragment());
        fragmentTransaction.commit();



        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iv_help = (ImageView) findViewById(R.id.iv_help);
        iv_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(InspireActivity.this, CustomDialogActivity.class);
                i.putExtra("title","Inspire World");
                i.putExtra("des",getResources().getString(R.string.inspiredes));
                i.putExtra("modaltype","inspire");
                startActivity(i);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
