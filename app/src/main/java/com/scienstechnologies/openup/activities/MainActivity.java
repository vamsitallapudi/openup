package com.scienstechnologies.openup.activities;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.scienstechnologies.openup.LoginActivity;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.other.AboutFragment;
import com.scienstechnologies.openup.fragments.other.FavouriteFragment;
import com.scienstechnologies.openup.fragments.other.FindFriendsFragment;
import com.scienstechnologies.openup.fragments.other.GroupFragment;
import com.scienstechnologies.openup.fragments.other.HomeFragment;
import com.scienstechnologies.openup.fragments.other.MyPosts;
import com.scienstechnologies.openup.fragments.other.MyProfileFragment;
import com.scienstechnologies.openup.fragments.other.NotificationFragment;
import com.scienstechnologies.openup.fragments.other.RoundedTransformation;
import com.scienstechnologies.openup.fragments.other.ShareFragment;
import com.scienstechnologies.openup.fragments.other.TermsAndConditions;
import com.scienstechnologies.openup.others.StringConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    LinearLayout profile, group, notification, findFriends;
    String userId;
    ImageView profilepic;
    TextView username;
    ProgressDialog mProgressDialog;

    private DisplayImageOptions logooptions;
    ImageLoader imageLoader;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_profile)
                .showImageForEmptyUri(R.drawable.no_profile)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .denyCacheImageMultipleSizesInMemory()
                .writeDebugLogs()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        sharedPreferences = this.getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        if (isNetworkConnected()) {

            String url = StringConstants.BASEURL + "myprofile?userid=" + userId;

            StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    mProgressDialog.show();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            JSONObject object = jsonObject.getJSONObject("data");

                            SharedPreferences.Editor editor = sharedPreferences.edit();

                            username.setText(object.getString("firstName"));
                            editor.putString("userName", object.getString("firstName"));
                            editor.putString("profilePic", object.getString("profile_pic"));
                            try {
                                editor.putString("hashTag", object.getString("hashtag"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            editor.commit();
                            try {
                                imageLoader.displayImage(object.getString("profile_pic"), profilepic, logooptions);
                            } catch (Exception e) {

                            }

                            mProgressDialog.dismiss();
                        } else if (status == 0) {
                            String str = jsonObject.getString("message");
                            Toast.makeText(MainActivity.this, str, Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    try {
                        Toast.makeText(MainActivity.this, "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mProgressDialog.dismiss();

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(MainActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
        }


        // If the Android version is lower than Jellybean, use this call to hide
        // the status bar.
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
// Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.hide();
            }
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("OpenUp");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        username = (TextView) header.findViewById(R.id.tv_nav_txt);
        profilepic = (ImageView) header.findViewById(R.id.nav_img);

        ((View) findViewById(R.id.ll_top_strip)).setVisibility(View.VISIBLE);

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container_main, new HomeFragment());
        fragmentTransaction.commit();


        profile = (LinearLayout) findViewById(R.id.ll_home);

        profile.setBackgroundColor(Color.parseColor("#d3d4d6"));

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setBackgroundColor(Color.parseColor("#d3d4d6"));
                group.setBackgroundColor(Color.parseColor("#e9eaee"));
                notification.setBackgroundColor(Color.parseColor("#e9eaee"));
                findFriends.setBackgroundColor(Color.parseColor("#e9eaee"));
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_main, new HomeFragment());
                fragmentTransaction.commit();
            }
        });
        group = (LinearLayout) findViewById(R.id.ll_group);
        group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setBackgroundColor(Color.parseColor("#e9eaee"));
                group.setBackgroundColor(Color.parseColor("#d3d4d6"));
                notification.setBackgroundColor(Color.parseColor("#e9eaee"));
                findFriends.setBackgroundColor(Color.parseColor("#e9eaee"));
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_main, new GroupFragment());
                fragmentTransaction.commit();
            }
        });
        notification = (LinearLayout) findViewById(R.id.ll_notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setBackgroundColor(Color.parseColor("#e9eaee"));
                group.setBackgroundColor(Color.parseColor("#e9eaee"));
                notification.setBackgroundColor(Color.parseColor("#d3d4d6"));
                findFriends.setBackgroundColor(Color.parseColor("#e9eaee"));
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_main, new NotificationFragment());
                fragmentTransaction.commit();
            }
        });
        findFriends = (LinearLayout) findViewById(R.id.ll_find_friends);
        findFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setBackgroundColor(Color.parseColor("#e9eaee"));
                group.setBackgroundColor(Color.parseColor("#e9eaee"));
                notification.setBackgroundColor(Color.parseColor("#e9eaee"));
                findFriends.setBackgroundColor(Color.parseColor("#d3d4d6"));
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_main, new FindFriendsFragment());
                fragmentTransaction.commit();
            }
        });
    }

    private void getUserData() {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.ic_alert)
                    .setTitle("Exit")
                    .setMessage("Do you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishAffinity();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.inspire_world, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            Intent intent = new Intent(MainActivity.this, SearchFriendsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        ((View) findViewById(R.id.ll_top_strip)).setVisibility(View.GONE);
        switch (item.getItemId()) {
            case R.id.nav_profile:
                fragmentTransaction.replace(R.id.container_main, new MyProfileFragment());
                fragmentTransaction.commit();
                break;
            case R.id.nav_about:
                fragmentTransaction.replace(R.id.container_main, new AboutFragment());
                fragmentTransaction.commit();
                break;
            case R.id.nav_home:
                profile.setBackgroundColor(Color.parseColor("#d3d4d6"));
                group.setBackgroundColor(Color.parseColor("#e9eaee"));
                notification.setBackgroundColor(Color.parseColor("#e9eaee"));
//                findFriends.setBackgroundColor(Color.parseColor("#e9eaee"));
                ((View) findViewById(R.id.ll_top_strip)).setVisibility(View.VISIBLE);
                fragmentTransaction.replace(R.id.container_main, new HomeFragment());
                fragmentTransaction.commit();
                break;
            case R.id.nav_myposts:
                fragmentTransaction.replace(R.id.container_main, new MyPosts());
                fragmentTransaction.commit();
                break;
            case R.id.nav_share:
                fragmentTransaction.replace(R.id.container_main, new ShareFragment());
                fragmentTransaction.commit();
                break;
            case R.id.nav_favourities:
                fragmentTransaction.replace(R.id.container_main, new FavouriteFragment());
                fragmentTransaction.commit();
                break;
            case R.id.nav_terms:
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_main, new TermsAndConditions());
                fragmentTransaction.commit();
                break;
            case R.id.nav_update:
                ((View) findViewById(R.id.ll_top_strip)).setVisibility(View.VISIBLE);
                Intent i = new Intent(MainActivity.this, UpdateProfileActivity.class);
                startActivity(i);
                break;
            case R.id.nav_logout:
                SharedPreferences prefs = getSharedPreferences(LoginActivity.USER, Context.MODE_PRIVATE);
                prefs.edit().clear().commit();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }
}
