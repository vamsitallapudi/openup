package com.scienstechnologies.openup.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.scienstechnologies.openup.LoginActivity;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.others.RetroHelper;
import com.scienstechnologies.openup.others.ServiceHelper;
import com.scienstechnologies.openup.others.StringConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class UpdateProfileActivity extends AppCompatActivity {
    File file;
    Spinner country, state, city;
    Button update;
    EditText email, firstName, lastName, phone;
    ProgressDialog mProgressDialog;
    String userId,encodedImage;
    ImageView profilePic,create,upload,back;
    String mailID,FirstName,LastName,Phone;
    private DisplayImageOptions logooptions;
    ImageLoader imageLoader;

    ArrayList<String> country_list = new ArrayList<>();
    ArrayList<String> country_id = new ArrayList<>();
    ArrayList<String> state_list = new ArrayList<>();
    ArrayList<String> state_id = new ArrayList<>();
    ArrayList<String> city_list = new ArrayList<>();
    ArrayList<String> city_id = new ArrayList<>();
    String selected_country_id, selected_state_id, selected_city_id;

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    private int PICK_IMAGE_REQUEST = 1;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_profile)
                .showImageForEmptyUri(R.drawable.no_profile)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .denyCacheImageMultipleSizesInMemory()
                .writeDebugLogs()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        back = (ImageView)findViewById(R.id.iv_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final SharedPreferences sharedPreferences = this.getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        email = (EditText) findViewById(R.id.et_up_email);
        firstName = (EditText) findViewById(R.id.et_up_firstname);
        lastName = (EditText) findViewById(R.id.et_up_lastname);
        phone = (EditText) findViewById(R.id.et_up_phone);
        country = (Spinner) findViewById(R.id.spi_cuntry);
        state = (Spinner) findViewById(R.id.spi_state);
        city = (Spinner) findViewById(R.id.spi_city);
        update = (Button) findViewById(R.id.btn_update);
        profilePic = (ImageView) findViewById(R.id.profile_logo);
        create = (ImageView)  findViewById(R.id.iv_upload_create);
        upload = (ImageView)  findViewById(R.id.iv_update_upload);

        if (bitmap == null){
            bitmap = BitmapFactory.decodeResource(getResources(),
                    R.drawable.round_image);
        }


        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,
                        CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        //get user details
        getUserDetails();

        getCountryDetails();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                bitmap = ((BitmapDrawable)profilePic.getDrawable()).getBitmap();
                //create a file to write bitmap data
                file= new File(getCacheDir(), "file");
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                postUserDetails();
            }
        });
    }

    private void getUserDetails() {
        if (isNetworkConnected()) {

            String url = StringConstants.BASEURL + "myprofile?userid=" + userId;

            StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    mProgressDialog.show();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            JSONObject object = jsonObject.getJSONObject("data");
                            email.setText(object.getString("emailId"));
                            firstName.setText(object.getString("firstName"));
                            lastName.setText(object.getString("lastName"));
                            phone.setText(object.getString("contactNo"));
                            try{
                                imageLoader.displayImage(object.getString("profile_pic"),profilePic,logooptions);
//                                URL url = new URL(object.getString("profile_pic"));
//                                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            }catch (Exception e){

                            }
                            mProgressDialog.dismiss();
                        } else if (status == 0) {
                            String str = jsonObject.getString("message");
                            Toast.makeText(UpdateProfileActivity.this, str, Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(UpdateProfileActivity.this, "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    try {
                        Toast.makeText(UpdateProfileActivity.this, "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mProgressDialog.dismiss();

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(UpdateProfileActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }

    }

    private void getCountryDetails() {
        String country_url = "http://openup.sciensdemos.in/Openup_services/countries";
        StringRequest sr = new StringRequest(Request.Method.GET, country_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.d("Product", jsonObject.toString());

                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            country_list.add(jsonObject1.optString("name"));
                            country_id.add(jsonObject1.getString("id"));

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(UpdateProfileActivity.this,
                                    android.R.layout.simple_spinner_item, country_list);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            country.setAdapter(dataAdapter);


                            country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                                    selected_country_id = country_id.get(position);
                                    getState();

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            country.getSelectedItemPosition();


                        }
                    } else if (status == 0) {
                        String error = jsonObject.getString("error");
                        Toast.makeText(UpdateProfileActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(UpdateProfileActivity.this, "Json Exception", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(UpdateProfileActivity.this, "Volley Error", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(sr);
    }

    private void getState() {
        String state_url = StringConstants.BASEURL + "states?countryid=" + selected_country_id;

        StringRequest sr1 = new StringRequest(Request.Method.GET, state_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.d("Product", jsonObject.toString());

                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            state_list.add(jsonObject1.optString("name"));
                            state_id.add(jsonObject1.getString("id"));

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(UpdateProfileActivity.this,
                                    android.R.layout.simple_spinner_item, state_list);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            state.setAdapter(dataAdapter);


                            state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                                    selected_state_id = state_id.get(position);
                                    getCity();

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            state.getSelectedItemPosition();


                        }
                    } else if (status == 0) {
                        String error = jsonObject.getString("error");
                        Toast.makeText(UpdateProfileActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(UpdateProfileActivity.this, "Json Exception", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(UpdateProfileActivity.this, "Volley Error", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(sr1);
    }

    private void getCity() {
        String state_url = StringConstants.BASEURL + "cities?stateid=" + selected_state_id;

        StringRequest sr1 = new StringRequest(Request.Method.GET, state_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.d("Product", jsonObject.toString());

                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        city_list.clear();
                        city_id.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            city_list.add(jsonObject1.optString("name"));
                            city_id.add(jsonObject1.optString("id"));
                        }

                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(UpdateProfileActivity.this,
                                android.R.layout.simple_spinner_item, city_list);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        city.setAdapter(dataAdapter);


                        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                                selected_city_id = city_id.get(position);


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else if (status == 0) {
                        String error = jsonObject.getString("error");
                        Toast.makeText(UpdateProfileActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(UpdateProfileActivity.this, "Json Exception", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(UpdateProfileActivity.this, "Volley Error", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(sr1);
    }
    private void postUserDetails() {
        if (isNetworkConnected()) {
            mProgressDialog.show();
            mailID = email.getText().toString();
            FirstName = firstName.getText().toString();
            LastName = lastName.getText().toString();
            Phone = phone.getText().toString();

            MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();

            multipartTypedOutput.addPart("userid", new TypedString(userId));
            multipartTypedOutput.addPart("emailId", new TypedString(mailID));
            multipartTypedOutput.addPart("firstName", new TypedString(FirstName));
            multipartTypedOutput.addPart("lastName", new TypedString(LastName));
            multipartTypedOutput.addPart("contactNo", new TypedString(Phone));
            multipartTypedOutput.addPart("country", new TypedString(selected_country_id));
            multipartTypedOutput.addPart("state", new TypedString(selected_state_id));
            multipartTypedOutput.addPart("city", new TypedString(selected_city_id));
            multipartTypedOutput.addPart("ProfilePic", new TypedFile("image/*", file));



            getBaseClassService().UpdateProfile(multipartTypedOutput, new Callback<JsonObject>() {

                @Override
                public void success(JsonObject jsonObject, retrofit.client.Response response) {
                    Log.e("KAR","response ::"+response);
                    finishAffinity();
                    mProgressDialog.dismiss();
                    Toast.makeText(UpdateProfileActivity.this,"Registration update success", Toast.LENGTH_SHORT).show();
                    Intent i =new Intent(UpdateProfileActivity.this,LoginActivity.class);
                    startActivity(i);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("KAR","error ::"+error.getResponse().toString());
                    mProgressDialog.dismiss();
                    Toast.makeText(UpdateProfileActivity.this,"Upload Failed,please try again", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(UpdateProfileActivity.this, "Please connect to Internet Network!", Toast.LENGTH_LONG).show();
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                // convert byte array to Bitmap

                bitmap = BitmapFactory.decodeByteArray(byteArray, 0,
                        byteArray.length);

                profilePic.setImageBitmap(bitmap);


            }
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                profilePic.setImageBitmap(bitmap);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] byteImage = baos.toByteArray();
                encodedImage = Base64.encodeToString(byteImage, Base64.DEFAULT);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }
    public ServiceHelper getBaseClassService() {
        return new RetroHelper().getAdapter().create(ServiceHelper.class);
    }


}
