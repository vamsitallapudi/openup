package com.scienstechnologies.openup.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.SearchFriendsAdapter;
import com.scienstechnologies.openup.models.SearchModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FollowersActivity extends Activity {
    ProgressDialog mProgressDialog;
    private SearchFriendsAdapter followersAdapter;
    private ListView listView;
    List<SearchModel> followersModels = new ArrayList<>();
    private final static String url = StringConstants.BASEURL+"list_of_followers?UserId=";
    String userId,API,profileId;
    ImageView back;
    String type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers);
        listView = (ListView)findViewById(R.id.followers_listview);
        back = (ImageView)findViewById(R.id.iv_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SharedPreferences sharedPreferences = this.getSharedPreferences("user", Context.MODE_PRIVATE);
        profileId = sharedPreferences.getString("userId", "");

        Intent intent = getIntent();
        API = intent.getStringExtra("Follower");
        userId = intent.getStringExtra("userId");
        type = intent.getStringExtra("type");

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        getFollowers();
    }

    private void getFollowers() {
        if (isNetworkConnected()) {
            if (userId.equals(profileId)){
                profileId = "";
            }

            String URL = StringConstants.BASEURL+API+userId+"&ProfileId="+profileId;

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            JSONArray array = jsonObject.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                SearchModel followersModel = new SearchModel();
                                followersModel.setProfilePic(object.getString("profile_pic"));
                                followersModel.setName(object.getString("firstName"));
                                if (type.equals("1")){
                                    followersModel.setId(object.getString("receiverId"));
                                }else if (type.equals("2")){
                                    followersModel.setId(object.getString("senderId"));
                                }
                                followersModel.setHashtag(object.getString("hashtag"));
                                try{
                                    followersModel.setFollowtype(object.getString("followtype"));
                                }catch (Exception e){
                                    followersModel.setFollowtype("");
                                }
                                followersModels.add(followersModel);
                            }
                            followersAdapter = new SearchFriendsAdapter(FollowersActivity.this, followersModels);
                            listView.setAdapter(followersAdapter);
                            mProgressDialog.dismiss();
                        } else if (status == 0) {
                            Toast.makeText(FollowersActivity.this, "No followers", Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(FollowersActivity.this, "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    try {
                        Toast.makeText(FollowersActivity.this, "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mProgressDialog.dismiss();

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(FollowersActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
