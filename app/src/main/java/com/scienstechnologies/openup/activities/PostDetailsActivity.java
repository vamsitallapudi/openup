package com.scienstechnologies.openup.activities;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.CommentAdapter;
import com.scienstechnologies.openup.adapters.PostAdapter;
import com.scienstechnologies.openup.adapters.ProfileAdapter;
import com.scienstechnologies.openup.models.Comments;
import com.scienstechnologies.openup.models.Profile;
import com.scienstechnologies.openup.others.ScrollViewExt;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PostDetailsActivity extends AppCompatActivity {
    private PostAdapter postAdapter;
    ListView listView;
    ProgressDialog mProgressDialog;
    List<Profile> profileList = new ArrayList<>();
    String userId,time,postId;
    long elapsed;
    ImageView back;
    ScrollViewExt scrollViewExt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        Intent intent = getIntent();
        postId = intent.getStringExtra("postId");
        back = (ImageView)findViewById(R.id.iv_back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");
        listView = (ListView)findViewById(R.id.postid_list);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        getPostDataFromNetwork();
    }

    private void getPostDataFromNetwork() {
        if (isNetworkConnected()) {
            mProgressDialog.show();

            SharedPreferences sharedPreferences = this.getSharedPreferences("user", Context.MODE_PRIVATE);
            String userId = sharedPreferences.getString("userId", "");
            String URL = StringConstants.BASEURL + "postview?PostId=" +postId +"&UserId=" +userId ;

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            try {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    Profile profile1 = new Profile();
                                    profile1.setPostTypeId(jsonObject1.getString("postTypeId"));
                                    profile1.setPostCategoryId(jsonObject1.getString("postCategory"));

                                    try {
                                        profile1.setContent(jsonObject1.getString("content"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setContent("");
                                    }
                                    try {
                                        profile1.setSubCategory(jsonObject1.getString("postSubScategory"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setSubCategory("");
                                    }
                                    try {
                                        String gold = jsonObject1.getString("noOfGoldMedals");
                                        if (gold.equals("null")) {
                                            profile1.setGold("0");
                                        } else {
                                            profile1.setGold(gold);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setGold("");
                                    }
                                    try {
                                        String silver = jsonObject1.getString("noOfSilverMedals");
                                        if (silver.equals("null")) {
                                            profile1.setSilver("0");
                                        } else {
                                            profile1.setSilver(silver);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setSilver("");
                                    }
                                    try {
                                        String copper = jsonObject1.getString("noOfBronzeMedals");
                                        if (copper.equals("null")) {
                                            profile1.setCopper("0");
                                        } else {
                                            profile1.setCopper(copper);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setCopper("");
                                    }
                                    try {
                                        profile1.setTitle(jsonObject1.getString("title"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setTitle("");
                                    }
                                    try {
                                        long date = Long.parseLong(jsonObject1.getString("time_difference"));
                                        dateFormat(date);
                                        profile1.setTimer(String.valueOf(time));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setUserName(jsonObject1.getString("firstName"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setUserName("");
                                    }
                                    try {
                                        profile1.setProfilePic(jsonObject1.getString("profile_pic"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    profile1.setImage(jsonObject1.getString("contentUrl"));
                                    try {
                                        profile1.setVideo(jsonObject1.getString("video_player_img"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setLikeCount(jsonObject1.getString("LikesCount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setCommentCount(jsonObject1.getString("CommentsCount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setGoldLike(jsonObject1.getInt("goldLikes"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setPostId(jsonObject1.getString("id"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setFavStatus(jsonObject1.getBoolean("FavouriteStatus"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setLikeStatus(jsonObject1.getBoolean("LikeStatus"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setSubCatName(jsonObject1.getString("subCategoryName"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setSharedname(jsonObject1.getString("shared_username"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setShareType(jsonObject1.getString("SharedType"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        String quoteType = jsonObject1.getString("quotetype");
                                        if (quoteType.equals("null")){
                                            profile1.setQuoteType("");
                                        }else {
                                            profile1.setQuoteType(quoteType);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setUserId(jsonObject1.getString("userId"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setHashtag(jsonObject1.getString("hashtag"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        JSONArray jsonArray1 = jsonObject1.getJSONArray("answers");
                                        profile1.setLength(jsonArray1.length());
                                        for (int j = 0; j < jsonArray1.length(); j++) {
                                            JSONObject object = jsonArray1.getJSONObject(j);
                                            profile1.setAnswer(object.getString("comments"));
                                        }
                                    } catch (Exception e) {

                                    }
                                    profileList.add(profile1);
                                }
                            } catch (Exception e) {

                            }

                            postAdapter = new PostAdapter(PostDetailsActivity.this, profileList);
                            listView.setAdapter(postAdapter);
                            mProgressDialog.dismiss();
                        } else if (status == 0) {
                            mProgressDialog.dismiss();
                            Toast.makeText(PostDetailsActivity.this, "null", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(PostDetailsActivity.this, "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressDialog.dismiss();
                    try {
                        Toast.makeText(PostDetailsActivity.this, "volly Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        }else {
            Toast.makeText(PostDetailsActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
        }
    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    private void dateFormat(long date) {

        //milliseconds
        long different = date;
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        if (different >= 86400000) {
            long str = different / daysInMilli;
            if (str > 365) {
                elapsed = str / 365;
                time = String.valueOf(elapsed + "yr");
            } else if (str > 31) {
                elapsed = str / 31;
                time = String.valueOf(elapsed + "mon");
            } else {
                elapsed = str;
                time = String.valueOf(elapsed + "d");
            }
        } else if (different >= 3600000) {
            elapsed = different / hoursInMilli;
            time = String.valueOf(elapsed + "h");
        } else if (different >= 60000) {
            elapsed = different / minutesInMilli;
            time = String.valueOf(elapsed + "min");
        } else if (different >= 6000) {
            elapsed = different / secondsInMilli;
            time = String.valueOf(elapsed + "s");
        } else {
            time = String.valueOf("1s");
        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
