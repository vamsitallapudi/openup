package com.scienstechnologies.openup.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.scienstechnologies.openup.LoginActivity;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.other.IntroFragment;

public class OpenupFirstActivity extends AppCompatActivity {
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_openup_first);

        SharedPreferences prefs = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putBoolean("HaveShownPrefs", true);
        ed.commit();


        mViewPager = (ViewPager) findViewById(R.id.activity_intro_view_pager);
        final Button join = (Button) findViewById(R.id.join_now);
        join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OpenupFirstActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();

        final MyAdapter myAdapter = new MyAdapter(fragmentManager);
        mViewPager.setAdapter(myAdapter);
        final DetailOnPageChangeListener mDetailOnPageChangeListener = new DetailOnPageChangeListener();


        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {

            }

            public void onPageScrolled(final int position, float positionOffset, int positionOffsetPixels) {

                if (position == 3) {
                    join.setText("Join now");
                    join.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(OpenupFirstActivity.this, LoginActivity.class);
                            startActivity(i);
                        }
                    });
                } else {
                    join.setText("Next");
                    join.setOnClickListener(new View.OnClickListener() {
                        int pos = position;

                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(pos + 1);
                        }
                    });
                }

            }

            public void onPageSelected(int position) {
                // Check if this is the page you want.
            }
        });

    }


    public class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        private int currentPage;

        @Override
        public void onPageSelected(int position) {
            currentPage = position;
        }

        public final int getCurrentPage() {
            return currentPage;
        }


        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }
    }


    public static class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            return super.instantiateItem(container, position);
        }


        @Override
        public Fragment getItem(int position) {
            return IntroFragment.newInstance(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 4;
        }
    }


}
