package com.scienstechnologies.openup.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.CommentAdapter;
import com.scienstechnologies.openup.models.Comments;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommentsActivity extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    private CommentAdapter commentAdapter;
    private ListView listView;
    private EditText commentPost;
    ImageView send, back;
    String postId, userId, pic, uname,hashtag;
    List<Comments> commentsList = new ArrayList<>();
    private final static String url = StringConstants.BASEURL + "CommentsInsertion";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comments_activity);
        back = (ImageView) findViewById(R.id.iv_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        SharedPreferences sharedPreferences = this.getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");
        pic = sharedPreferences.getString("profilePic", "");
        uname = sharedPreferences.getString("userName", "");
        hashtag = sharedPreferences.getString("hashTag", "");


        Intent intent = getIntent();
        postId = intent.getStringExtra("postId");


        commentPost = (EditText) findViewById(R.id.tv_send_comment);
        send = (ImageView) findViewById(R.id.iv_send_comment);
        listView = (ListView) findViewById(R.id.lv_comments_list);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        commentAdapter = new CommentAdapter(CommentsActivity.this, commentsList);
        listView.setAdapter(commentAdapter);
        getPostDataFromNetwork();
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cmnt = commentPost.getText().toString();
                if (cmnt.equals("")) {
                    Toast.makeText(CommentsActivity.this, "please enter your comment", Toast.LENGTH_LONG).show();
                } else {
                    postDataToNetwork();
                }
            }
        });


    }

    private void postDataToNetwork() {
        if (isNetworkConnected()) {
            mProgressDialog.show();
            StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        if (status.equals("1")) {
                            String message = object.getString("message");
                            Toast.makeText(CommentsActivity.this, message, Toast.LENGTH_SHORT).show();
                            /*Intent i = new Intent(CommentsActivity.this, CommentsActivity.class);
                            i.putExtra("postId",postId);
                            startActivity(i);
                            finish();*/
                            Comments comments = new Comments();
                            comments.setProfilePic(pic);
                            comments.setUserName(uname);
                            comments.setComment(commentPost.getText().toString());
                            comments.setHashtag(hashtag);
                            commentsList.add(comments);
                            commentAdapter.notifyDataSetChanged();
                            commentPost.setText("");
                            mProgressDialog.dismiss();

                        } else if (status.equals("0")) {
                            String message = object.getString("message");
                            Toast.makeText(CommentsActivity.this, message, Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressDialog.dismiss();
                    //  VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> map = new HashMap<>();
                    map.put("PostId", postId);
                    map.put("UserId", userId);
                    map.put("Comment", commentPost.getText().toString());
                    return map;

                }
            };
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(CommentsActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
        }

    }

    private void getPostDataFromNetwork() {
        if (isNetworkConnected()) {

            String URL = StringConstants.BASEURL + "CommentsListBasedONPostId?postId=" + postId;

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            JSONArray array = jsonObject.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                Comments comments = new Comments();
                                comments.setProfilePic(object.getString("profile_pic"));
                                comments.setUserName(object.getString("firstName"));
                                comments.setComment(object.getString("subComments"));
                                try{
                                    comments.setHashtag(object.getString("hashtag"));
                                }catch (Exception e){

                                }
                                commentsList.add(comments);
                            }
                            commentAdapter.notifyDataSetChanged();
                            mProgressDialog.dismiss();
                        } else if (status == 0) {
                            Toast.makeText(CommentsActivity.this, "null", Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(CommentsActivity.this, "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    try {
                        Toast.makeText(CommentsActivity.this, "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mProgressDialog.dismiss();

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(CommentsActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
