package com.scienstechnologies.openup.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.inspire.InspireFragment;
import com.scienstechnologies.openup.fragments.inspire.InspireImageFragment;
import com.scienstechnologies.openup.fragments.inspire.InspirePopup;
import com.scienstechnologies.openup.fragments.inspire.InspireSelectVideoFragment;

public class InspireWorldActivity extends FragmentActivity {
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    ImageView write,image,video,help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, new InspireFragment());
        fragmentTransaction.commit();

        write = (ImageView)findViewById(R.id.image_write);
        image = (ImageView)findViewById(R.id.tv_inspire);
        video = (ImageView)findViewById(R.id.video_inspire);
        help = (ImageView)findViewById(R.id.iv_help);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.main_container, new InspirePopup());
//                fragmentTransaction.commit();
            }
        });
        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new InspirePopup());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                image.setVisibility(View.GONE);
                video.setVisibility(View.GONE);
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new InspireImageFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                write.setVisibility(View.GONE);
                video.setVisibility(View.GONE);
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new InspireSelectVideoFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                image.setVisibility(View.GONE);
                write.setVisibility(View.GONE);
            }
        });
    }
}
