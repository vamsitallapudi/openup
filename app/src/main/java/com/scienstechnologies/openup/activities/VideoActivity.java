package com.scienstechnologies.openup.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.scienstechnologies.openup.R;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class VideoActivity extends AppCompatActivity {
    VideoView videoView;
    ImageView close;
    private ProgressDialog pDialog;
    String imagePath="";
    int condition = 0;
    public static final int progress_bar_type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        Intent intent = getIntent();
        final String url = intent.getStringExtra("videoUrl");
        close = (ImageView) findViewById(R.id.iv_close_video);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (condition == 0) {
            new DownloadFileFromURL().execute(url);
        } else {
            try {
                videoView = (VideoView) findViewById(R.id.vv_1);
                MediaController mediaController = new MediaController(VideoActivity.this);
                mediaController.setAnchorView(videoView);
                Uri video = Uri.parse(imagePath);
                videoView.setMediaController(mediaController);
                videoView.setVideoURI(video);
                videoView.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        if (imagePath.equals("")) {
//            Toast.makeText(VideoActivity.this,"no data found",Toast.LENGTH_SHORT).show();
//        } else {
//            try {
//                videoView = (VideoView) findViewById(R.id.vv_1);
//                MediaController mediaController = new MediaController(VideoActivity.this);
//                mediaController.setAnchorView(videoView);
//                Uri video = Uri.parse(imagePath);
//                videoView.setMediaController(mediaController);
//                videoView.setVideoURI(video);
//                videoView.start();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }


    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }


    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

            // Displaying downloaded image into image view
            // Reading image path from sdcard
            imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
            // setting downloaded into image view
            condition = 1;
            try {
                videoView = (VideoView) findViewById(R.id.vv_1);
                MediaController mediaController = new MediaController(VideoActivity.this);
                mediaController.setAnchorView(videoView);
                Uri video = Uri.parse(imagePath);
                videoView.setMediaController(mediaController);
                videoView.setVideoURI(video);
                videoView.start();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}
