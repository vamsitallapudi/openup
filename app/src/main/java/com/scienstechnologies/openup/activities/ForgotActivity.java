package com.scienstechnologies.openup.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.LoginActivity;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotActivity extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    private EditText email;
    private Button submit;
    private static final String URL = StringConstants.BASEURL + "reset_password";
    private static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");


        email = (EditText) findViewById(R.id.et_email_forgot);
        submit = (Button) findViewById(R.id.button_change);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressDialog.show();
                mProgressDialog.setCancelable(false);
                if (isNetworkConnected()) {
                    StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, response);
                            try {
                                JSONObject object = new JSONObject(response);
                                Log.d(TAG, response);
                                String status = object.getString("status");
                                if (status.equals("1")) {
                                    String message = object.getString("message");
                                    Toast.makeText(ForgotActivity.this, message, Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(ForgotActivity.this, LoginActivity.class);
                                    i.putExtra("jsonstring", response);
                                    startActivity(i);
                                    finish();
                                    mProgressDialog.dismiss();
                                } else if (status.equals("0")) {
                                    String message = object.getString("message");
                                    Toast.makeText(ForgotActivity.this, message, Toast.LENGTH_LONG).show();
                                    mProgressDialog.dismiss();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                mProgressDialog.dismiss();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                Toast.makeText(ForgotActivity.this, "volly error", Toast.LENGTH_LONG).show();
                                mProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }) {
                        protected Map<String, String> getParams() {
                            Map<String, String> map = new HashMap<>();
                            map.put("emailId", email.getText().toString());
                            return map;

                        }
                    };
                    AppController.getInstance().addToRequestQueue(sr);
                } else {
                    Toast.makeText(ForgotActivity.this, "Please connect to Network!", Toast.LENGTH_LONG).show();
                    mProgressDialog.dismiss();
                }


            }
        });

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;


    }

}
