package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.activities.PostDetailsActivity;
import com.scienstechnologies.openup.fragments.other.RoundedTransformation;
import com.scienstechnologies.openup.models.NotificationModel;
import com.scienstechnologies.openup.others.StringConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dell on 5/16/2016.
 */
public class NotificationAdapter extends BaseAdapter {
    public Activity activity;
    private List<NotificationModel> notificationModels;
    private LayoutInflater inflater;
    String notificationID;
    ProgressDialog mProgressDialog;
    String readStatus;
    private final static String url = StringConstants.BASEURL + "notifications_read_status_update";

    private DisplayImageOptions logooptions;
    ImageLoader imageLoader;

    public NotificationAdapter(Activity activity, List<NotificationModel> notificationModelList) {
        this.activity = activity;
        this.notificationModels = notificationModelList;

        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_profile)
                .showImageForEmptyUri(R.drawable.no_profile)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(activity)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .denyCacheImageMultipleSizesInMemory()
                .writeDebugLogs()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }

    @Override
    public int getCount() {
        return notificationModels.size();
    }

    @Override
    public Object getItem(int position) {
        return notificationModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.notification_list_item, null);
        }
        final TextView notification = (TextView) convertView.findViewById(R.id.tv_notification);
        final TextView userName = (TextView) convertView.findViewById(R.id.tv_username);
        final TextView time = (TextView) convertView.findViewById(R.id.tv_home_time);
        final ImageView profilePic = (ImageView) convertView.findViewById(R.id.iv_profile);
        final RelativeLayout background = (RelativeLayout) convertView.findViewById(R.id.rl_background);

        final NotificationModel notificationModel = notificationModels.get(position);
        readStatus = notificationModel.getStatus(position);
        if (readStatus.equals("1")) {
            background.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_not_read));
        } else {
            background.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_not_unread));
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String postId = notificationModel.getPostId(position);
                notificationID = notificationModel.getNotificationId(position);
                readStatus = notificationModel.getStatus(position);
                if (readStatus.equals("1")) {
                    Intent intent = new Intent(activity, PostDetailsActivity.class);
                    intent.putExtra("postId", postId);
                    activity.startActivity(intent);
                } else {

                    if (isNetworkConnected()) {
                        mProgressDialog = new ProgressDialog(activity);
                        mProgressDialog.setMessage("Loading..");
                        mProgressDialog.setCancelable(false);
                        mProgressDialog.show();
                        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject object = new JSONObject(response);
                                    String status = object.getString("status");
                                    if (status.equals("1")) {
                                        String message = object.getString("message");
                                        if (readStatus.equals("1")) {
                                            background.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_not_read));
                                        } else {
                                            background.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_not_read));
                                            notificationModel.setStatus("1");
                                        }
                                        mProgressDialog.dismiss();
                                        Intent intent = new Intent(activity, PostDetailsActivity.class);
                                        intent.putExtra("postId", postId);
                                        activity.startActivity(intent);

                                    } else if (status.equals("0")) {
                                        String message = object.getString("message");
                                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                                        mProgressDialog.dismiss();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressDialog.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                mProgressDialog.dismiss();
                                //  VolleyLog.d(TAG, "Error: " + error.getMessage());
                            }
                        }) {
                            protected Map<String, String> getParams() {
                                Map<String, String> map = new HashMap<>();
                                map.put("notiicationid", notificationID);
                                return map;

                            }
                        };
                        AppController.getInstance().addToRequestQueue(sr);
                    } else {
                        Toast.makeText(activity, "Please connect to Network!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        notification.setText(notificationModel.getNotification());
        userName.setText(notificationModel.getUserName());
        time.setText(notificationModel.getTime());
        try {

            imageLoader.displayImage(notificationModel.getImage(), profilePic, logooptions);
        } catch (Exception e) {

        }
        final String userId = notificationModel.getUserId(position);
        final String postId = notificationModel.getPostId(position);
//        userName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(activity, PostDetailsActivity.class);
//                intent.putExtra("postId", postId);
//                intent.putExtra("userId", userId);
//                activity.startActivity(intent);
//            }
//        });

        return convertView;

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

}
