package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AdminImagesActivity;
import com.scienstechnologies.openup.fragments.Mylife.Mylifecropimage;
import com.scienstechnologies.openup.fragments.inspire.CreateImageFragment;
import com.scienstechnologies.openup.fragments.quoteFuzz.QuoteImageFragment;
import com.scienstechnologies.openup.models.ShareModel;
import com.squareup.picasso.Picasso;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by Sciens on 6/10/2016.
 */
public class ImagesAdapter extends BaseAdapter {
    private Activity activity;
    private List<ShareModel> shareModelList;
    private LayoutInflater inflater;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    Bundle b;
    Bitmap imageBit;
    String value;

    public ImagesAdapter(Activity activity, List<ShareModel> shareModelList, FragmentManager fragmentManager, String id){
        this.activity = activity;
        this.shareModelList= shareModelList;
        this.fragmentManager=fragmentManager;
        this.value = id;
    }


    @Override
    public int getCount() {
        return shareModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return shareModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(inflater == null){
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null){
            convertView = inflater.inflate(R.layout.images_list_admin,parent,false);
        }

        ImageView image = (ImageView)convertView.findViewById(R.id.iv_admin_image);
        final ShareModel shareModel = shareModelList.get(position);
        Picasso.with(activity).load(shareModel.getImageId(position)).into(image);

        b = new Bundle();
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imageId = shareModel.getImageId(position);

                URL url = null;
                try {
                    url = new URL(imageId);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                try {
                    imageBit = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (value.equals("1")){
                    b.putParcelable("Bitmap", imageBit);
                    CreateImageFragment createImageFragment = new CreateImageFragment();
                    createImageFragment.setArguments(b);
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.inspire_container, createImageFragment);
                    fragmentTransaction.commit();
                }else if (value.equals("2")){
                    b.putParcelable("Bitmap", imageBit);
                    QuoteImageFragment createImageFragment = new QuoteImageFragment();
                    createImageFragment.setArguments(b);
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_quote, createImageFragment);
                    fragmentTransaction.commit();
                }else if (value.equals("3")){
                    b.putParcelable("Bitmap", imageBit);
                    Mylifecropimage createImageFragment = new Mylifecropimage();
                    createImageFragment.setArguments(b);
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_mylife, createImageFragment);
                    fragmentTransaction.commit();
                }
            }
        });
        return convertView;
    }
}
