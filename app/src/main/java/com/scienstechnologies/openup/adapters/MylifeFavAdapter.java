package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.models.Mylifefavmodel;
import com.scienstechnologies.openup.models.NotificationModel;

import java.util.List;

/**
 * Created by Dell on 5/16/2016.
 */
public class MylifeFavAdapter  extends BaseAdapter {
    public Activity activity;
    private List<Mylifefavmodel> mylifefavmodels;
    private LayoutInflater inflater;

    public MylifeFavAdapter(Activity activity, List<Mylifefavmodel> modelList){
        this.activity = activity;
        this.mylifefavmodels = modelList;
    }
    @Override
    public int getCount() {
        return mylifefavmodels.size();
    }

    @Override
    public Object getItem(int position) {
        return mylifefavmodels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater==null){
            inflater= (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null){
            convertView=inflater.inflate(R.layout.mylife_list,null);
        }
        final TextView fav=(TextView)convertView.findViewById(R.id.tv_mylifefav_username);
        Mylifefavmodel  mylifefavmodel = mylifefavmodels.get(position);
        fav.setText(mylifefavmodel.getUserName());
        return convertView;
    }

}
