package com.scienstechnologies.openup.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.other.RoundedTransformation;
import com.scienstechnologies.openup.models.AnswerModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Santhosh on 5/24/2016.
 */
public class AnswerAdapter extends BaseAdapter {
    List<AnswerModel>answerModels = new ArrayList<>();
    LayoutInflater inflater;
    Context mcontext;
    private DisplayImageOptions logooptions;
    ImageLoader imageLoader;

    public AnswerAdapter(Context mcontext,List<AnswerModel>answerModels){
        this.mcontext = mcontext;
        this.answerModels = answerModels;

        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_profile)
                .showImageForEmptyUri(R.drawable.no_profile)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .denyCacheImageMultipleSizesInMemory()
                .writeDebugLogs()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }
    @Override
    public int getCount() {
        return answerModels.size();
    }

    @Override
    public Object getItem(int position) {
        return answerModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.answer_list_item,null);
        }
        TextView answer = (TextView)convertView.findViewById(R.id.tv_answer);
        TextView profileName = (TextView)convertView.findViewById(R.id.tv_answer_user);
        ImageView profilePic = (ImageView)convertView.findViewById(R.id.iv_answer_dp);
        TextView hashtag = (TextView)convertView.findViewById(R.id.tv_hashtag);

        AnswerModel answerModel = answerModels.get(position);
        answer.setText(answerModel.getAnswer());
        profileName.setText(answerModel.getUserName());
        String hash = answerModel.getHashtag();
        if (hash.equals("null")){
            hashtag.setText("");
        }else {
            hashtag.setText(hash);
        }
        try{
           imageLoader.displayImage(answerModel.getProfilePic(),profilePic,logooptions);
        }catch (Exception e){

        }

        return convertView;
    }
}
