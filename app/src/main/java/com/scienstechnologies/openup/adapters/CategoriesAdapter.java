package com.scienstechnologies.openup.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.inspire.WriteFragment;
import com.scienstechnologies.openup.fragments.quoteFuzz.QuoteWriteFragment;
import com.scienstechnologies.openup.models.CategoriesModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sciens on 6/22/2016.
 */
public class CategoriesAdapter extends BaseAdapter{
    List<CategoriesModel>categoriesModels = new ArrayList<>();
    LayoutInflater inflater;
    Context mContext;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    int selectId;
    String quoteType;

    public CategoriesAdapter(Context mContext, List<CategoriesModel> categoriesModels, int id, FragmentManager supportFragmentManager, String quoteType){
        this.categoriesModels = categoriesModels;
        this.mContext = mContext;
        this.fragmentManager = supportFragmentManager;
        this.selectId = id;
        this.quoteType = quoteType;
    }

    @Override
    public int getCount() {
        return categoriesModels.size();
    }

    @Override
    public Object getItem(int position) {
        return categoriesModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.categories_list_item,null);
        }
        TextView name = (TextView)convertView.findViewById(R.id.cat_name);
        LinearLayout background = (LinearLayout)convertView.findViewById(R.id.ll_background);
        final CategoriesModel categoriesModel = categoriesModels.get(position);
        background.setBackgroundColor(Color.parseColor(categoriesModel.getColor()));
        name.setText(categoriesModel.getName(position));

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = categoriesModel.getId(position);
                String name = categoriesModel.getName(position);
                Bundle args = new Bundle();
                if (selectId==2){
                    WriteFragment writeFragment = new WriteFragment();
                    args.putString("life_key", id);
                    args.putString("name_key", name);
                    writeFragment.setArguments(args);

                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.inspire_container, writeFragment);
                    fragmentTransaction.commit();
                }
                else if (selectId == 1){
                    QuoteWriteFragment quoteWriteFragment = new QuoteWriteFragment();
                    args.putString("catId",id);
                    args.putString("name_key", name);
                    args.putString("quoteType",quoteType);
                    quoteWriteFragment.setArguments(args);
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_quote, quoteWriteFragment);
                    fragmentTransaction.commit();
                }
            }
        });

        return convertView;
    }
}
