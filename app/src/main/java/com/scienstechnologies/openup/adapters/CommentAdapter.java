package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.other.RoundedTransformation;
import com.scienstechnologies.openup.models.Comments;
import com.scienstechnologies.openup.models.Profile;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sciens on 5/21/2016.
 */
public class CommentAdapter extends BaseAdapter {
    List<Comments> commentsList = new ArrayList<>();
    private LayoutInflater inflater;
    Activity activity;
    private DisplayImageOptions logooptions;
    ImageLoader imageLoader;
    public CommentAdapter(Activity activity,List<Comments> commentses){
        this.activity = activity;
        this.commentsList = commentses;

        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_profile)
                .showImageForEmptyUri(R.drawable.no_profile)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(activity)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .denyCacheImageMultipleSizesInMemory()
                .writeDebugLogs()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }

    @Override
    public int getCount() {
        return commentsList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.comments_list_item, null);
        }

        final ImageView profilePic = (ImageView)convertView.findViewById(R.id.iv_comment_dp);
        final TextView userName = (TextView)convertView.findViewById(R.id.tv_comment_user);
        final TextView comment = (TextView)convertView.findViewById(R.id.tv_comment_text);
        TextView hashtag = (TextView)convertView.findViewById(R.id.tv_hashtag);

        Comments comments = commentsList.get(position);
        String hash = comments.getHashtag();
        if (hash.equals("null")){
            hashtag.setText("");
        }else {
            hashtag.setText(hash);
        }
        userName.setText(comments.getUserName());
        comment.setText(comments.getComment());
        try{
            imageLoader.displayImage(comments.getProfilePic(),profilePic,logooptions);
        }catch (Exception e){

        }

        return convertView;
    }
}
