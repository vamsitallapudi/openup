package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.ProfileActivity;
import com.scienstechnologies.openup.fragments.Mylife.MylifeWriteFragment;
import com.scienstechnologies.openup.fragments.other.MyProfileFragment;
import com.scienstechnologies.openup.fragments.other.RoundedTransformation;
import com.scienstechnologies.openup.models.GroupModel;
import com.scienstechnologies.openup.models.ShareModel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

/**
 * Created by vamsi on 21-May-16.
 */
public class GroupItemAdapter extends BaseAdapter {
    int score;
    private Context activity;
    private List<GroupModel> groupModelList;
    private LayoutInflater inflater;
    private Intent intent;
    Context context;

    private TextView tvName, tvScore;
    private ImageView ivProfilePic, ivCoin;
    private LinearLayout llContainer;
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;

    private DisplayImageOptions logooptions;
    ImageLoader imageLoader;
    public GroupItemAdapter(Context context, List<GroupModel> groupModelList, FragmentManager fragmentManager) {
        this.context = context;
        this.groupModelList = groupModelList;
        this.fragmentManager = fragmentManager;

        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_profile)
                .showImageForEmptyUri(R.drawable.no_profile)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .denyCacheImageMultipleSizesInMemory()
                .writeDebugLogs()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }


    @Override
    public int getCount() {
        return groupModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return groupModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        context = parent.getContext();


        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.group_list_item, parent, false);
        }

        tvName = (TextView) convertView.findViewById(R.id.tv_profile_name);
        tvScore = (TextView) convertView.findViewById(R.id.tv_score);

        ivCoin = (ImageView) convertView.findViewById(R.id.iv_coin_icon);
        ivProfilePic = (ImageView) convertView.findViewById(R.id.iv_profile_pic);
        llContainer = (LinearLayout) convertView.findViewById(R.id.ll_grup);


        final GroupModel groupModel = groupModelList.get(position);
        tvName.setText(groupModel.getName());
        tvScore.setText(groupModel.getScore());
        score = Integer.parseInt(groupModel.getScore());
        if (score>=100000){
            ivCoin.setImageResource(R.drawable.ic_lakh_40);
        }else if (score>=10000){
            ivCoin.setImageResource(R.drawable.ic_ten_40);
        }else if (score>=1000){
            ivCoin.setImageResource(R.drawable.ic_thousend_40);
        }else {
            ivCoin.setVisibility(View.GONE);
        }
        try{
            imageLoader.displayImage(groupModel.getProfilePic(),ivProfilePic,logooptions);

        }catch (Exception e){

        }
//        ivCoin.setImageResource(groupModel.getCoin());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userId = groupModel.getUserId(position);
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.putExtra("userId",userId);
                context.startActivity(intent);
            }
        });


        return convertView;
    }





}
