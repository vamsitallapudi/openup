package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.models.FollowersModel;
import com.scienstechnologies.openup.models.ShareModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Sciens on 6/11/2016.
 */
public class FollowersAdapter extends BaseAdapter {
    private Activity activity;
    private List<FollowersModel> followersModels;
    private LayoutInflater inflater;
    private DisplayImageOptions logooptions;
    ImageLoader imageLoader;
    public FollowersAdapter(Activity activity, List<FollowersModel> followersModels){
        this.activity = activity;
        this.followersModels= followersModels;

        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_profile)
                .showImageForEmptyUri(R.drawable.no_profile)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(activity)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .denyCacheImageMultipleSizesInMemory()
                .writeDebugLogs()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }

    @Override
    public int getCount() {
        return followersModels.size();
    }

    @Override
    public Object getItem(int position) {
        return followersModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater == null){
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null){
            convertView = inflater.inflate(R.layout.followers_list_item,parent,false);
        }
        TextView userName = (TextView)convertView.findViewById(R.id.tv_follower_name);
        ImageView profilePic = (ImageView)convertView.findViewById(R.id.iv_follower);

        FollowersModel followersModel = followersModels.get(position);
        userName.setText(followersModel.getUserName());
        try{
            imageLoader.displayImage(followersModel.getProfilePic(),profilePic,logooptions);
        }catch (Exception e){

        }

        return convertView;
    }
}
