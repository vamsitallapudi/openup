package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.models.Home;
import com.scienstechnologies.openup.models.HomeItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home on 3/15/2016.
 */
public class HomeAdapter extends BaseAdapter{
    public Activity activity;
    List<HomeItem> homeList = new ArrayList<>();
    private LayoutInflater inflater;
    boolean value =true;

    public HomeAdapter(Activity activity, List<HomeItem>homeList){
        this.activity=activity;
        this.homeList = homeList;
    }

    @Override
    public int getCount() {
        return homeList.size();
    }

    @Override
    public Object getItem(int position) {
        return homeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater==null){
            inflater= (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null){
            convertView=inflater.inflate(R.layout.home_listitems,null);
        }
        final TextView tvDpname=(TextView)convertView.findViewById(R.id.tv_message_text);
        final TextView username = (TextView)convertView.findViewById(R.id.tv_dp_name);
        HomeItem homeItem = homeList.get(position);

        String postCategory = homeItem.getPostCategory();
        String postTypeId = homeItem.getPostTypeId();



        return convertView;
    }
}
