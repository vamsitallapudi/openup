package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.activities.ProfileActivity;
import com.scienstechnologies.openup.models.SearchModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vamsi on 21-Jun-16.
 */
public class SearchFriendsAdapter extends BaseAdapter {
    List<SearchModel> shareModels = new ArrayList<>();
    private Activity activity;
    private LayoutInflater inflater;
    TextView tvName;
    ImageView tvFollow;
    private RelativeLayout rlFriendsItem;
    String userId;
    String id;
    ProgressDialog mProgressDialog;

    private DisplayImageOptions logooptions;
    ImageLoader imageLoader;


    public SearchFriendsAdapter(Activity activity, List<SearchModel> searchModels) {
        this.activity = activity;
        this.shareModels = searchModels;

        mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);


        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_profile)
                .showImageForEmptyUri(R.drawable.no_profile)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(activity)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .denyCacheImageMultipleSizesInMemory()
                .writeDebugLogs()
                .denyCacheImageMultipleSizesInMemory()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }


    @Override
    public int getCount() {
        return shareModels.size();
    }

    @Override
    public Object getItem(int position) {
        return shareModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        SharedPreferences sharedPreferences = activity.getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");


        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.friends_list_item, parent, false);
        }
        rlFriendsItem = (RelativeLayout) convertView.findViewById(R.id.rl_friends_item);
        tvName = (TextView) convertView.findViewById(R.id.tv_name);
        tvFollow = (ImageView) convertView.findViewById(R.id.tv_follow1);
        ImageView image = (ImageView) convertView.findViewById(R.id.iv_profile_pic);
        TextView hashtag = (TextView) convertView.findViewById(R.id.tv_hashtag);

        final SearchModel searchModel = shareModels.get(position);

        hashtag.setText(searchModel.getHashtag());
        tvName.setText(searchModel.getName());
        imageLoader.displayImage(searchModel.getProfilePic(), image, logooptions);
        String followType = searchModel.getFollowtype(position);
        try {
            if (followType.equals("following")) {
                tvFollow.setImageDrawable(activity.getResources().getDrawable(R.drawable.following_new_user));
            } else {
                tvFollow.setImageDrawable(activity.getResources().getDrawable(R.drawable.follow_new_user));
            }
        } catch (Exception e) {

        }


        tvFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mProgressDialog.show();
                id = searchModel.getId(position);
                String URL = StringConstants.BASEURL + "send_request";

                StringRequest sr = new StringRequest(Request.Method.POST, URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject respObj = new JSONObject(response);

                                    if (respObj.getInt("status") == 1) {

                                        tvFollow = (ImageView) v.findViewById(R.id.tv_follow1);
                                        if (respObj.getInt("response") == 1) {
                                            searchModel.setFollowtype("following");
                                            tvFollow.setImageDrawable(activity.getResources().getDrawable(R.drawable.following_new_user));

                                            mProgressDialog.dismiss();
                                        } else if (respObj.getInt("response") == 0) {
                                            searchModel.setFollowtype("follower");
                                            tvFollow.setImageDrawable(activity.getResources().getDrawable(R.drawable.follow_new_user));
                                            mProgressDialog.dismiss();
                                        }

                                    } else {
                                        String message = respObj.getString("message");
                                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                                        mProgressDialog.dismiss();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(activity, "json exception", Toast.LENGTH_SHORT).show();
                                    mProgressDialog.dismiss();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            Toast.makeText(activity, "volly error", Toast.LENGTH_SHORT).show();
                            mProgressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> paramMap = new HashMap<>();
                        paramMap.put("senderId", userId);
                        paramMap.put("receiverId", id);
                        return paramMap;
                    }
                };

                AppController.getInstance().addToRequestQueue(sr);
            }
        });


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, ProfileActivity.class);
                intent.putExtra("userId", searchModel.getId(position));
                activity.startActivity(intent);

            }
        });


        return convertView;
    }

}
