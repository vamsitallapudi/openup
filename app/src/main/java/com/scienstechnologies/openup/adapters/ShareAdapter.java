package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.models.ShareModel;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by vamsi on 20-May-16.
 */
public class ShareAdapter extends BaseAdapter {
    private Activity activity;
    private List<ShareModel> shareModelList;
    private LayoutInflater inflater;
    private ImageView ivShareImage;
    private Intent chooser;
    private TextView share,save;

    public  ShareAdapter(Activity activity, List<ShareModel> shareModelList){
        this.activity = activity;
        this.shareModelList= shareModelList;
    }


    @Override
    public int getCount() {
        return shareModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return shareModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        if(inflater == null){
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null){
            convertView = inflater.inflate(R.layout.share_list_item,parent,false);
        }
        save = (TextView) convertView.findViewById(R.id.tv_save);
        share = (TextView) convertView.findViewById(R.id.tv_share);

        ivShareImage = (ImageView) convertView.findViewById(R.id.iv_share_image);

        final ShareModel shareModel = shareModelList.get(position);
        Picasso.with(activity).load(shareModel.getImageId(position)).into(ivShareImage);

        implementOnClickListeners();

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Picasso.with(activity).load(shareModel.getImageId(position)).into(ivShareImage);
                }catch (Exception e){

                }
                Uri uri = getLocalBitmapUri(ivShareImage);
                shareData(uri);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Picasso.with(activity).load(shareModel.getImageId(position)).into(ivShareImage);
                }catch (Exception e){

                }
                Uri uri = getLocalBitmapUri(ivShareImage);
                saveData(ivShareImage);
            }
        });
        return convertView;
    }

    private void saveData(ImageView uri) {

        Bitmap bitmap = ((BitmapDrawable) uri.getDrawable()).getBitmap();

        //always save as
        String fileName = "share_image"+System.currentTimeMillis()+".png";

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

        File ExternalStorageDirectory = Environment.getExternalStorageDirectory();
        File file = new File(ExternalStorageDirectory + File.separator + fileName);

        FileOutputStream fileOutputStream = null;
        try {
            file.createNewFile();
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes.toByteArray());

            Toast.makeText(activity,"file saved to "+
                            file.getAbsolutePath(),
                    Toast.LENGTH_LONG).show();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

    }

    private void shareData(Uri uri) {
        if(uri!=null){
            Intent share = new Intent(Intent.ACTION_SEND);
            // If you want to share a png image only, you can do:
            // setType("image/png"); OR for jpeg: setType("image/jpeg");
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            share.putExtra(Intent.EXTRA_TEXT, "");
            chooser = Intent.createChooser(share, "Send image");
            activity.startActivity(chooser);
        }else{
            Toast.makeText(activity, "Image Sharing Failed", Toast.LENGTH_SHORT).show();
        }
    }

    private void implementOnClickListeners() {
//        save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
// /*
//     * Save bitmap to ExternalStorageDirectory
//     */
//
//                //get bitmap from ImageVIew
//                //not always valid, depends on your drawable
//                Bitmap bitmap = ((BitmapDrawable) ivShareImage.getDrawable()).getBitmap();
//
//                //always save as
//                String fileName = "test.jpg";
//
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
//
//                File ExternalStorageDirectory = Environment.getExternalStorageDirectory();
//                File file = new File(ExternalStorageDirectory + File.separator + fileName);
//
//                FileOutputStream fileOutputStream = null;
//                try {
//                    file.createNewFile();
//                    fileOutputStream = new FileOutputStream(file);
//                    fileOutputStream.write(bytes.toByteArray());
//
//                    Toast.makeText(activity,"file saved to "+
//                            file.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
//
//                } catch (IOException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                } finally {
//                    if (fileOutputStream != null) {
//                        try {
//                            fileOutputStream.close();
//                        } catch (IOException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//
//        });
    }


    // Returns the URI path to the Bitmap displayed in specified ImageView
    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file =  new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}
