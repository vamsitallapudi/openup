package com.scienstechnologies.openup.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AnswerActivity;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.activities.CommentsActivity;
import com.scienstechnologies.openup.activities.ImageViewActivity;
import com.scienstechnologies.openup.activities.ProfileActivity;
import com.scienstechnologies.openup.activities.VideoActivity;
import com.scienstechnologies.openup.models.Profile;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sciens on 6/23/2016.
 */
public class PostAdapter extends BaseAdapter {
    ProgressDialog mProgressDialog;
    List<Profile> profileList = new ArrayList<>();
    private LayoutInflater inflater;
    Context mcontext;
    TextView userName, feeling, content, title, answer, clock, comment, share, gold, copper, silver, more, like, hashtag,quoteType;
    ImageView profilePic, postimage, coin, favourite2, imagePlay, post,postimageOverlay;
    LinearLayout llMedals;
    String url_post_fav = StringConstants.BASEURL + "favoritePosting";
    String url_post_share = StringConstants.BASEURL + "userSharing";
    String url_post_like = StringConstants.BASEURL + "userLikes";
    String getUrl_post_medals = StringConstants.BASEURL + "userMedalsLike";
    View view;
    String goldValue;
    String postId, likeType, userId;
    private DisplayImageOptions logooptions,postImageOptions;
    ImageLoader imageLoader,postImageLoader;

    public PostAdapter(Context mcontext, List<Profile> profiles) {
        this.mcontext = mcontext;
        this.profileList = profiles;

        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.loading)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .showImageOnFail(R.drawable.loading)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .writeDebugLogs()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


        postImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.loading)
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.loading)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration postConfig = new ImageLoaderConfiguration.Builder(mcontext)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(postImageOptions)
                .writeDebugLogs()
                .build();
        postImageLoader = ImageLoader.getInstance();
        postImageLoader.init(postConfig);
    }

    @Override
    public int getCount() {
        return profileList.size();
    }

    @Override
    public Object getItem(int position) {
        return profileList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.post_list_item, null);
        }

        mProgressDialog = new ProgressDialog(mcontext);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setIndeterminate(false);


        SharedPreferences sharedPreferences = mcontext.getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");


        userName = (TextView) convertView.findViewById(R.id.tv_dp_name);
        feeling = (TextView) convertView.findViewById(R.id.tv_home_feeling);
        content = (TextView) convertView.findViewById(R.id.iv_home_contenttextview);
        title = (TextView) convertView.findViewById(R.id.iv_home_comment);
        answer = (TextView) convertView.findViewById(R.id.iv_home_ans);
        clock = (TextView) convertView.findViewById(R.id.iv_home_clock);
        comment = (TextView) convertView.findViewById(R.id.tv_home_comment);
        share = (TextView) convertView.findViewById(R.id.iv_home_share);
        gold = (TextView) convertView.findViewById(R.id.iv_home_gold);
        silver = (TextView) convertView.findViewById(R.id.iv_home_silver);
        copper = (TextView) convertView.findViewById(R.id.iv_home_copper);
        postimage = (ImageView) convertView.findViewById(R.id.iv_home_imageview);
        postimageOverlay = (ImageView) convertView.findViewById(R.id.iv_home_imageview_overlay);
        imagePlay = (ImageView) convertView.findViewById(R.id.play_video);
        profilePic = (ImageView) convertView.findViewById(R.id.iv_home_dp);
        favourite2 = (ImageView) convertView.findViewById(R.id.iv_home_fav_black);
        llMedals = (LinearLayout) convertView.findViewById(R.id.iv_home_medals_RL);
        post = (ImageView) convertView.findViewById(R.id.tv_home_postcomment);
        more = (TextView) convertView.findViewById(R.id.tv_home_more);
        like = (TextView) convertView.findViewById(R.id.iv_home_like);
        view = (View) convertView.findViewById(R.id.bottom_view);
        coin = (ImageView) convertView.findViewById(R.id.user_coin);
        hashtag = (TextView) convertView.findViewById(R.id.tv_hashtag);
        quoteType = (TextView)convertView.findViewById(R.id.quote_type);
        TextView tv_top = (TextView) convertView.findViewById(R.id.tv_top);


        final Profile profile = profileList.get(position);
        hashtag.setText(profile.getHashtag());
        try {
            imageLoader.displayImage(profile.getProfilePic(), profilePic, logooptions);
        } catch (Exception e) {

        }

        int goldLikeCount = profile.getGoldLike();
        coin.setVisibility(View.VISIBLE);
        if (goldLikeCount >= 100000) {
            coin.setImageResource(R.drawable.ic_100k_icon);
        } else if (goldLikeCount >= 10000) {
            coin.setImageResource(R.drawable.ic_star_icon);
        } else if (goldLikeCount >= 1000) {
            coin.setImageResource(R.drawable.ic_gold_icon);
        } else {
            coin.setVisibility(View.GONE);
        }
        userName.setText(profile.getUserName());

        String postCategoryId = profile.getPostCategoryId();
        if (postCategoryId.equals("1")) {
            quoteType.setVisibility(View.GONE);
            like.setVisibility(View.GONE);
            favourite2.setVisibility(View.GONE);
            answer.setVisibility(View.GONE);
            post.setVisibility(View.GONE);
            clock.setVisibility(View.VISIBLE);
            clock.setText(profile.getTimer());
            comment.setVisibility(View.VISIBLE);
            comment.setText(profile.getCommentCount());
            llMedals.setVisibility(View.VISIBLE);
            more.setVisibility(View.GONE);

            share.setVisibility(View.VISIBLE);
            gold.setVisibility(View.VISIBLE);
            gold.setText(profile.getGold());
            silver.setText(profile.getSilver());
            copper.setVisibility(View.VISIBLE);
            copper.setText(profile.getCopper());
            String subCategory = profile.getSubCategory();
            feeling.setText(profile.getSubCatName());

            userName.setTextColor(ContextCompat.getColor(mcontext, R.color.color_inspire_the_world));
            view.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.color_inspire_the_world));
            title.setTextColor(ContextCompat.getColor(mcontext, R.color.color_inspire_the_world));
//            tv_top.setTextColor(ContextCompat.getColor(mcontext, R.color.color_inspire_the_world));
            String postTypeId = profile.getPostTypeId();
            if (postTypeId.equals("1")) {
                feeling.setTextColor(ContextCompat.getColor(mcontext, R.color.color_inspire_the_world));
                feeling.setBackground(ContextCompat.getDrawable(mcontext, R.drawable.inspire_padding));
                feeling.setVisibility(View.VISIBLE);
                title.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
                postimage.setVisibility(View.GONE);
                postimageOverlay.setVisibility(View.GONE);
                imagePlay.setVisibility(View.GONE);
                content.setText(profile.getContent());
                if (subCategory.equals("0")) {
                    title.setVisibility(View.VISIBLE);
                    title.setText(profile.getTitle());
                    feeling.setVisibility(View.GONE);
                }
            } else if (postTypeId.equals("2")) {
                feeling.setVisibility(View.GONE);
                String tit = profile.getContent();
                if (tit.equals("null")) {
                    content.setVisibility(View.GONE);
                } else {
                    content.setVisibility(View.VISIBLE);
                    content.setText(profile.getContent());
                }
                title.setVisibility(View.GONE);
                postimage.setVisibility(View.VISIBLE);
                postimageOverlay.setVisibility(View.VISIBLE);
                imagePlay.setVisibility(View.GONE);
                try {
                    setImageTo(profile.getImage(position),postimage);
                } catch (Exception e) {

                }
            } else if (postTypeId.equals("3")) {
                title.setVisibility(View.VISIBLE);
                title.setText(profile.getTitle());
                feeling.setVisibility(View.GONE);
                content.setVisibility(View.GONE);
                postimage.setVisibility(View.VISIBLE);
                postimageOverlay.setVisibility(View.VISIBLE);
                imagePlay.setVisibility(View.VISIBLE);
                try {
                    setImageTo(profile.getVideo(position),postimage);
                } catch (Exception e) {

                }
            }


        } else if (postCategoryId.equals("2")) {
            like.setVisibility(View.GONE);
            favourite2.setVisibility(View.GONE);
            answer.setVisibility(View.GONE);
            title.setVisibility(View.GONE);
            post.setVisibility(View.GONE);
            clock.setVisibility(View.VISIBLE);
            clock.setText(profile.getTimer());
            comment.setVisibility(View.VISIBLE);
            comment.setText(profile.getCommentCount());
            llMedals.setVisibility(View.VISIBLE);
            more.setVisibility(View.GONE);

            share.setVisibility(View.VISIBLE);
            gold.setVisibility(View.VISIBLE);
            gold.setText(profile.getGold());
            silver.setText(profile.getSilver());
            copper.setVisibility(View.VISIBLE);
            copper.setText(profile.getCopper());
            String subCategory = profile.getSubCategory();
            if (subCategory.equals("0")) {
                title.setVisibility(View.VISIBLE);
                feeling.setVisibility(View.GONE);
                title.setText(profile.getTitle());
            } else {
                feeling.setText(profile.getSubCatName());
            }
            String postType = profile.getPostTypeId();
            if (postType.equals("1")) {
                feeling.setBackground(ContextCompat.getDrawable(mcontext, R.drawable.quote_padding));
                quoteType.setVisibility(View.VISIBLE);
                String QuoteType = profile.getQuoteType();
                if (QuoteType.equals("1")){
                    quoteType.setText("Inspire Quote");
                }else if (QuoteType.equals("0")){
                    quoteType.setText("My Quote");
                }
                content.setVisibility(View.VISIBLE);
                content.setText(profile.getContent());
                postimage.setVisibility(View.GONE);
                postimageOverlay.setVisibility(View.GONE);
                imagePlay.setVisibility(View.GONE);
            } else if (postType.equals("2")) {
                quoteType.setVisibility(View.GONE);
                String tit = profile.getContent();
                if (tit.equals("null")) {
                    title.setVisibility(View.GONE);
                } else {
                    content.setVisibility(View.VISIBLE);
                    content.setText(profile.getContent());
                }
                feeling.setVisibility(View.GONE);
                title.setVisibility(View.GONE);
                postimage.setVisibility(View.VISIBLE);
                postimageOverlay.setVisibility(View.VISIBLE);
                try {
                    setImageTo(profile.getImage(position),postimage);
                } catch (Exception e) {

                }
                imagePlay.setVisibility(View.GONE);
            }
            view.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.color_quote_fuzz));
            userName.setTextColor(ContextCompat.getColor(mcontext, R.color.color_quote_fuzz));
            title.setTextColor(ContextCompat.getColor(mcontext, R.color.color_quote_fuzz));
//            tv_top.setTextColor(ContextCompat.getColor(mcontext, R.color.color_quote_fuzz));


        } else if (postCategoryId.equals("3")) {
            quoteType.setVisibility(View.GONE);
            clock.setVisibility(View.VISIBLE);
            clock.setText(profile.getTimer());
            int length = profile.getLength();
            if (length < 1) {
                answer.setVisibility(View.GONE);
                more.setVisibility(View.GONE);
            } else if (length < 2) {
                answer.setVisibility(View.VISIBLE);
                more.setVisibility(View.GONE);
            } else {
                answer.setVisibility(View.VISIBLE);
                more.setVisibility(View.VISIBLE);
            }
            like.setVisibility(View.GONE);
            favourite2.setVisibility(View.GONE);
            answer.setVisibility(View.VISIBLE);
            userName.setTextColor(ContextCompat.getColor(mcontext, R.color.color_ask_me));
            view.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.color_ask_me));
//            tv_top.setTextColor(ContextCompat.getColor(mcontext, R.color.color_ask_me));
            post.setVisibility(View.VISIBLE);
            llMedals.setVisibility(View.GONE);
            comment.setVisibility(View.GONE);
            comment.setText(profile.getCommentCount());
            share.setVisibility(View.VISIBLE);
            feeling.setVisibility(View.GONE);
            title.setVisibility(View.GONE);
            content.setVisibility(View.VISIBLE);
            content.setText(profile.getContent());
            content.setTextColor(ContextCompat.getColor(mcontext, R.color.color_ask_me));
            postimage.setVisibility(View.GONE);
            postimageOverlay.setVisibility(View.GONE);
            imagePlay.setVisibility(View.GONE);
            answer.setText(profile.getAnswer());

        } else if (postCategoryId.equals("4")) {
            quoteType.setVisibility(View.GONE);
            comment.setVisibility(View.VISIBLE);
            comment.setText(profile.getCommentCount());
            clock.setText(profile.getTimer());
            feeling.setVisibility(View.GONE);
            like.setVisibility(View.VISIBLE);
            like.setText(profile.getLikeCount());
            share.setVisibility(View.GONE);
            answer.setVisibility(View.GONE);
            userName.setTextColor(ContextCompat.getColor(mcontext, R.color.color_my_life));
            view.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.color_my_life));
            title.setTextColor(ContextCompat.getColor(mcontext, R.color.color_my_life));
//            tv_top.setTextColor(ContextCompat.getColor(mcontext, R.color.color_my_life));
            post.setVisibility(View.GONE);
            favourite2.setVisibility(View.VISIBLE);
            llMedals.setVisibility(View.GONE);
            more.setVisibility(View.GONE);

            String postType = profile.getPostTypeId();
            if (postType.equals("1")) {
                title.setVisibility(View.VISIBLE);
                title.setText(profile.getTitle());
                content.setVisibility(View.VISIBLE);
                content.setText(profile.getContent());
                postimage.setVisibility(View.GONE);
                postimageOverlay.setVisibility(View.GONE);
                imagePlay.setVisibility(View.GONE);
            } else if (postType.equals("2")) {
                title.setVisibility(View.VISIBLE);
                title.setText(profile.getTitle());
                content.setVisibility(View.VISIBLE);
                content.setText(profile.getContent());
                postimage.setVisibility(View.VISIBLE);
                postimageOverlay.setVisibility(View.VISIBLE);
                try {
                    setImageTo(profile.getImage(position),postimage);
                } catch (Exception e) {

                }
                imagePlay.setVisibility(View.GONE);
            } else if (postType.equals("3")) {
                title.setVisibility(View.VISIBLE);
                title.setText(profile.getTitle());
                content.setVisibility(View.GONE);
                content.setText(profile.getContent());
                postimage.setVisibility(View.VISIBLE);
                postimageOverlay.setVisibility(View.VISIBLE);
                try {
                    setImageTo(profile.getVideo(position),postimage);
                } catch (Exception e) {

                }
                imagePlay.setVisibility(View.VISIBLE);

            }
        } else {

        }
        postimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imageurl = profile.getImage(position);
                Intent intent = new Intent(mcontext, ImageViewActivity.class);
                intent.putExtra("imageId", imageurl);
                mcontext.startActivity(intent);
            }
        });
        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = profile.getUserId(position);
                if (!user.equals(userId)) {
                    Intent intent = new Intent(mcontext, ProfileActivity.class);
                    intent.putExtra("userId", user);
                    mcontext.startActivity(intent);
                } else {

                }

            }
        });
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = profile.getUserId(position);
                if (!user.equals(userId)) {
                    Intent intent = new Intent(mcontext, ProfileActivity.class);
                    intent.putExtra("userId", user);
                    mcontext.startActivity(intent);
                } else {
                }
            }
        });
        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = profile.getUserId(position);
                if (!user.equals(userId)) {
                    Intent intent = new Intent(mcontext, ProfileActivity.class);
                    intent.putExtra("userId", user);
                    mcontext.startActivity(intent);
                } else {
                }
            }
        });
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postId = String.valueOf(profile.getPostId(position));
                Intent intent = new Intent(mcontext, AnswerActivity.class);
                intent.putExtra("postId", postId);
                mcontext.startActivity(intent);
            }
        });
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postId = String.valueOf(profile.getPostId(position));
                Log.d("KAR", "postiddd ::" + postId + "::post type id" + profile.getPostTypeId());
                Intent intent = new Intent(mcontext, AnswerActivity.class);
                intent.putExtra("postId", postId);
                mcontext.startActivity(intent);
            }
        });
        imagePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = String.valueOf(profile.getImage(position));
                Intent intent = new Intent(mcontext, VideoActivity.class);
                intent.putExtra("videoUrl", url);
                mcontext.startActivity(intent);

            }
        });
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postId = String.valueOf(profile.getPostId(position));
                Log.d("KAR", "postiddd ::" + postId + "::post type id" + profile.getPostTypeId());
                Intent intent = new Intent(mcontext, CommentsActivity.class);
                intent.putExtra("postId", postId);
                mcontext.startActivity(intent);

            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postId = profile.getPostId(position);
                new AlertDialog.Builder(mcontext)
                        .setTitle("OpenUp")
                        .setMessage("Do you want to share the post")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                postShare();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();

            }
        });

        if (profile.isFavStatus()) {
            favourite2.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.star));
        } else {
            favourite2.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_star_border_black_18dp));

            favourite2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final String temppostId = String.valueOf(profile.getPostId(position));
                    if (isNetworkConnected()) {
                        StringRequest sr = new StringRequest(Request.Method.POST, url_post_fav, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject object = new JSONObject(response);
                                    Log.d("responce", response);
                                    String status = object.getString("status");
                                    if (status.equals("1")) {
                                        String msg = object.getString("message");
                                        Toast.makeText(mcontext, msg, Toast.LENGTH_SHORT).show();
                                        if (profile.isFavStatus()) {
                                            ((ImageView) v).setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_star_border_black_18dp));
                                            profile.setFavStatus(false);
                                        } else {
                                            ((ImageView) v).setImageDrawable(mcontext.getResources().getDrawable(R.drawable.star));
                                            profile.setFavStatus(false);
                                        }
                                        mProgressDialog.dismiss();

                                    } else if (status.equals("0")) {
                                        String message = object.getString("message");
                                        Toast.makeText(mcontext, message, Toast.LENGTH_SHORT).show();
                                        mProgressDialog.dismiss();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressDialog.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(mcontext, "volly error", Toast.LENGTH_LONG).show();
                                mProgressDialog.dismiss();
                                //  VolleyLog.d(TAG, "Error: " + error.getMessage());
                            }
                        }) {
                            protected Map<String, String> getParams() {
                                Map<String, String> map = new HashMap<>();
                                map.put("UserId", userId);
                                map.put("PostId", temppostId);
                                return map;

                            }
                        };
                        AppController.getInstance().addToRequestQueue(sr);
                    } else {
                        Toast.makeText(mcontext, "Please connect to Network!", Toast.LENGTH_LONG).show();
                        mProgressDialog.dismiss();
                    }
                }
            });
        }

        if (profile.isLikeStatus()) {
            like.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_blue_icon, //left
                    0, //top
                    0, //right
                    0);//bottom
        } else {
            like.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.like,
                    0,
                    0,
                    0);

            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    postId = profile.getPostId(position);
                    goldValue = profile.getLikeCount();
                    mProgressDialog.show();
                    mProgressDialog.setCancelable(false);
                    if (isNetworkConnected()) {
                        StringRequest sr = new StringRequest(Request.Method.POST, url_post_like, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject object = new JSONObject(response);
                                    String status = object.getString("status");
                                    if (status.equals("1")) {
                                        Toast.makeText(mcontext, "posting like!", Toast.LENGTH_SHORT).show();
                                        String value = "" + (Integer.parseInt(goldValue) + 1);
                                        ((TextView) v).setText(value);
                                        profileList.get(position).setLikeCount(value);
                                        if (profile.isFavStatus()) {
                                            ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(R.drawable.like, 0, 0, 0);
                                            profile.setFavStatus(false);
                                        } else {
                                            ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_blue_icon, 0, 0, 0);
                                            profile.setFavStatus(true);
                                        }
                                        mProgressDialog.dismiss();

                                    } else if (status.equals("0")) {
                                        String message = object.getString("message");
                                        Toast.makeText(mcontext, message, Toast.LENGTH_SHORT).show();
                                        mProgressDialog.dismiss();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressDialog.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                mProgressDialog.dismiss();
                                //  VolleyLog.d(TAG, "Error: " + error.getMessage());
                            }
                        }) {
                            protected Map<String, String> getParams() {
                                Map<String, String> map = new HashMap<>();
                                map.put("UserId", userId);
                                map.put("PostId", postId);
                                return map;

                            }
                        };
                        AppController.getInstance().addToRequestQueue(sr);
                    } else {
                        Toast.makeText(mcontext, "Please connect to Network!", Toast.LENGTH_LONG).show();
                        mProgressDialog.dismiss();
                    }

                }
            });
        }


        gold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postId = profile.getPostId(position);
                goldValue = profile.getGold();
                likeType = String.valueOf(1);
                postMedals(v, position, likeType, postId, goldValue);
            }
        });
        silver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postId = profile.getPostId(position);
                goldValue = profile.getSilver();
                likeType = String.valueOf(2);
                postMedals(v, position, likeType, postId, goldValue);
            }
        });
        copper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postId = profile.getPostId(position);
                goldValue = profile.getCopper();
                likeType = String.valueOf(3);
                postMedals(v, position, likeType, postId, goldValue);
            }
        });
        return convertView;
    }

    private void postShare() {
        if (isNetworkConnected()) {
            mProgressDialog.show();
            StringRequest sr = new StringRequest(Request.Method.POST, url_post_share, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        if (status.equals("1")) {
                            String msg = object.getString("data");
                            Toast.makeText(mcontext, msg, Toast.LENGTH_SHORT).show();
                            mProgressDialog.dismiss();

                        } else if (status.equals("0")) {
                            String message = object.getString("message");
                            Toast.makeText(mcontext, message, Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressDialog.dismiss();
                    //  VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> map = new HashMap<>();
                    map.put("UserId", userId);
                    map.put("PostId", postId);
                    return map;

                }
            };
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(mcontext, "Please connect to Network!", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }
    }

    private void postMedals(final View v, final int position, final String type, final String postId, final String likeType) {
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
        if (isNetworkConnected()) {
            StringRequest sr = new StringRequest(Request.Method.POST, getUrl_post_medals, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        if (status.equals("1")) {
                            Toast.makeText(mcontext, "posted!", Toast.LENGTH_SHORT).show();
                            String value = "" + (Integer.parseInt(goldValue) + 1);
                            ((TextView) v).setText(value);
                            if (type.equalsIgnoreCase("1")) {
                                profileList.get(position).setGold(value);
                            } else if (type.equalsIgnoreCase("2")) {
                                profileList.get(position).setSilver(value);
                            } else if (type.equalsIgnoreCase("3")) {
                                profileList.get(position).setCopper(value);
                            }
                            mProgressDialog.dismiss();

                        } else if (status.equals("0")) {
                            String message = object.getString("message");
                            Toast.makeText(mcontext, message, Toast.LENGTH_SHORT).show();
                            mProgressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mcontext, "volly error", Toast.LENGTH_LONG).show();
                    mProgressDialog.dismiss();
                    //  VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> map = new HashMap<>();
                    map.put("UserId", userId);
                    map.put("postId", postId);
                    map.put("LikeType", type);
                    return map;

                }
            };
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(mcontext, "Please connect to Network!", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) mcontext.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void setImageTo(String imageurl,final ImageView mpostimage) {
        //mpostimage.setImageBitmap(getRoundedCornerBitmap(postImageLoader.loadImageSync(imageurl,postImageOptions)));
        postImageLoader.displayImage(imageurl, mpostimage, postImageOptions, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (loadedImage != null) {
                    mpostimage.setImageBitmap(loadedImage);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }
}
