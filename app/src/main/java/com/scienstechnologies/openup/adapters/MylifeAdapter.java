package com.scienstechnologies.openup.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.scienstechnologies.openup.fragments.Mylife.MylifeSlideFragment;

/**
 * Created by Home on 3/15/2016.
 */
public class MylifeAdapter extends FragmentStatePagerAdapter {
    public MylifeAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return MylifeSlideFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 4;
    }
}
