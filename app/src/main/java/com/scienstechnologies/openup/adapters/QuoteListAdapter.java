package com.scienstechnologies.openup.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.models.Quote;

import java.util.List;

/**
 * Created by Home on 3/17/2016.
 */
public class QuoteListAdapter extends BaseAdapter {
    private Activity activity;
    private List<Quote> quoteList;
    private LayoutInflater inflater;
    boolean value =true;
    public QuoteListAdapter(Activity activity,List<Quote>quoteList){
        this.activity = activity;
        this.quoteList = quoteList;
    }
    @Override
    public int getCount() {
        return quoteList.size();
    }

    @Override
    public Object getItem(int position) {
        return quoteList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater==null){
            inflater= (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null){
            convertView=inflater.inflate(R.layout.quote_list_item,null);
        }
        final TextView dpname=(TextView)convertView.findViewById(R.id.tv_quote_dpname);
        Quote quote = quoteList.get(position);
        dpname.setText(quote.getDp_name());

        return convertView;
    }
}
