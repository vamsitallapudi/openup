package com.scienstechnologies.openup;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.activities.MainActivity;
import com.scienstechnologies.openup.fragments.inspire.CreateImageFragment;
import com.scienstechnologies.openup.fragments.other.RoundedTransformation;
import com.scienstechnologies.openup.others.RegisterHelper;
import com.scienstechnologies.openup.others.RetroHelper;
import com.scienstechnologies.openup.others.ServiceHelper;
import com.scienstechnologies.openup.others.StringConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by Santhu on 3/4/2016.
 */
public class SignUpActivity extends AppCompatActivity {
    File file;
    ProgressDialog mProgressDialog;
    static boolean ERROR = false;
    private static final String URL = StringConstants.BASEURL + "register";
    private static final String country_url = StringConstants.BASEURL + "countries";
    ArrayList<String> country_list = new ArrayList<>();
    ArrayList<String> country_id = new ArrayList<>();
    ArrayList<String> state_list = new ArrayList<>();
    ArrayList<String> state_id = new ArrayList<>();
    ArrayList<String> city_list = new ArrayList<>();
    ArrayList<String> city_id = new ArrayList<>();
    private static final String TAG = SignUpActivity.class.getSimpleName();
    private EditText email, password, first_name, last_name, mobile, dob,etHashtag;
    private ImageView profilePic;
    Spinner country, state, city;
    private Button submit;
    private int day;
    private int month;
    private int year;
    Calendar cal;
    String selected_country_id, selected_state_id, selected_city_id;
    String gender = String.valueOf("1");
    private RadioGroup rg;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    private int PICK_IMAGE_REQUEST = 1;
    Bitmap bitmap;
    SharedPreferences sharedPreferences;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        if (bitmap == null){
            bitmap = BitmapFactory.decodeResource(getResources(),
                    R.drawable.no_profile);
        }
        email = (EditText) findViewById(R.id.et_email);
        password = (EditText) findViewById(R.id.et_password);
        first_name = (EditText) findViewById(R.id.et_firstname);
        last_name = (EditText) findViewById(R.id.et_lastname);
        mobile = (EditText) findViewById(R.id.et_phone);
        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        dob = (EditText) findViewById(R.id.et_dob);
        etHashtag = (EditText) findViewById(R.id.et_hashtag);
        country = (Spinner) findViewById(R.id.spinner_country);
        state = (Spinner) findViewById(R.id.spinner_state);
        city = (Spinner) findViewById(R.id.spinner_city);
        profilePic = (ImageView) findViewById(R.id.iv_register_profilepic);
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPopup(); //create a file to write bitmap data

            }
        });

        //get country lists
        StringRequest sr = new StringRequest(Request.Method.GET, country_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.d("Product", jsonObject.toString());

                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            country_list.add(jsonObject1.optString("name"));
                            country_id.add(jsonObject1.getString("id"));

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SignUpActivity.this,
                                    android.R.layout.simple_spinner_item, country_list);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            country.setAdapter(dataAdapter);


                            country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                                    selected_country_id = country_id.get(position);
                                    getState();

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            country.getSelectedItemPosition();


                        }
                    } else if (status == 0) {
                        String error = jsonObject.getString("error");
                        Toast.makeText(SignUpActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SignUpActivity.this, "Json Exception", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SignUpActivity.this, "Volley Error", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(sr);


        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(0);
            }
        });

        rg = (RadioGroup) findViewById(R.id.rg_gender);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_male) {
                    gender = String.valueOf("1");
                } else if (checkedId == R.id.rb_female) {
                    gender = String.valueOf("0");
                }
            }
        });
        validateFields();





        submit = (Button) findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                file= new File(getCacheDir(), "file");
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                if (email.length() == 0 && password.length() == 0 && first_name.length() == 0 && last_name.length() == 0 && dob.length() == 0 && mobile.length() == 0) {
                    Toast.makeText(SignUpActivity.this, "Please ener all credentials", Toast.LENGTH_LONG).show();
                } else if (mobile.length() != 10) {
                    Toast.makeText(SignUpActivity.this, "mobile number should be 10 digits", Toast.LENGTH_LONG).show();
                } else {

                    if (isNetworkConnected()) {
                        mProgressDialog.show();

                        MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();


                        multipartTypedOutput.addPart("emailId", new TypedString(email.getText().toString()));
                        multipartTypedOutput.addPart("password", new TypedString(password.getText().toString()));
                        multipartTypedOutput.addPart("firstName", new TypedString(first_name.getText().toString()));
                        multipartTypedOutput.addPart("lastName", new TypedString(last_name.getText().toString()));
                        multipartTypedOutput.addPart("contactNo", new TypedString(mobile.getText().toString()));
                        multipartTypedOutput.addPart("dateOfBirth", new TypedString(dob.getText().toString()));
                        multipartTypedOutput.addPart("hashtag",new TypedString(etHashtag.getText().toString()));
                        multipartTypedOutput.addPart("country", new TypedString(selected_country_id));
                        multipartTypedOutput.addPart("state", new TypedString(selected_state_id));
                        multipartTypedOutput.addPart("city", new TypedString(selected_city_id));
                        multipartTypedOutput.addPart("gender", new TypedString(gender));
                        multipartTypedOutput.addPart("ProfilePic", new TypedFile("image/*", file));



                        getBaseClassService().UploadImage(multipartTypedOutput, new Callback<JsonObject>() {

                            @Override
                            public void success(JsonObject jsonObject, retrofit.client.Response response) {
                                Log.e("KAR","response ::"+jsonObject);
                                try {
                                    JSONObject object = new JSONObject(String.valueOf(jsonObject));
                                    String status = object.getString("status");
                                    if (status.equals("1")){
                                        JSONObject object1 = object.getJSONObject("data");
                                        String userId = object1.getString("userId");
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("userId", userId);
                                        editor.commit();
                                        Toast.makeText(SignUpActivity.this,"Registration success", Toast.LENGTH_SHORT).show();
                                        Intent i =new Intent(SignUpActivity.this,MainActivity.class);
                                        startActivity(i);
                                        finishAffinity();
                                        mProgressDialog.dismiss();

                                    }else if (status.equals("0")) {
                                        String message = object.getString("message");
                                        Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_LONG).show();
                                        mProgressDialog.dismiss();

                                    }
                                } catch (JSONException e) {
                                    mProgressDialog.dismiss();
                                    Toast.makeText(SignUpActivity.this, "json exception", Toast.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                try {
                                    Toast.makeText(SignUpActivity.this, "volly error", Toast.LENGTH_LONG).show();
                                    mProgressDialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    } else {
                        Toast.makeText(SignUpActivity.this, "Please connect to Internet Network!", Toast.LENGTH_LONG).show();
                    }
                }

            }

        });
    }

    PopupWindow imageSelection;

    private void selectPopup() {
        try {
            LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen12, null);
            imageSelection = new PopupWindow(layout, 300, 500, true);
            imageSelection.showAtLocation(layout, Gravity.CENTER, 0, 0);
            imageSelection.setBackgroundDrawable(new BitmapDrawable(this.getResources(), Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)));

            ImageView create = (ImageView)layout.findViewById(R.id.iv_create);
            ImageView upload = (ImageView)layout.findViewById(R.id.iv_upload);
            ImageView close = (ImageView)layout.findViewById(R.id.iv_close);
            close.setVisibility(View.VISIBLE);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageSelection.dismiss();
                }
            });
            create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,
                            CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }
            });
            upload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
// Show only images, no videos or anything else
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                }
            });

        } catch (Exception e) {

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                imageSelection.dismiss();
                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                // convert byte array to Bitmap

                bitmap = BitmapFactory.decodeByteArray(byteArray, 0,
                        byteArray.length);

                profilePic.setImageBitmap(bitmap);

//                Picasso.with(this).load(String.valueOf(bitmap))
//                        .transform(new RoundedTransformation(50, 4))
//                        .resize(100,100)
//                        .centerCrop()
//                        .into(profilePic);


            }
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            imageSelection.dismiss();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                profilePic.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void getState() {
        String state_url = StringConstants.BASEURL + "states?countryid=" + selected_country_id;

        StringRequest sr1 = new StringRequest(Request.Method.GET, state_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.d("Product", jsonObject.toString());

                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            state_list.add(jsonObject1.optString("name"));
                            state_id.add(jsonObject1.getString("id"));

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SignUpActivity.this,
                                    android.R.layout.simple_spinner_item, state_list);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            state.setAdapter(dataAdapter);


                            state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                                    selected_state_id = state_id.get(position);
                                    getCity();

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            state.getSelectedItemPosition();


                        }
                    } else if (status == 0) {
                        String error = jsonObject.getString("error");
                        Toast.makeText(SignUpActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SignUpActivity.this, "Json Exception", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SignUpActivity.this, "Volley Error", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(sr1);
    }

    private void getCity() {

        String state_url = StringConstants.BASEURL + "cities?stateid=" + selected_state_id;

        StringRequest sr1 = new StringRequest(Request.Method.GET, state_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.d("Product", jsonObject.toString());

                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        city_list.clear();
                        city_id.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            city_list.add(jsonObject1.optString("name"));
                            city_id.add(jsonObject1.optString("id"));
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SignUpActivity.this,
                                android.R.layout.simple_spinner_item, city_list);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        city.setAdapter(dataAdapter);


                        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                                selected_city_id = city_list.get(position);


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else if (status == 0) {
                        String error = jsonObject.getString("error");
                        Toast.makeText(SignUpActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SignUpActivity.this, "Json Exception", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SignUpActivity.this, "Volley Error", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(sr1);
    }

    private void validateFields() {

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (AppController.isValidEmail(s.toString())) {
                    ERROR = false;
                    email.setError(null);
                    //enableRegisterBtn();
                } else {
                    ERROR = true;
                    email.setError("Email is not Valid.");
                    //disableRegisterBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (AppController.isValidPhone(s.toString())) {
                    ERROR = false;
                    mobile.setError(null);
                    //enableRegisterBtn();
                } else {
                    ERROR = true;
                    mobile.setError("Phone Number should be exactly 10 digits and should be starting with 7 or 8 or 9.");
                    //disableRegisterBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (AppController.isValidPassword(s.toString())) {
                    ERROR = false;
                    password.setError(null);
                    //enableRegisterBtn();
                } else {
                    ERROR = true;
                    password.setError("Password should be at-least 8 characters & atleast one upper case letter, one special charecter ");
                    //disableRegisterBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        first_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (AppController.isValidFirstname(s.toString())) {
                    ERROR = false;
                    first_name.setError(null);
                    //enableRegisterBtn();
                } else {
                    ERROR = true;
                    first_name.setError("First name should be at-least 3 characters");
                    //disableRegisterBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        last_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (AppController.isValidLastname(s.toString())) {
                    ERROR = false;
                    last_name.setError(null);
                    //enableRegisterBtn();
                } else {
                    ERROR = true;
                    last_name.setError("Last name should be at-least 3 characters");
                    //disableRegisterBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


    }

    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            dob.setText(selectedDay + " / " + (selectedMonth + 1) + " / "
                    + selectedYear);
        }
    };

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }
    public RegisterHelper getBaseClassService() {
        return new RetroHelper().getAdapter().create(RegisterHelper.class);
    }


}
