package com.scienstechnologies.openup.others;


import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.mime.MultipartTypedOutput;

/**
 * Created by JoinUs4 on 17-06-2015.
 */
public interface ServiceHelper {


    @Headers("Accept:application/json")
    @POST("/insert_post_data")
    void UploadImage(@Body MultipartTypedOutput attachments, Callback<JsonObject> callback);

    @Headers("Accept:application/json")
    @POST("/update_profile")
    void UpdateProfile(@Body MultipartTypedOutput attachments, Callback<JsonObject> callback);

}
