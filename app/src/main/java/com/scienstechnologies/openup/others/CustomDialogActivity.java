package com.scienstechnologies.openup.others;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scienstechnologies.openup.R;

public class CustomDialogActivity extends Activity {

    private TextView title_tv;
    private TextView des_tv;
    private CheckBox checkBox;
    private SharedPreferences mSharedPreferences;
    boolean ischecked;
    RelativeLayout relativeLayout;


    private String title,des,modalType;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_dialog);
        mSharedPreferences = this.getSharedPreferences("visiteStatus", Context.MODE_PRIVATE);
        editor = mSharedPreferences.edit();
        relativeLayout = (RelativeLayout)findViewById(R.id.rl_custom);

//        title_tv = (TextView)findViewById(R.id.title_tv);
//        des_tv = (TextView)findViewById(R.id.des_tv);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ischecked = isChecked;
            }
        });
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            title = bundle.getString("title");
            des = bundle.getString("des");
            modalType = bundle.getString("modaltype");
        }
        if (modalType.equals("inspire")){
            relativeLayout.setBackground(getResources().getDrawable(R.drawable.black_inspire));
//            title_tv.setTextColor(getResources().getColor(R.color.color_inspire_the_world));
        }else if (modalType.equals("quote")){
            relativeLayout.setBackground(getResources().getDrawable(R.drawable.black_quote));
//            title_tv.setTextColor(getResources().getColor(R.color.color_quote_fuzz));
        }else if (modalType.equals("ask")){
            relativeLayout.setBackground(getResources().getDrawable(R.drawable.black_ask));
//            title_tv.setTextColor(getResources().getColor(R.color.color_ask_me));
        }else if (modalType.equals("life")){
            relativeLayout.setBackground(getResources().getDrawable(R.drawable.black_life));
//            title_tv.setTextColor(getResources().getColor(R.color.color_my_life));
        } else if (modalType.equals("diamond")){
            relativeLayout.setBackground(getResources().getDrawable(R.drawable.black_diamond));
//            title_tv.setTextColor(getResources().getColor(R.color.color_not_unread));
        }

//        title_tv.setText(title);
//        des_tv.setText(des);

        if(mSharedPreferences.getBoolean(modalType,false)){
            checkBox.setChecked(true);
        }

    }

    public void oncloseclick(View v){
        if(ischecked){
            editor.putBoolean(modalType,true);
            editor.commit();
        }else{
            editor.putBoolean(modalType,false);
            editor.commit();
        }

        finish();

    }
}
