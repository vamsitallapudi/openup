package com.scienstechnologies.openup.others;

/**
 * Created by Sciens on 6/16/2016.
 */
public interface ScrollViewListener {
    void onScrollChanged(ScrollViewExt scrollView,
                         int x, int y, int oldx, int oldy);
}
