package com.scienstechnologies.openup.others;

import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.mime.MultipartTypedOutput;

/**
 * Created by Sciens on 6/13/2016.
 */
public interface UpdateHelper {


    @Headers("Accept:application/json")
    @POST("/register")
    void UpdateProfile(@Body MultipartTypedOutput attachments, Callback<JsonObject> callback);

}
