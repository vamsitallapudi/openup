package com.scienstechnologies.openup.others;


import android.util.Log;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

public class RetroHelper {

    private static final String LOG_TAG = RetroHelper.class.getSimpleName();

    public static RestAdapter getAdapter() {

        String url = StringConstants.FILEURL;
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url)
                .setRequestInterceptor(getRequestInterceptor())
                .setLogLevel(RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.e(LOG_TAG, msg);
                    }
                })
                .build();

        return restAdapter;
    }

    private static RequestInterceptor getRequestInterceptor() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {

            }
        };
        return requestInterceptor;
    }

}
