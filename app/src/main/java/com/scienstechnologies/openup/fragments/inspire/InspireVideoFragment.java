package com.scienstechnologies.openup.fragments.inspire;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.gson.JsonObject;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.MainActivity;
import com.scienstechnologies.openup.others.RetroHelper;
import com.scienstechnologies.openup.others.ServiceHelper;
import com.scienstechnologies.openup.others.StringConstants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by Home on 3/3/2016.
 */
public class InspireVideoFragment extends Fragment {
    private static final String URL = StringConstants.BASEURL+"MyLifesend";
    private static final String TAG = InspireVideoFragment.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_WRITE_VIDEO = 1;
    VideoView videoView;
    Button retake, submit;
    ProgressDialog mProgressDialog;
    String userId;
    String path;
    long timeInMillisec;

    EditText editText;
    File file,thumbnailFile;
    Bitmap thumbBmp;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.screen15, container, false);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        videoView = (VideoView)v.findViewById(R.id.vv_inspire);
        Uri uri = getArguments().getParcelable("video");
        Log.d(TAG, uri.toString());
        path = getPath(getActivity(),uri) ;// "file:///mnt/sdcard/FileName.mp3"
        Log.d(TAG, path.toString());

        createAndWriteFiles(path);

        videoView.setVideoURI(uri);
        videoView.setMediaController(new MediaController(getActivity()));
        videoView.requestFocus();
        videoView.start();

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        //use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(getActivity(), uri);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        timeInMillisec = Long.parseLong(time );



        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");
        editText = (EditText)v.findViewById(R.id.et_video);
        submit = (Button)v.findViewById(R.id.btn_edit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editText.getText().toString().trim().length() > 0) {
                    if (isNetworkConnected()) {
                        if (timeInMillisec < 120000){
                            mProgressDialog.show();


                            MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();
                            multipartTypedOutput.addPart("video", new TypedFile("video/*", file));
                            multipartTypedOutput.addPart("userid", new TypedString(userId));
                            multipartTypedOutput.addPart("catid", new TypedString("1"));
                            multipartTypedOutput.addPart("posttype", new TypedString("3"));
                            multipartTypedOutput.addPart("video_player_img",new TypedFile("image/*",thumbnailFile));
                            multipartTypedOutput.addPart("title",new TypedString(editText.getText().toString()));

                            getBaseClassService().UploadImage(multipartTypedOutput, new Callback<JsonObject>() {

                                @Override
                                public void success(JsonObject jsonObject, retrofit.client.Response response) {
                                    Log.e("KAR", "response ::" + response);
                                    getActivity().finishAffinity();
                                    mProgressDialog.dismiss();
                                    Intent i = new Intent(getActivity(), MainActivity.class);
                                    startActivity(i);
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    mProgressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Upload Unsuccessful,please try again", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }else {
                            Toast.makeText(getActivity(), "Video duration should be below 2minutes", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Please enter title", Toast.LENGTH_LONG).show();
                }

            }
        });
        retake = (Button)v.findViewById(R.id.btn_retake);
        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.inspire_container, new InspireSelectVideoFragment());
                fragmentTransaction.commit();
            }
        });




        return v;
    }
    private void createAndWriteFiles(String path) {
        FileOutputStream out = null;
        try {
            file = new File(path);
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);

            String bitmap_filePath = getActivity().getExternalCacheDir().getAbsolutePath()+"/"+ System.currentTimeMillis()+".png";
            out = new FileOutputStream(bitmap_filePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            thumbnailFile = new File(bitmap_filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri)
    {
        final boolean isKitKatOrAbove = Build.VERSION.SDK_INT >=  Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKatOrAbove && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    public void onStop() {
        super.onStop();
        videoView.stopPlayback();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }
    public ServiceHelper getBaseClassService() {
        return new RetroHelper().getAdapter().create(ServiceHelper.class);
    }



}
