package com.scienstechnologies.openup.fragments.other;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.adapters.ImagesAdapter;
import com.scienstechnologies.openup.models.ShareModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sciens on 6/10/2016.
 */
public class AdminImagesFragment extends Fragment {
    ProgressDialog mProgressDialog;
    private ListView listView;
    ImagesAdapter imagesAdapter;
    private List<ShareModel> shareModelList = new ArrayList<>();
    String Id;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.admin_images_fragment, container, false);

        Id = getArguments().getString("imageType");

        listView = (ListView) v.findViewById(R.id.admin_images_list);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        getData();
        return v;
    }

    private void getData() {
        if (isNetworkConnected()) {
            mProgressDialog.show();
            String URL = StringConstants.BASEURL+"imagesList";

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            JSONArray array = jsonObject.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                ShareModel shareModel = new ShareModel();
                                try{
                                    shareModel.setImageId(object.getString("url"));
                                }catch (Exception e){

                                }
                                shareModelList.add(shareModel);
                            }
                            imagesAdapter = new ImagesAdapter(getActivity(), shareModelList,getFragmentManager(),Id);
                            listView.setAdapter(imagesAdapter);
                            listView.deferNotifyDataSetChanged();
                            mProgressDialog.dismiss();
                        } else if (status == 0) {

                            Toast.makeText(getActivity(), "null", Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mProgressDialog.dismiss();

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
