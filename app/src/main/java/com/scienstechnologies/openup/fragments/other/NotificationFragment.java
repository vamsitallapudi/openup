package com.scienstechnologies.openup.fragments.other;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.adapters.CommentAdapter;
import com.scienstechnologies.openup.adapters.NotificationAdapter;
import com.scienstechnologies.openup.models.Comments;
import com.scienstechnologies.openup.models.NotificationModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home on 3/10/2016.
 */
public class NotificationFragment extends Fragment {
    List<NotificationModel> notificationModels = new ArrayList<>();
    private NotificationAdapter notificationAdapter;
    private ListView listView;
    ProgressDialog mProgressDialog;
    String userId;
    String time;
    long elapsed;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen25, container, false);
        listView = (ListView)v.findViewById(R.id.lv_notification);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        getPostDataFromNetwork();
        return v;
    }

    private void getPostDataFromNetwork() {
        if (isNetworkConnected()) {

            String URL = StringConstants.BASEURL+"notifications?UserId="+userId;

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            JSONArray array = jsonObject.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                NotificationModel model = new NotificationModel();
                                model.setImage(object.getString("profile_pic"));
                                model.setUserName(object.getString("firstName"));
                                try {
                                    long date = Long.parseLong(object.getString("time_difference"));
                                    dateFormat(date);
                                    model.setTime(String.valueOf(time));
                                } catch (Exception e) {

                                }
                                model.setNotification(object.getString("content"));
                                model.setNotificationId(object.getString("notiicationid"));
                                model.setStatus(object.getString("read"));
                                model.setPostId(object.getString("postid"));
                                model.setUserId(object.getString("userid"));
                                notificationModels.add(model);
                            }
                            notificationAdapter = new NotificationAdapter(getActivity(), notificationModels);
                            listView.setAdapter(notificationAdapter);
                            mProgressDialog.dismiss();
                        } else if (status == 0) {
                            String error = jsonObject.getString("message");
                            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    try {
                        Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }
    }
    private void dateFormat(long date) {

        //milliseconds
        long different = date;
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        if (different >= 86400000) {
            long str = different / daysInMilli;
            if (str > 365) {
                elapsed = str / 365;
                time = String.valueOf(elapsed + "yr");
            } else if (str > 31) {
                elapsed = str / 31;
                time = String.valueOf(elapsed + "mon");
            } else {
                elapsed = str;
                time = String.valueOf(elapsed + "d");
            }
        } else if (different >= 3600000) {
            elapsed = different / hoursInMilli;
            time = String.valueOf(elapsed + "h");
        } else if (different >= 60000) {
            elapsed = different / minutesInMilli;
            time = String.valueOf(elapsed + "min");
        } else if (different >= 6000) {
            elapsed = different / secondsInMilli;
            time = String.valueOf(elapsed + "s");
        } else {
            time = String.valueOf("1s");
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

}
