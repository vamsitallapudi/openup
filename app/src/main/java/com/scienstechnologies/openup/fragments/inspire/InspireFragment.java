package com.scienstechnologies.openup.fragments.inspire;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.HomeAdapter;
import com.scienstechnologies.openup.models.Home;
import com.scienstechnologies.openup.others.CustomDialogActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home on 3/11/2016.
 */
public class InspireFragment extends Fragment {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE = 1;
    FragmentTransaction fragmentTransaction;
    List<Home> homeList = new ArrayList<>();
    private HomeAdapter homeAdapter;
    private ListView listView;
    String[] dp_name = new String[]{"santhu", "anil"};
    ImageView image, wrire, video;
    TextView mywords;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.inspire_main_activity, container, false);
        wrire = (ImageView) v.findViewById(R.id.image_write);
        wrire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.inspire_container, new InspirePopup());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        image = (ImageView) v.findViewById(R.id.tv_inspire);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE);

                }else{
                    fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.inspire_container, new InspireImageFragment());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }

            }
        });
        video = (ImageView) v.findViewById(R.id.video_inspire);
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE);

                }else{
                    fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.inspire_container, new InspireSelectVideoFragment());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
        });
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!(getActivity().getSharedPreferences("visiteStatus", Context.MODE_PRIVATE)).getBoolean("inspire",false)){
            Intent i = new Intent(getActivity(), CustomDialogActivity.class);
            i.putExtra("title","Inspire World");
            i.putExtra("des",getActivity().getResources().getString(R.string.inspiredes));
            i.putExtra("modaltype","inspire");
            getActivity().startActivity(i);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // storage-related task you need to do.
                    
                    fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.inspire_container, new InspireImageFragment());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "Requires permission for image Selection", Toast.LENGTH_SHORT).show();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


}
