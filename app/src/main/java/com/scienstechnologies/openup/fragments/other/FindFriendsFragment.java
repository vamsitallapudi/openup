package com.scienstechnologies.openup.fragments.other;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.activities.SearchFriendsActivity;
import com.scienstechnologies.openup.adapters.SearchFriendsAdapter;
import com.scienstechnologies.openup.models.SearchModel;
import com.scienstechnologies.openup.models.ShareModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vamsi on 21-Jun-16.
 */
public class FindFriendsFragment extends Fragment {
    List<SearchModel> shareModels = new ArrayList<>();
    List<SearchModel> dummyList = new ArrayList<>();
    ProgressDialog mProgressDialog;
    private ListView listView;
    EditText etSearchFriends;
    String searchKey;
    private SearchFriendsAdapter searchFriendsAdapter;
    String URL = StringConstants.BASEURL + "find_friends";
    String URL_Search = StringConstants.BASEURL + "search_find_friends";
    String userId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_find_friends, container, false);
        listView = (ListView) v.findViewById(R.id.lv_find_friends);
        etSearchFriends = (EditText) v.findViewById(R.id.et_search_friends);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");


        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        etSearchFriends.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                searchKey = s.toString();
                if (!searchKey.equals("")) {
                    dummyList.clear();
                    for (SearchModel model : shareModels ){
                        if (model.getName() != null && model.getName().toLowerCase().contains(searchKey)) {
                            dummyList.add(model);
                        } else {

                        }
                    }
                    searchFriendsAdapter = new SearchFriendsAdapter(getActivity(),dummyList);
                    listView.setAdapter(searchFriendsAdapter);
                    searchFriendsAdapter.notifyDataSetChanged();
                } else {
                    searchFriendsAdapter = new SearchFriendsAdapter(getActivity(),shareModels);
                    listView.setAdapter(searchFriendsAdapter);
                    searchFriendsAdapter.notifyDataSetChanged();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        getDataFromNetwork();

        return v;
    }

    private void getDataFromNetwork() {

        if (isNetworkConnected()) {
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();

                StringRequest sr = new StringRequest(Request.Method.POST, URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                mProgressDialog.dismiss();

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    int status = jsonObject.getInt("status");

                                    if (status == 1) {
                                        shareModels.clear();
                                        JSONArray array = jsonObject.getJSONArray("data");
                                        for (int i = 0; i < array.length(); i++) {
                                            JSONObject object = array.getJSONObject(i);
                                            SearchModel searchModel = new SearchModel();
                                            searchModel.setName(object.getString("firstName"));
                                            searchModel.setProfilePic(object.getString("profile_pic"));
                                            searchModel.setHashtag(object.getString("hashtag"));
                                            try{
                                                searchModel.setFollowtype(object.getString("followtype"));
                                            }catch (Exception e){
                                                searchModel.setFollowtype("");
                                            }

                                            searchModel.setId(object.getString("receiverId"));
                                            shareModels.add(searchModel);
                                            dummyList.add(searchModel);
                                        }
                                        searchFriendsAdapter = new SearchFriendsAdapter(getActivity(),shareModels);
                                        listView.setAdapter(searchFriendsAdapter);
                                    }else{
                                        Toast.makeText (getActivity(), "Error Fetching data", Toast.LENGTH_SHORT).show();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(), "volly error", Toast.LENGTH_LONG).show();
                                mProgressDialog.dismiss();
                            }
                        }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        HashMap<String, String> paramMap = new HashMap<>();
                        paramMap.put("UserId", userId);
                        return paramMap;


                    }

                };

                AppController.getInstance().addToRequestQueue(sr);

            }


        }


    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
