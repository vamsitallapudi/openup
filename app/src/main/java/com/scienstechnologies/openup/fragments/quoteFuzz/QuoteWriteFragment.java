package com.scienstechnologies.openup.fragments.quoteFuzz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.MainActivity;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Home on 3/17/2016.
 */
public class QuoteWriteFragment extends Fragment {
    String userId;
    ProgressDialog mProgressDialog;
    FragmentTransaction fragmentTransaction;
    Button submit;
    EditText chat_txt,title;
    TextInputLayout text;
    Bundle args;
    String catID,quoteType;
    private static final String URL = StringConstants.BASEURL+"insert_post_data";
    private static final String TAG = QuoteWriteFragment.class.getSimpleName();
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.quote_text, container, false);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        text = (TextInputLayout)v.findViewById(R.id.textContainer3);
        title = (EditText)v.findViewById(R.id.et_chat_title);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        catID = getArguments().getString("catId");
        quoteType = getArguments().getString("quoteType");
        if (catID.equals("0")){
            text.setVisibility(View.VISIBLE);
        }else {
            text.setVisibility(View.GONE);
        }
        chat_txt = (EditText)v.findViewById(R.id.et_chattext);
        submit = (Button)v.findViewById(R.id.button_quote_txt);
        args = new Bundle();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chat_txt.getText().toString().length()==0)
                {
                    Toast.makeText(getActivity(), "Write a comment...", Toast.LENGTH_LONG).show();
                    //mEditText.setError("Name cannot be Blank");
                }else {
                    if (isNetworkConnected()) {
                        mProgressDialog.show();
                        StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d(TAG, response);
                                try {
                                    JSONObject jsonobject = new JSONObject(response);
                                    Log.d(TAG, response);
                                    String status = jsonobject.getString("status");
                                    if (status.equals("1")) {
                                        String message = jsonobject.getString("message");
                                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        getActivity().finishAffinity();
                                        mProgressDialog.dismiss();
                                        Intent i =new Intent(getActivity(),MainActivity.class);
                                        startActivity(i);
                                        // Toast.makeText(getActivity(), "message", Toast.LENGTH_LONG).show();
//                                    Intent i = new Intent(getActivity(),InspireFragment.class);
//                                    i.putExtra("jsonstring", response);
//                                    startActivity(i);
                                    } else if (status.equals("0")) {
                                        String message = jsonobject.getString("message");
                                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        mProgressDialog.dismiss();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressDialog.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d(TAG, "Error: " + error.getMessage());
                                mProgressDialog.dismiss();
                            }
                        }) {
                            protected Map<String, String> getParams() {
                                Map<String, String> map = new HashMap<>();
                                map.put("userid",userId );
                                map.put("catid", "2");
                                map.put("subcatid", catID);
                                map.put("posttype", "1");
                                //   map.put("title","hi");
                                map.put("content", chat_txt.getText().toString());
                                map.put("title",title.getText().toString());
                                map.put("quotetype",quoteType);
                                return map;
                            }
                        };
                        AppController.getInstance().addToRequestQueue(sr);
                    } else {
                        Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
                    }
                }
//                String txt="hello";
//                QuoteFragment quoteFragment = new QuoteFragment();
//                args.putString("txt_key",txt);
//                quoteFragment.setArguments(args);
            }

            private boolean isNetworkConnected() {
                ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                return cm.getActiveNetworkInfo() != null;

            }
        });
        return v;
    }
}
