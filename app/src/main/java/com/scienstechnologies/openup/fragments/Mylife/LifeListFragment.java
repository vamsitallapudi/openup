package com.scienstechnologies.openup.fragments.Mylife;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scienstechnologies.openup.R;

/**
 * Created by Home on 3/18/2016.
 */
public class LifeListFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mylife_list, container, false);
        return v;
    }
}
