package com.scienstechnologies.openup.fragments.Mylife;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.isseiaoki.simplecropview.CropImageView;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.MainActivity;
import com.scienstechnologies.openup.others.RetroHelper;
import com.scienstechnologies.openup.others.ServiceHelper;
import com.scienstechnologies.openup.others.StringConstants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by Dell on 4/27/2016.
 */
public class Mylifecropimage extends Fragment {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE = 1;
    FragmentTransaction fragmentTransaction;
    ProgressDialog mProgressDialog;
    EditText title,content;

    ImageView create,crop;
    Button retake,share;
    File file;
    String userId;
    Bitmap bitmap;
    TextView save;

    public static Bitmap bmp;
    public static Mylifecropimage newInstance(Bitmap bitmap){
        bmp = bitmap;
        Mylifecropimage mylifecropimage = new Mylifecropimage();
        return mylifecropimage;
    }
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mylife_crop_image, container, false);
        create = (ImageView) v.findViewById(R.id.imageView5_mylife);
        final CropImageView cropImageView = (CropImageView)v.findViewById(R.id.cropImageView_mylife);
        bitmap = getArguments().getParcelable("Bitmap");

        title = (EditText)v.findViewById(R.id.et_life_title);
        content = (EditText)v.findViewById(R.id.et_life_content);


        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        createAndWriteFiles(bmp);
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                createAndWriteFiles(bmp);


            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE);

                // MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }


        if(bitmap != null){
            create.setImageBitmap(bitmap);
        }
        else {
            Toast.makeText(getActivity(), "Bitmap is null!", Toast.LENGTH_SHORT).show();
        }
        crop = (ImageView)v.findViewById(R.id.iv_crop);
        crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                create.setVisibility(View.GONE);
                cropImageView.setVisibility(View.VISIBLE);
                cropImageView.setImageBitmap(bitmap);
            }
        });
        save = (TextView) v.findViewById(R.id.crop_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                create.setVisibility(View.VISIBLE);
                cropImageView.setVisibility(View.GONE);
                create.setImageBitmap(cropImageView.getCroppedBitmap());
                bitmap = cropImageView.getCroppedBitmap();
            }
        });
        retake = (Button) v.findViewById(R.id.iv_retake_img_mulife);
        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_mylife, new MyLifeImageFragment());
                fragmentTransaction.commit();
            }
        });
        share = (Button) v.findViewById(R.id.iv_share_mylife);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (content.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Enter your content", Toast.LENGTH_LONG).show();
                } else {
                    //create a file to write bitmap data
                    file = new File(getActivity().getCacheDir(), "file");
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(file);
                        fos.write(bitmapdata);
                        fos.flush();
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (isNetworkConnected()) {
                        mProgressDialog.show();


                        MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();

                        multipartTypedOutput.addPart("image", new TypedFile("image/*", file));
                        multipartTypedOutput.addPart("userid", new TypedString(userId));
                        multipartTypedOutput.addPart("catid", new TypedString("4"));
                        multipartTypedOutput.addPart("posttype", new TypedString("2"));
                        multipartTypedOutput.addPart("title", new TypedString(title.getText().toString()));
                        multipartTypedOutput.addPart("content", new TypedString(content.getText().toString()));

                        getBaseClassService().UploadImage(multipartTypedOutput, new Callback<JsonObject>() {

                            @Override
                            public void success(JsonObject jsonObject, retrofit.client.Response response) {
                                Log.e("KAR", "response ::" + response);
                                getActivity().finishAffinity();
                                mProgressDialog.dismiss();
                                Intent i = new Intent(getActivity(), MainActivity.class);
                                startActivity(i);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                mProgressDialog.dismiss();
                                Toast.makeText(getActivity(), "Upload Unsuccessful,please try again", Toast.LENGTH_SHORT).show();
                            }
                        });


                    } else {
                        Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        return v;
    }

    private void createAndWriteFiles(Bitmap bmp) {

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }

    public ServiceHelper getBaseClassService() {
        return new RetroHelper().getAdapter().create(ServiceHelper.class);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // storage-related task you need to do.

                    if(bmp !=null){
                        createAndWriteFiles(bmp);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



}
