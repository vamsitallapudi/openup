package com.scienstechnologies.openup.fragments.quoteFuzz;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.*;
import com.scienstechnologies.openup.models.Quote;
import com.scienstechnologies.openup.others.CustomDialogActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home on 3/12/2016.
 */
public class QuoteFragment extends Fragment {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE = 1;
    FragmentTransaction fragmentTransaction;
    ImageView help;
    private ImageView text,image;
    List<Quote> quoteList = new ArrayList<>();
    private QuoteListAdapter quoteListAdapter;
    private ListView listView;
    String[] dp_name = new String[]{"santhu","vamshi","vijay"};
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.quote_main_activity, container, false);
        text = (ImageView)v.findViewById(R.id.ll_quote_text);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_quote, new QuoteFuzzPopup());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        image = (ImageView)v.findViewById(R.id.ll_quote_image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE);
                }else{
                    fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container_quote, new QuotePopupImage());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
        });
        return v;

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!(getActivity().getSharedPreferences("visiteStatus", Context.MODE_PRIVATE)).getBoolean("quote",false)){
            Intent i = new Intent(getActivity(), CustomDialogActivity.class);
            i.putExtra("title","Quote Fuzz");
            i.putExtra("des",getActivity().getResources().getString(R.string.quotedes));
            i.putExtra("modaltype","quote");
            getActivity().startActivity(i);
        }
    }
}
