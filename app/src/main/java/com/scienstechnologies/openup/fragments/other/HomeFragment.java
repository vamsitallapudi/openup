package com.scienstechnologies.openup.fragments.other;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.activities.AskmeActivity;
import com.scienstechnologies.openup.activities.InspireActivity;
import com.scienstechnologies.openup.activities.MyLifeActivity;
import com.scienstechnologies.openup.activities.PopupGoldActivity;
import com.scienstechnologies.openup.activities.QuoteFuzzActivity;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.ProfileAdapter;
import com.scienstechnologies.openup.models.Profile;
import com.scienstechnologies.openup.others.ScrollViewExt;
import com.scienstechnologies.openup.others.ScrollViewListener;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Home on 3/10/2016.
 */
public class HomeFragment extends Fragment {
    long elapsed;
    String time;
    int pageCount = 1,mCount = 0;
    private static final String url = StringConstants.BASEURL + "SlideImagesbasedcat?cateid=4";

    private ProfileAdapter profileAdapter;
    ListView listView;
    ScrollViewExt scrollView;
    List<Profile> profileList = new ArrayList<>();
    ProgressDialog mProgressDialog;

    ArrayList<String> imageUrl = new ArrayList<>();
    HashMap<String, String> url_maps;
    private SliderLayout mDemoSlider;
    TextView inspire, mylife, quote_fuzz, ask_me;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        listView = (ListView) v.findViewById(R.id.home_list);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);


        scrollView = (ScrollViewExt) v.findViewById(R.id.scrollView);
        mDemoSlider = (SliderLayout) v.findViewById(R.id.slider_home);
        inspire = (TextView) v.findViewById(R.id.tv_inspire);
        inspire.setOnClickListener(new View.OnClickListener()

                                   {
                                       @Override
                                       public void onClick(View v) {
                                           Intent i = new Intent(getActivity(), InspireActivity.class);
                                           startActivity(i);
                                       }
                                   }

        );
        mylife = (TextView) v.findViewById(R.id.tv_my_life);
        mylife.setOnClickListener(new View.OnClickListener()

                                  {
                                      @Override
                                      public void onClick(View v) {
                                          Intent i = new Intent(getActivity(), MyLifeActivity.class);
                                          startActivity(i);
                                      }
                                  }

        );
        quote_fuzz = (TextView) v.findViewById(R.id.tv_quote);
        quote_fuzz.setOnClickListener(new View.OnClickListener()

                                      {
                                          @Override
                                          public void onClick(View v) {
                                              Intent i = new Intent(getActivity(), QuoteFuzzActivity.class);
                                              startActivity(i);
                                          }
                                      }

        );
        ask_me = (TextView) v.findViewById(R.id.tv_ask_me);
        ask_me.setOnClickListener(new View.OnClickListener()

                                  {
                                      @Override
                                      public void onClick(View v) {
                                          Intent i = new Intent(getActivity(), AskmeActivity.class);
                                          startActivity(i);
                                      }
                                  }

        );
        if (isNetworkConnected()) {
            mProgressDialog.show();
            StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            url_maps = new HashMap<String, String>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                url_maps.put("Inspire World" + i, jsonObject1.getString("url"));
                                imageUrl.add(jsonObject1.getString("url"));
                            }

                            for (String name : url_maps.keySet()) {

                                final DefaultSliderView textSliderView = new DefaultSliderView(getActivity());

                                //initialize a sliderlayout`
                                textSliderView.image(url_maps.get(name))
                                        .setScaleType(BaseSliderView.ScaleType.Fit);

                                textSliderView.bundle(new Bundle());
                                mDemoSlider.addSlider(textSliderView);
                            }
                            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            //mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                            mDemoSlider.setDuration(3000);
                            mDemoSlider.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                                @Override
                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                }

                                @Override
                                public void onPageSelected(int position) {
                                }

                                @Override
                                public void onPageScrollStateChanged(int state) {
                                }
                            });
                        } else if (status == 0) {
                            Toast.makeText(getActivity(), "null", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("About", error.toString());

                    try {
                        Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        }else {
            Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
        }


        getPostDataFromNetwork();
        return v;
    }

    private void getPostDataFromNetwork() {
        if (isNetworkConnected()) {

            mProgressDialog.show();
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
            String userId = sharedPreferences.getString("userId", "");

            String url = StringConstants.BASEURL + "homeList?UserId=" + userId + "&page=" + pageCount + "&limit=20";

            StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            try {
                                JSONObject object = jsonObject.getJSONObject("user_data");
                                String count = object.getString("GoldMedalPopupStatus");
                                if (count.equals("1")) {
                                    mProgressDialog.dismiss();
                                    Intent intent = new Intent(getActivity(), PopupGoldActivity.class);
                                    startActivity(intent);
                                }
                            } catch (Exception e) {

                            }

                            try {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    Log.d("jsonObject1", jsonObject1.toString());
                                    Profile profile1 = new Profile();
                                    profile1.setPostTypeId(jsonObject1.getString("postTypeId"));
                                    profile1.setPostCategoryId(jsonObject1.getString("postCategory"));

                                    try {
                                        profile1.setContent(jsonObject1.getString("content"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setContent("");
                                    }
                                    try {
                                        profile1.setSubCategory(jsonObject1.getString("postSubScategory"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setSubCategory("");
                                    }
                                    try {
                                        String gold = jsonObject1.getString("noOfGoldMedals");
                                        if (gold.equals("null")) {
                                            profile1.setGold("0");
                                        } else {
                                            profile1.setGold(gold);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setGold("");
                                    }
                                    try {
                                        String silver = jsonObject1.getString("noOfSilverMedals");
                                        if (silver.equals("null")) {
                                            profile1.setSilver("0");
                                        } else {
                                            profile1.setSilver(silver);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setSilver("");
                                    }
                                    try {
                                        String copper = jsonObject1.getString("noOfBronzeMedals");
                                        if (copper.equals("null")) {
                                            profile1.setCopper("0");
                                        } else {
                                            profile1.setCopper(copper);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setCopper("");
                                    }
                                    try {
                                        profile1.setTitle(jsonObject1.getString("title"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setTitle("");
                                    }
                                    try {
                                        long date = Long.parseLong(jsonObject1.getString("time_difference"));
                                        dateFormat(date);
                                        profile1.setTimer(String.valueOf(time));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setUserName(jsonObject1.getString("firstName"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setUserName("");
                                    }
                                    try {
                                        profile1.setProfilePic(jsonObject1.getString("profile_pic"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    profile1.setImage(jsonObject1.getString("contentUrl"));
                                    try {
                                        profile1.setVideo(jsonObject1.getString("video_player_img"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setPostId(jsonObject1.getString("id"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setLikeCount(jsonObject1.getString("LikesCount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setCommentCount(jsonObject1.getString("CommentsCount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setFavStatus(jsonObject1.getBoolean("FavouriteStatus"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setLikeStatus(jsonObject1.getBoolean("LikeStatus"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setSubCatName(jsonObject1.getString("subCategoryName"));
                                    } catch (Exception e) {

                                    }
                                    profile1.setHashtag(jsonObject1.getString("hashtag"));
                                    try {
                                        profile1.setGoldLike(jsonObject1.getInt("goldLikes"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setSharedname(jsonObject1.getString("shared_username"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setShareType(jsonObject1.getString("SharedType"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        String quoteType = jsonObject1.getString("quotetype");
                                        if (quoteType.equals("null")){
                                            profile1.setQuoteType("");
                                        }else {
                                            profile1.setQuoteType(quoteType);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setUserId(jsonObject1.getString("userId"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        JSONArray jsonArray1 = jsonObject1.getJSONArray("answers");
                                        profile1.setLength(jsonArray1.length());
                                        for (int j = 0; j < jsonArray1.length(); j++) {
                                            JSONObject object = jsonArray1.getJSONObject(j);
                                            profile1.setAnswer(object.getString("comments"));
                                        }
                                    } catch (Exception e) {

                                    }
                                    profileList.add(profile1);
                                }
                            } catch (Exception e) {

                            }

                            if (pageCount == 1) {
                                profileAdapter = new ProfileAdapter(getActivity(), profileList);
                                listView.setAdapter(profileAdapter);
                            } else {
                                profileAdapter.notifyDataSetChanged();
                            }

                            setListViewHeightBasedOnChildren(listView);

                            scrollView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (pageCount == 1) {
                                        scrollView.fullScroll(View.FOCUS_UP);
                                        scrollView.scrollTo(0, 0);
                                    }
                                    mProgressDialog.dismiss();
                                }
                            }, 1000);


                        } else if (status == 0) {
                            mProgressDialog.dismiss();
                            Toast.makeText(getActivity(), "No posts", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    try {
                        Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        }else {
            Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
        }

        scrollView.setScrollViewListener(new ScrollViewListener() {
            @Override
            public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
                View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                // if diff is zero, then the bottom has been reached
                if (diff == 0) {

                    pageCount = pageCount + 1;
                    getPostDataFromNetwork();

//                    mCount = listView.getAdapter().getCount();
//                    Toast.makeText(getActivity(), "count"+ mCount, Toast.LENGTH_SHORT).show();
//
//                    if (mCount % 20 == 1 ){
//                        mCount = mCount-1;
//                    }
//                    if (mCount % 20 == 0) {
//                        pageCount = pageCount + 1;
//                        getPostDataFromNetwork();
//                    }
                }
            }
        });


    }


    private void dateFormat(long date) {


        //milliseconds
        long different = date;
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        if (different >= 86400000) {
            long str = different / daysInMilli;
            if (str > 365) {
                elapsed = str / 365;
                time = String.valueOf(elapsed + "yr");
            } else if (str > 31) {
                elapsed = str / 31;
                time = String.valueOf(elapsed + "mon");
            } else {
                elapsed = str;
                time = String.valueOf(elapsed + "d");
            }
        } else if (different >= 3600000) {
            elapsed = different / hoursInMilli;
            time = String.valueOf(elapsed + "h");
        } else if (different >= 60000) {
            elapsed = different / minutesInMilli;
            time = String.valueOf(elapsed + "min");
        } else if (different >= 6000) {
            elapsed = different / secondsInMilli;
            time = String.valueOf(elapsed + "s");
        } else {
            time = String.valueOf("1s");
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    public void onResume() {
        super.onResume();
    }


}
