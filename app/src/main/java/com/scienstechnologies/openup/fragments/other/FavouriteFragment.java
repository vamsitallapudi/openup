package com.scienstechnologies.openup.fragments.other;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.adapters.ProfileAdapter;
import com.scienstechnologies.openup.models.Profile;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Sciens on 5/23/2016.
 */
public class FavouriteFragment extends Fragment {
    private ProfileAdapter profileAdapter;
    ListView listView;
    ProgressDialog mProgressDialog;
    List<Profile> profileList = new ArrayList<>();
    String time;
    long elapsed;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.favourite_layout, container, false);

        listView = (ListView) v.findViewById(R.id.favourite_list);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        getPostDataFromNetwork();

        return v;
    }

    private void getPostDataFromNetwork() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "");
        String URL = StringConstants.BASEURL + "favoritePostList?UserId=" + userId;
        StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");

                    if (status == 1) {
                        String msg = jsonObject.getString("message");
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                        try {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                Profile profile1 = new Profile();
                                profile1.setPostTypeId(jsonObject1.getString("postTypeId"));
                                profile1.setPostCategoryId(jsonObject1.getString("postCategory"));

                                try {
                                    profile1.setContent(jsonObject1.getString("content"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    profile1.setContent("");
                                }
                                try {
                                    profile1.setSubCategory(jsonObject1.getString("postSubScategory"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    profile1.setSubCategory("");
                                }
                                try {
                                    profile1.setGold(jsonObject1.getString("noOfGoldMedals"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    profile1.setGold("");
                                }
                                try {
                                    profile1.setSilver(jsonObject1.getString("noOfSilverMedals"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    profile1.setSilver("");
                                }
                                try {
                                    profile1.setCopper(jsonObject1.getString("noOfBronzeMedals"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    profile1.setCopper("");
                                }
                                try {
                                    profile1.setTitle(jsonObject1.getString("title"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    profile1.setTitle("");
                                }
                                try {
                                    long date = Long.parseLong(jsonObject1.getString("time_difference"));
                                    dateFormat(date);
                                    profile1.setTimer(String.valueOf(time));
                                } catch (Exception e) {

                                }
                                try {
                                    profile1.setUserName(jsonObject1.getString("firstName"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    profile1.setUserName("");
                                }
                                try {
                                    profile1.setProfilePic(jsonObject1.getString("profile_pic"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    profile1.setImage(jsonObject1.getString("contentUrl"));
                                } catch (Exception e) {

                                }

                                try {
                                    profile1.setVideo(jsonObject1.getString("video_player_img"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    profile1.setPostId(jsonObject1.getString("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    profile1.setLikeCount(jsonObject1.getString("LikesCount"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    profile1.setCommentCount(jsonObject1.getString("CommentsCount"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    profile1.setFavStatus(jsonObject1.getBoolean("FavouriteStatus"));
                                } catch (Exception e) {

                                }
                                try {
                                    profile1.setLikeStatus(jsonObject1.getBoolean("LikeStatus"));
                                } catch (Exception e) {

                                }
                                try {
                                    profile1.setSubCatName(jsonObject1.getString("subCategoryName"));
                                } catch (Exception e) {

                                }
                                try {
                                    profile1.setSharedname(jsonObject1.getString("shared_username"));
                                } catch (Exception e) {

                                }
                                try {
                                    profile1.setShareType(jsonObject1.getString("SharedType"));
                                } catch (Exception e) {
                                    profile1.setShareType("");
                                }

                                try {
                                    profile1.setGoldLike(jsonObject1.getInt("goldLikes"));
                                } catch (Exception e) {

                                }
                                try {
                                    profile1.setHashtag(jsonObject1.getString("hashtag"));
                                } catch (Exception e) {
                                    profile1.setHashtag("null");
                                }
                                profileList.add(profile1);
                                mProgressDialog.dismiss();
                            }
                        } catch (Exception e) {

                        }


                        profileAdapter = new ProfileAdapter(getActivity(), profileList);
                        listView.setAdapter(profileAdapter);
//                        setListViewHeightBasedOnChildren(listView);
//                        scrollView.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                scrollView.fullScroll(View.FOCUS_UP);
//                                scrollView.scrollTo(0,0);
//                                mProgressDialog.dismiss();
//                            }
//                        },1000);

                        mProgressDialog.dismiss();
                    } else if (status == 0) {
                        Toast.makeText(getActivity(), "null", Toast.LENGTH_LONG).show();
                        mProgressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    mProgressDialog.dismiss();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mProgressDialog.dismiss();

            }
        });
        AppController.getInstance().addToRequestQueue(sr);
    }

    private void dateFormat(long date) {
        //milliseconds
        long different = date;
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        if (different >= 86400000) {
            long str = different / daysInMilli;
            if (str > 365) {
                elapsed = str / 365;
                time = String.valueOf(elapsed + "yr");
            } else if (str > 31) {
                elapsed = str / 31;
                time = String.valueOf(elapsed + "mon");
            } else {
                elapsed = str;
                time = String.valueOf(elapsed + "d");
            }
        } else if (different >= 3600000) {
            elapsed = different / hoursInMilli;
            time = String.valueOf(elapsed + "h");
        } else if (different >= 60000) {
            elapsed = different / minutesInMilli;
            time = String.valueOf(elapsed + "min");
        } else if (different >= 6000) {
            elapsed = different / secondsInMilli;
            time = String.valueOf(elapsed + "s");
        } else {
            time = String.valueOf("1s");
        }
    }
}
