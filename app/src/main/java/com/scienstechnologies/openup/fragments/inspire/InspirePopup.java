package com.scienstechnologies.openup.fragments.inspire;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.*;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.adapters.CategoriesAdapter;
import com.scienstechnologies.openup.models.CategoriesModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home on 3/9/2016.
 */
public class InspirePopup extends Fragment {
    ProgressDialog mProgressDialog;
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    TextView mywords, inspire_words;
    Bundle args;
    String category;
    ImageView close;

    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.inspire_popup, container, false);
        args = new Bundle();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);


        mywords = (TextView) v.findViewById(R.id.tv_myword);
        mywords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category = "mywords";
                mywords.setBackgroundColor(Color.parseColor("#21a4dc"));
                inspire_words.setBackgroundColor(Color.parseColor("#424242"));
                WriteFragment writeFragment = new WriteFragment();
                args.putString("life_key", category);
                writeFragment.setArguments(args);


                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.inspire_container, writeFragment);
                fragmentTransaction.commit();
            }
        });
        inspire_words = (TextView) v.findViewById(R.id.tv_inspirewords);
        inspire_words.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.inspire_container, new ListViewFragment());
                fragmentTransaction.commit();
            }
        });
        close = (ImageView) v.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.inspire_container, new InspireFragment());
                fragmentTransaction.commit();
            }
        });
        return v;
    }

}
