package com.scienstechnologies.openup.fragments.inspire;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AdminImagesActivity;
import com.scienstechnologies.openup.fragments.other.AdminImagesFragment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Home on 3/3/2016.
 */
public class InspireImageFragment extends Fragment {
    final static int REQUEST_VIDEO_CAPTURED = 1;
    private int PICK_IMAGE_REQUEST = 1;
    FragmentTransaction fragmentTransaction;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    ImageView create, upload;
    Bitmap bitmap;
    Bundle b;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen12, container, false);
        create = (ImageView) v.findViewById(R.id.iv_create);

        upload = (ImageView) v.findViewById(R.id.iv_upload);
        b = new Bundle();
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Bundle b = new Bundle();
//                AdminImagesFragment adminImagesFragment = new AdminImagesFragment();
//                b.putString("imageType","1");
//                adminImagesFragment.setArguments(b);
//                fragmentTransaction = getFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.inspire_container, adminImagesFragment);
//                fragmentTransaction.commit();

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,
                        CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {

            //  public static final int ACTION_REQUEST_GALLERY = 1888;

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
// Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }

        });
        return v;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                // convert byte array to Bitmap
                bitmap = BitmapFactory.decodeByteArray(byteArray, 0,
                        byteArray.length);

                create.setImageBitmap(bitmap);
                b.putParcelable("Bitmap", bitmap);

                CreateImageFragment createImageFragment = new CreateImageFragment();
                createImageFragment.setArguments(b);
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.inspire_container, createImageFragment);
                fragmentTransaction.commit();

            }
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                b.putParcelable("Bitmap", bitmap);
                upload.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }

            CreateImageFragment createImageFragment = new CreateImageFragment();
            createImageFragment.setArguments(b);
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.inspire_container, createImageFragment);
            fragmentTransaction.commit();
        }

    }



    @Override
    public void onResume() {
        super.onResume();
    }


}