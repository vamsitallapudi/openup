package com.scienstechnologies.openup.fragments.Mylife;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.scienstechnologies.openup.R;

/**
 * Created by Dell on 4/23/2016.
 */
public class Mylifevideofragment extends android.support.v4.app.Fragment {
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    private static final int PICK_FROM_GALLERY = 1;

    final static int REQUEST_VIDEO_CAPTURED = 1;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    Bundle b;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mylife_videoactivity, container, false);
        ImageView create_video = (ImageView)v.findViewById(R.id.iv_create_video_mylife);
        ImageView upload_video = (ImageView)v.findViewById(R.id.iv_upload_video_mylife);
        b = new Bundle();
        upload_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_GALLERY);
            }
        });

        create_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(intent, REQUEST_VIDEO_CAPTURED);
            }
        });
        return v;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_VIDEO_CAPTURED) {

                Uri videoUri = data.getData();
                b.putParcelable("video", videoUri);

                MylifeVideofragemnt mylifeVideofragemnt = new MylifeVideofragemnt();
                mylifeVideofragemnt.setArguments(b);
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_mylife, mylifeVideofragemnt);
                fragmentTransaction.commit();
            }
        }
        if (resultCode != Activity.RESULT_OK) return;

        if (requestCode == PICK_FROM_GALLERY) {
            Uri videoUri = data.getData();
            b.putParcelable("video", videoUri);

            MylifeVideofragemnt mylifeVideofragemnt = new MylifeVideofragemnt();
            mylifeVideofragemnt.setArguments(b);
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_mylife, mylifeVideofragemnt);
            fragmentTransaction.commit();
        }
    }

    public void onResume() {
        super.onResume();
    }


}
