package com.scienstechnologies.openup.fragments.inspire;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.isseiaoki.simplecropview.CropImageView;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.MainActivity;
import com.scienstechnologies.openup.others.RetroHelper;
import com.scienstechnologies.openup.others.ServiceHelper;
import com.scienstechnologies.openup.others.StringConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by Home on 3/9/2016.
 */
public class CreateImageFragment extends Fragment {
    ProgressDialog mProgressDialog;
    FragmentTransaction fragmentTransaction;
    ImageView create, crop;
    Button share, retake;
    File file;
    String userId, content;
    TextInputLayout title;
    EditText tv_title;
    public static Bitmap bmp;
    Bitmap bitmap;
    TextView save;

    public static CreateImageFragment newInstance(Bitmap bitmap) {
        bmp = bitmap;

        CreateImageFragment createImageFragment = new CreateImageFragment();
        return createImageFragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen13, container, false);
        create = (ImageView) v.findViewById(R.id.imageView5);
        final CropImageView cropImageView = (CropImageView) v.findViewById(R.id.cropImageView);
        title = (TextInputLayout) v.findViewById(R.id.txt1);
        tv_title = (EditText) v.findViewById(R.id.et_inspire_title);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        bitmap = getArguments().getParcelable("Bitmap");


        if (bitmap != null) {
            create.setImageBitmap(bitmap);
        } else {
            Toast.makeText(getActivity(), "Bitmap is null!", Toast.LENGTH_SHORT).show();
        }
        crop = (ImageView) v.findViewById(R.id.iv_crop);
        crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                create.setVisibility(View.GONE);
                cropImageView.setVisibility(View.VISIBLE);
                cropImageView.setImageBitmap(bitmap);
            }
        });
        save = (TextView) v.findViewById(R.id.crop_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                create.setVisibility(View.VISIBLE);
                cropImageView.setVisibility(View.GONE);
                create.setImageBitmap(cropImageView.getCroppedBitmap());
                bitmap = cropImageView.getCroppedBitmap();
            }
        });
        retake = (Button) v.findViewById(R.id.iv_retake_img);
        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.inspire_container, new InspireImageFragment());
                fragmentTransaction.commit();
            }
        });
        share = (Button) v.findViewById(R.id.iv_share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content = tv_title.getText().toString();
                if (content.equals("")) {
                    Toast.makeText(getActivity(), "Please add your content", Toast.LENGTH_SHORT).show();
                } else {
                    //create a file to write bitmap data
                    file = new File(getActivity().getCacheDir(), "file");
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(file);
                        fos.write(bitmapdata);
                        fos.flush();
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (isNetworkConnected()) {
                        mProgressDialog.show();


                        MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();

                        multipartTypedOutput.addPart("image", new TypedFile("image/*", file));
                        multipartTypedOutput.addPart("userid", new TypedString(userId));
                        multipartTypedOutput.addPart("catid", new TypedString("1"));
                        multipartTypedOutput.addPart("posttype", new TypedString("2"));
                        multipartTypedOutput.addPart("content", new TypedString(content));

                        getBaseClassService().UploadImage(multipartTypedOutput, new Callback<JsonObject>() {

                            @Override
                            public void success(JsonObject jsonObject, retrofit.client.Response response) {
                                Log.e("KAR", "response ::" + response);
                                getActivity().finishAffinity();
                                mProgressDialog.dismiss();
                                Intent i = new Intent(getActivity(), MainActivity.class);
                                startActivity(i);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Log.e("KAR", "error ::" + error.getResponse().toString());
                                mProgressDialog.dismiss();
                                Toast.makeText(getActivity(), "Upload Unsuccessful,please try again", Toast.LENGTH_SHORT).show();
                            }
                        });


                    } else {
                        Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        return v;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }

    public ServiceHelper getBaseClassService() {
        return new RetroHelper().getAdapter().create(ServiceHelper.class);
    }
}
