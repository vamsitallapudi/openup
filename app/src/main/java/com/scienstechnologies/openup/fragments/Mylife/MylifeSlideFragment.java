package com.scienstechnologies.openup.fragments.Mylife;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.scienstechnologies.openup.R;

/**
 * Created by Home on 3/15/2016.
 */
public class MylifeSlideFragment extends Fragment {
    int position;
    int introImages[] = {R.drawable.alerts_36,R.drawable.alerts_36,R.drawable.alerts_36,R.drawable.alerts_36};

    public static MylifeSlideFragment newInstance(int position){
        Bundle args = new Bundle();
        args.putInt("position",position);
        MylifeSlideFragment mylifeSlideFragment = new MylifeSlideFragment();
        mylifeSlideFragment.setArguments(args);
        return mylifeSlideFragment;
    }
    public MylifeSlideFragment(){

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v = inflater.inflate(R.layout.intro_fragment,container,false);

        position = (int) getArguments().getInt("position");

        LinearLayout llIntroFragment = (LinearLayout) v.findViewById(R.id.llIntroFragment);
        llIntroFragment.setBackgroundResource(introImages[position]);

        return v;
    }
    public void onResume() {
        super.onResume();
    }
}
