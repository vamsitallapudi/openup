package com.scienstechnologies.openup.fragments.other;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.activities.CommentsActivity;
import com.scienstechnologies.openup.activities.FollowersActivity;
import com.scienstechnologies.openup.activities.OpenupFirstActivity;
import com.scienstechnologies.openup.activities.UpdateProfileActivity;
import com.scienstechnologies.openup.activities.VideoActivity;
import com.scienstechnologies.openup.adapters.ProfileAdapter;
import com.scienstechnologies.openup.models.Profile;
import com.scienstechnologies.openup.others.ScrollViewExt;
import com.scienstechnologies.openup.others.ScrollViewListener;
import com.scienstechnologies.openup.others.StringConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * Created by santhu on 3/16/2016.
 */
public class MyProfileFragment extends Fragment {
    String time;
    long elapsed;
    ProgressDialog mProgressDialog;
    private ViewPager mViewPager;
    private ProfileAdapter profileAdapter;
    ListView listView;
    ScrollViewExt scrollView;
    List<Profile> profileList = new ArrayList<>();
    String user, userId, userData;
    private final static String url = StringConstants.BASEURL + "send_request";
    TextView profileName, location, address, goldLike, silverLike, copperLike, follower, following, myposts,edit;
    ImageView profilePic,followTV;
    LinearLayout fallow, posts, followers,followingUser;
    String data;
    int pageCount = 1,mCount;
    String followingCount = String.valueOf("0"),followerCount= String.valueOf("0");

    private boolean followStatus;
    public boolean isFollowStatus() {
        return followStatus;
    }
    public void setFollowStatus(boolean followStatus) {
        this.followStatus = followStatus;
    }

    private DisplayImageOptions logooptions;
    ImageLoader imageLoader;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_myprofile_fragment, container, false);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");
        edit = (TextView) v.findViewById(R.id.tv_edit_profile);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UpdateProfileActivity.class);
                startActivity(intent);
            }
        });

        // Initialize ImageLoader with configuration.
        logooptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_profile)
                .showImageForEmptyUri(R.drawable.no_profile)
                .displayer(new RoundedBitmapDisplayer(10))
                .cacheOnDisk(true)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .defaultDisplayImageOptions(logooptions)
                .denyCacheImageMultipleSizesInMemory()
                .writeDebugLogs()
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        if (getArguments() == null) {
            userData = sharedPreferences.getString("userId", "");
            data = userData;
        } else {
            data = getArguments().getString("userId");
            userData = userId+"&myprofile=0&ProfileId="+data;
        }

        profileName = (TextView) v.findViewById(R.id.tv_profile_name);
        location = (TextView) v.findViewById(R.id.tv_location);
        address = (TextView) v.findViewById(R.id.tv_address);
        goldLike = (TextView) v.findViewById(R.id.tv_profile_goldlike);
        silverLike = (TextView) v.findViewById(R.id.tv_profile_silverlike);
        copperLike = (TextView) v.findViewById(R.id.tv_profile_copperlike);
        profilePic = (ImageView) v.findViewById(R.id.iv_profilepic);
        follower = (TextView) v.findViewById(R.id.tv_followers);
        following = (TextView) v.findViewById(R.id.tv_following);
        myposts = (TextView) v.findViewById(R.id.tv_myposts);
        scrollView = (ScrollViewExt) v.findViewById(R.id.scrollView);
        fallow = (LinearLayout) v.findViewById(R.id.ll_fallow);
        followTV = (ImageView) v.findViewById(R.id.tv_follow);
        posts = (LinearLayout) v.findViewById(R.id.ll_posts);
        followers = (LinearLayout) v.findViewById(R.id.ll_followers);
        followingUser = (LinearLayout) v.findViewById(R.id.ll_following);


        listView = (ListView) v.findViewById(R.id.profile_list);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        final LinearLayout profile = (LinearLayout) v.findViewById(R.id.ll_profile);
        final LinearLayout address = (LinearLayout) v.findViewById(R.id.ll_address);


        mViewPager = (ViewPager) v.findViewById(R.id.profile_view_pager);
        FragmentManager fragmentManager = getChildFragmentManager();
        final MyAdapter myAdapter = new MyAdapter(fragmentManager);
        mViewPager.setAdapter(myAdapter);
        final DetailOnPageChangeListener mDetailOnPageChangeListener = new DetailOnPageChangeListener();

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 0) {
                    profile.setVisibility(View.VISIBLE);
                    address.setVisibility(View.GONE);

                } else {
                    profile.setVisibility(View.GONE);
                    address.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            public void onPageScrollStateChanged(int state) {

            }
        });

        followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (followingCount.equals("0")){
                    Toast.makeText(getActivity(),"No followers",Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(getActivity(), FollowersActivity.class);
                    intent.putExtra("Follower","list_of_followings?UserId=");
                    intent.putExtra("userId",data);
                    intent.putExtra("type","1");
                    startActivity(intent);
                }

            }
        });
        followingUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (followerCount.equals("0")){
                    Toast.makeText(getActivity(),"No user following",Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(getActivity(), FollowersActivity.class);
                    intent.putExtra("Follower","list_of_followers?UserId=");
                    intent.putExtra("userId",data);
                    intent.putExtra("type","2");
                    startActivity(intent);
                }

            }
        });


        fallow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFollowData();
            }
        });


        getPostDataFromNetwork();
        return v;
    }

    private void postFollowData() {

        if (isNetworkConnected()) {
            mProgressDialog.show();
            StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        if (status.equals("1")) {

                            if (isFollowStatus()){
                                setFollowStatus(false);
                                followTV.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.follow_new_user));

                            }else {
                                setFollowStatus(true);
                                followTV.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.following_new_user));
                            }
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();

                        } else if (status.equals("0")) {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressDialog.dismiss();
                    //  VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> map = new HashMap<>();
                    map.put("senderId",userId);
                    map.put("receiverId",data);
                    return map;

                }
            };
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();

        }
    }

    private void getPostDataFromNetwork() {
        if (isNetworkConnected()) {
            mProgressDialog.show();

            String URL = StringConstants.BASEURL + "PostDetailsList?UserId=" + userData + "&page=" + pageCount + "&limit=20";
            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            try {
                                JSONObject object = jsonObject.getJSONObject("user_data");

                                try {
                                    String name = object.getString("firstName");
                                    if (name.equals(null)) {
                                        profileName.setText(object.getString(""));
                                    }
                                    profileName.setText(object.getString("firstName"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    profileName.setText("");
                                }
                                try {
                                    String gold = object.getString("goldLikes");
                                    if (gold.equals("null")) {
                                        goldLike.setText(object.getString("0"));
                                    } else {
                                        goldLike.setText(gold);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    goldLike.setText("0");
                                }
                                try {
                                    String silver = object.getString("silverLikes");
                                    if (silver.equals("null")) {
                                        silverLike.setText(object.getString("0"));
                                    } else {
                                        silverLike.setText(silver);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    silverLike.setText("0");
                                }
                                try {
                                    String copper = object.getString("bronzeLikes");
                                    if (copper.equals("null")) {
                                        copperLike.setText(object.getString("0"));
                                    } else {
                                        copperLike.setText(copper);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    copperLike.setText("");
                                }
                                try {
                                    follower.setText(object.getString("Followings"));
                                    followingCount = object.getString("Followings");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    following.setText(object.getString("Followers"));
                                    followerCount = object.getString("Followers");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    setFollowStatus(object.getBoolean("FollowStatus"));

                                    if (isFollowStatus()){
                                        followTV.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.following_new_user));
                                    }else {
                                        followTV.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.follow_new_user));
                                    }

                                } catch (Exception e) {

                                }
                                try {
                                    location.setText(object.getString("country_name"));
                                } catch (Exception e) {

                                }
                                try {
                                    imageLoader.displayImage(object.getString("profile_pic"),profilePic,logooptions);
                                } catch (Exception e) {

                                }
                                user = object.getString("userId");
                                if (user.equals(userId)) {
                                    posts.setVisibility(View.VISIBLE);
                                    fallow.setVisibility(View.GONE);
                                    edit.setVisibility(View.VISIBLE);
                                } else {
                                    posts.setVisibility(View.GONE);
                                    edit.setVisibility(View.GONE);
                                    fallow.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {

                            }

                            try {
                                JSONArray arrayPosts = jsonObject.getJSONArray("postCount");
                                for (int i = 0; i < arrayPosts.length(); i++) {
                                    JSONObject object1 = arrayPosts.getJSONObject(i);
                                    try {
                                        myposts.setText(object1.getString("post_count"));
                                    } catch (Exception e) {

                                    }

                                }
                            } catch (Exception e) {

                            }

                            try {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    Profile profile1 = new Profile();
                                    profile1.setPostTypeId(jsonObject1.getString("postTypeId"));
                                    profile1.setPostCategoryId(jsonObject1.getString("postCategory"));

                                    try {
                                        profile1.setContent(jsonObject1.getString("content"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setContent("");
                                    }
                                    try {
                                        profile1.setSubCategory(jsonObject1.getString("postSubScategory"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setSubCategory("");
                                    }
                                    try {
                                        String gold = jsonObject1.getString("noOfGoldMedals");
                                        if (gold.equals("null")) {
                                            profile1.setGold("0");
                                        } else {
                                            profile1.setGold(gold);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setGold("");
                                    }
                                    try {
                                        String silver = jsonObject1.getString("noOfSilverMedals");
                                        if (silver.equals("null")) {
                                            profile1.setSilver("0");
                                        } else {
                                            profile1.setSilver(silver);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setSilver("");
                                    }
                                    try {
                                        String copper = jsonObject1.getString("noOfBronzeMedals");
                                        if (copper.equals("null")) {
                                            profile1.setCopper("0");
                                        } else {
                                            profile1.setCopper(copper);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setCopper("");
                                    }
                                    try {
                                        profile1.setTitle(jsonObject1.getString("title"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setTitle("");
                                    }
                                    try {
                                        long date = Long.parseLong(jsonObject1.getString("time_difference"));
                                        dateFormat(date);
                                        profile1.setTimer(String.valueOf(time));
                                    } catch (Exception e) {

                                    }
                                    profile1.setHashtag(jsonObject1.getString("hashtag"));
                                    try {
                                        profile1.setUserName(jsonObject1.getString("firstName"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setUserName("");
                                    }
                                    try {
                                        profile1.setProfilePic(jsonObject1.getString("profile_pic"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    profile1.setImage(jsonObject1.getString("contentUrl"));
                                    try {
                                        profile1.setVideo(jsonObject1.getString("video_player_img"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setPostId(jsonObject1.getString("id"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setLikeCount(jsonObject1.getString("LikesCount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setCommentCount(jsonObject1.getString("CommentsCount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setFavStatus(jsonObject1.getBoolean("FavouriteStatus"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setLikeStatus(jsonObject1.getBoolean("LikeStatus"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setGoldLike(jsonObject1.getInt("goldLikes"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setSubCatName(jsonObject1.getString("subCategoryName"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setSharedname(jsonObject1.getString("shared_username"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setShareType(jsonObject1.getString("SharedType"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        String quoteType = jsonObject1.getString("quotetype");
                                        if (quoteType.equals("null")){
                                            profile1.setQuoteType("");
                                        }else {
                                            profile1.setQuoteType(quoteType);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setUserId(jsonObject1.getString("userId"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        JSONArray jsonArray1 = jsonObject1.getJSONArray("answers");
                                        profile1.setLength(jsonArray1.length());
                                        for (int j = 0; j < jsonArray1.length(); j++) {
                                            JSONObject object = jsonArray1.getJSONObject(j);
                                            profile1.setAnswer(object.getString("comments"));
                                        }
                                    } catch (Exception e) {

                                    }
                                    profileList.add(profile1);

                                }
                            } catch (Exception e) {

                            }
                            if (pageCount == 1) {
                                profileAdapter = new ProfileAdapter(getActivity(), profileList);
                                listView.setAdapter(profileAdapter);
                            } else {
                                profileAdapter.notifyDataSetChanged();
                            }
                            setListViewHeightBasedOnChildren(listView);
                            scrollView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (pageCount == 1) {
                                        scrollView.fullScroll(View.FOCUS_UP);
                                        scrollView.scrollTo(0, 0);
                                    }
                                    mProgressDialog.dismiss();
                                }
                            }, 1000);
                        } else if (status == 0) {
                            mProgressDialog.dismiss();
                            Toast.makeText(getActivity(), "null", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        }else {
            Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
        }

        scrollView.setScrollViewListener(new ScrollViewListener() {
            @Override
            public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
                View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                // if diff is zero, then the bottom has been reached
                if (diff == 0) {

                    pageCount = pageCount + 1;
                    getPostDataFromNetwork();

//                    mCount = listView.getAdapter().getCount();
//
//                    if (mCount % 20 == 1 ){
//                        mCount = mCount-1;
//                    }
//                    if (mCount%20 == 0){
//                        pageCount = pageCount + 1;
//                        getPostDataFromNetwork();
//
//                    }
                }
            }
        });
    }

    private void dateFormat(long date) {

        //milliseconds
        long different = date;
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        if (different >= 86400000) {
            long str = different / daysInMilli;
            if (str > 365) {
                elapsed = str / 365;
                time = String.valueOf(elapsed + "yr");
            } else if (str > 31) {
                elapsed = str / 31;
                time = String.valueOf(elapsed + "mon");
            } else {
                elapsed = str;
                time = String.valueOf(elapsed + "d");
            }
        } else if (different >= 3600000) {
            elapsed = different / hoursInMilli;
            time = String.valueOf(elapsed + "h");
        } else if (different >= 60000) {
            elapsed = different / minutesInMilli;
            time = String.valueOf(elapsed + "min");
        } else if (different >= 6000) {
            elapsed = different / secondsInMilli;
            time = String.valueOf(elapsed + "s");
        } else {
            time = String.valueOf("1s");
        }
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.setLayoutParams(new ViewGroup.LayoutParams(0,0));
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);

    }

    private class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {
        private int currentPage;

        @Override
        public void onPageSelected(int position) {
            currentPage = position;
        }

        public final int getCurrentPage() {
            return currentPage;
        }


        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}
