package com.scienstechnologies.openup.fragments.Mylife;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.others.CustomDialogActivity;

/**
 * Created by Dell on 4/22/2016.
 */
public class MyLifeNewFragment extends android.support.v4.app.Fragment {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE = 1;
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    ImageView write,image,video;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mylife_main_activity, container, false);
        write = (ImageView)v.findViewById(R.id.write);
        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                write.setBackgroundColor(Color.parseColor("#17a553"));

                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_mylife, new MylifeWriteFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();



            }
        });
        image = (ImageView)v.findViewById(R.id.images);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                write.setBackgroundColor(Color.parseColor("#17a553"));

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE);
                }else{
                    fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container_mylife, new MyLifeImageFragment());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
        });
        video = (ImageView)v.findViewById(R.id.video);
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                write.setBackgroundColor(Color.parseColor("#17a553"));
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXT_STORAGE);
                }else{

                    fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container_mylife, new Mylifevideofragment());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }

            }
        });
        return v;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!(getActivity().getSharedPreferences("visiteStatus", Context.MODE_PRIVATE)).getBoolean("life",false)){
            Intent i = new Intent(getActivity(), CustomDialogActivity.class);
            i.putExtra("title","My Life");
            i.putExtra("des",getActivity().getResources().getString(R.string.lifedes));
            i.putExtra("modaltype","life");
            getActivity().startActivity(i);
        }
    }
}
