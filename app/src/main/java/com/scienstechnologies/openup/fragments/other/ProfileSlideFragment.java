package com.scienstechnologies.openup.fragments.other;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.scienstechnologies.openup.R;

/**
 * Created by Sciens on 5/16/2016.
 */
public class ProfileSlideFragment extends Fragment {
    int position;
    int profileImages[] = {R.drawable.cover_pic,R.drawable.cover_pic};

    public static ProfileSlideFragment newInstance(int position){
        Bundle args = new Bundle();
        args.putInt("position",position);
        ProfileSlideFragment profileSlideFragment= new ProfileSlideFragment();
        profileSlideFragment.setArguments(args);
        return profileSlideFragment;
    }
    public ProfileSlideFragment(){

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v = inflater.inflate(R.layout.intro_fragment,container,false);

        position = (int) getArguments().getInt("position");

        LinearLayout llIntroFragment = (LinearLayout) v.findViewById(R.id.llIntroFragment);
        llIntroFragment.setBackgroundResource(profileImages[position]);

        return v;
    }
    public void onResume() {
        super.onResume();
    }
}
