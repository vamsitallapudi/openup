package com.scienstechnologies.openup.fragments.other;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.scienstechnologies.openup.R;

/**
 * Created by Home on 3/12/2016.
 */
public class IntroFragment extends Fragment {
    int position;
    int introImages[] = {R.drawable.openupinspirescreen,R.drawable.openupquotescreen,R.drawable.openupaskscreen,R.drawable.openuplifescreen};

    public static IntroFragment newInstance(int position){
        Bundle args = new Bundle();
        args.putInt("position",position);
        IntroFragment introFragment= new IntroFragment();
        introFragment.setArguments(args);
        return introFragment;
    }
    public IntroFragment(){

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v = inflater.inflate(R.layout.intro_fragment,container,false);

        position = (int) getArguments().getInt("position");

        LinearLayout llIntroFragment = (LinearLayout) v.findViewById(R.id.llIntroFragment);
        llIntroFragment.setBackgroundResource(introImages[position]);

        return v;
    }
    public void onResume() {
        super.onResume();
    }
}
