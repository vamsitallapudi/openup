package com.scienstechnologies.openup.fragments.Mylife;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.MainActivity;
import com.scienstechnologies.openup.others.StringConstants;

import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Home on 3/11/2016.
 */
public class MylifeWriteFragment extends android.support.v4.app.Fragment {
    ProgressDialog mProgressDialog;
    String userId;
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    private Button submit;
    EditText et1, et2;
    private static final String URL = StringConstants.BASEURL + "insert_post_data";
    private static final String TAG = MylifeWriteFragment.class.getSimpleName();

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mylife_write, container, false);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        et1 = (EditText) v.findViewById(R.id.et_count);
        et2 = (EditText) v.findViewById(R.id.et_count2);
        submit = (Button) v.findViewById(R.id.button_life_write);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et1.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Enter your title", Toast.LENGTH_LONG).show();
                } else if (et2.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Write a content.", Toast.LENGTH_LONG).show();
                } else {
                    if (isNetworkConnected()) {
                        mProgressDialog.show();
                        StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d(TAG, response);
                                try {
                                    JSONObject jsonobject = new JSONObject(response);
                                    Log.d(TAG, response);
                                    String status = jsonobject.getString("status");
                                    if (status.equals("1")) {
                                        String message = jsonobject.getString("message");
                                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        getActivity().finishAffinity();
                                        mProgressDialog.dismiss();
                                        Intent i = new Intent(getActivity(), MainActivity.class);
                                        startActivity(i);
                                    } else if (status.equals("0")) {
                                        String message = jsonobject.getString("message");
                                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        mProgressDialog.dismiss();
                                        // Toast.makeText(getActivity(), "message", Toast.LENGTH_LONG).show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressDialog.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d(TAG, "Error: " + error.getMessage());
                                mProgressDialog.dismiss();
                            }
                        }) {
                            protected Map<String, String> getParams() {
                                Map<String, String> map = new HashMap<>();
                                map.put("userid", userId);
                                map.put("catid", "4");
                                map.put("posttype", "1");
                                map.put("title", et1.getText().toString());
                                map.put("content", et2.getText().toString());
                                return map;
                            }
                        };
                        AppController.getInstance().addToRequestQueue(sr);
                    } else {
                        Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        return v;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }
}
