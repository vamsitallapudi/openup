package com.scienstechnologies.openup.fragments.other;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.adapters.CommentAdapter;
import com.scienstechnologies.openup.adapters.ShareAdapter;
import com.scienstechnologies.openup.models.Comments;
import com.scienstechnologies.openup.models.ShareModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home on 3/10/2016.
 */
public class ShareFragment extends Fragment {
    ProgressDialog mProgressDialog;
    private ListView listView;
    private ShareAdapter shareAdapter;
    private List<ShareModel> shareModelList = new ArrayList<>();


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_share, container, false);
        listView = (ListView) v.findViewById(R.id.lv_share);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        getDataFromNetwork();

        return v;
    }

    private void getDataFromNetwork() {
        if (isNetworkConnected()) {
            mProgressDialog.show();
            String URL = StringConstants.BASEURL+"imagesList";

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            JSONArray array = jsonObject.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                ShareModel shareModel = new ShareModel();
                                try{
                                    shareModel.setImageId(object.getString("url"));
                                }catch (Exception e){

                                }
                                shareModelList.add(shareModel);
                            }
                            shareAdapter = new ShareAdapter(getActivity(), shareModelList);
                            listView.setAdapter(shareAdapter);
                            listView.deferNotifyDataSetChanged();
                            mProgressDialog.dismiss();
                        } else if (status == 0) {

                            Toast.makeText(getActivity(), "null", Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


}
