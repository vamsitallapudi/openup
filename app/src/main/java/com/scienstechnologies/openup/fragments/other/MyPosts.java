package com.scienstechnologies.openup.fragments.other;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.adapters.ProfileAdapter;
import com.scienstechnologies.openup.models.*;
import com.scienstechnologies.openup.models.Profile;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by anshu on 3/16/2016.
 */
public class MyPosts extends Fragment {
    private ProfileAdapter profileAdapter;
    ListView listView;
    ProgressDialog mProgressDialog;
    List<Profile> profileList = new ArrayList<>();
    String userId,time;
    long elapsed;
    int pageCount = 1,mCount;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.myposts, container, false);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        listView = (ListView) v.findViewById(R.id.myposts_list);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.show();

        getPostDataFromNetwork();

        //In your onScrollListener() of the list make the following changes
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView arg0, int arg1) {

            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
                if (totalItemCount > 0)
                {
                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    if(lastInScreen == totalItemCount)
                    {

                        pageCount = pageCount + 1;
                        getPostDataFromNetwork();

//                        mCount = listView.getAdapter().getCount();
//
//                        if (mCount % 20 == 1){
//                            mCount = mCount-1;
//                        }
//
//                        if (mCount % 20 == 0){
//                            pageCount = pageCount + 1;
//                            getPostDataFromNetwork();
//
//                        }
                    }
                }
            }
        });

        return v;
    }

    private void getPostDataFromNetwork() {
        if (isNetworkConnected()) {
            mProgressDialog.show();

            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
            String userId = sharedPreferences.getString("userId", "");
            String URL = StringConstants.BASEURL + "PostDetailsList?UserId=" + userId +"&page="+pageCount+"&limit=20";

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {

                            try {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    Profile profile1 = new Profile();
                                    profile1.setPostTypeId(jsonObject1.getString("postTypeId"));
                                    profile1.setPostCategoryId(jsonObject1.getString("postCategory"));

                                    try {
                                        profile1.setContent(jsonObject1.getString("content"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setContent("");
                                    }
                                    try {
                                        profile1.setSubCategory(jsonObject1.getString("postSubScategory"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setSubCategory("");
                                    }
                                    try {
                                        String gold = jsonObject1.getString("noOfGoldMedals");
                                        if (gold.equals("null")) {
                                            profile1.setGold("0");
                                        } else {
                                            profile1.setGold(gold);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setGold("");
                                    }
                                    try {
                                        String silver = jsonObject1.getString("noOfSilverMedals");
                                        if (silver.equals("null")) {
                                            profile1.setSilver("0");
                                        } else {
                                            profile1.setSilver(silver);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setSilver("");
                                    }
                                    try {
                                        String copper = jsonObject1.getString("noOfBronzeMedals");
                                        if (copper.equals("null")) {
                                            profile1.setCopper("0");
                                        } else {
                                            profile1.setCopper(copper);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setCopper("");
                                    }
                                    try {
                                        profile1.setTitle(jsonObject1.getString("title"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setTitle("");
                                    }
                                    try {
                                        long date = Long.parseLong(jsonObject1.getString("time_difference"));
                                        dateFormat(date);
                                        profile1.setTimer(String.valueOf(time));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setUserName(jsonObject1.getString("firstName"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        profile1.setUserName("");
                                    }
                                    try {
                                        profile1.setProfilePic(jsonObject1.getString("profile_pic"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    profile1.setImage(jsonObject1.getString("contentUrl"));
                                    try {
                                        profile1.setVideo(jsonObject1.getString("video_player_img"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setLikeCount(jsonObject1.getString("LikesCount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setCommentCount(jsonObject1.getString("CommentsCount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setGoldLike(jsonObject1.getInt("goldLikes"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setPostId(jsonObject1.getString("id"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setFavStatus(jsonObject1.getBoolean("FavouriteStatus"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setSubCatName(jsonObject1.getString("subCategoryName"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setSharedname(jsonObject1.getString("shared_username"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        profile1.setShareType(jsonObject1.getString("SharedType"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        String quoteType = jsonObject1.getString("quotetype");
                                        if (quoteType.equals("null")){
                                            profile1.setQuoteType("");
                                        }else {
                                            profile1.setQuoteType(quoteType);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        profile1.setLikeStatus(jsonObject1.getBoolean("LikeStatus"));
                                    } catch (Exception e) {

                                    }
                                    profile1.setHashtag(jsonObject1.getString("hashtag"));
                                    try {
                                        JSONArray jsonArray1 = jsonObject1.getJSONArray("answers");
                                        profile1.setLength(jsonArray1.length());
                                        for (int j = 0; j < jsonArray1.length(); j++) {
                                            JSONObject object = jsonArray1.getJSONObject(j);
                                            profile1.setAnswer(object.getString("comments"));
                                        }
                                    } catch (Exception e) {

                                    }
                                    profileList.add(profile1);
                                }
                            } catch (Exception e) {

                            }

                            if(pageCount == 1){
                                profileAdapter = new ProfileAdapter(getActivity(), profileList);
                                listView.setAdapter(profileAdapter);
                            }else {
                                profileAdapter.notifyDataSetChanged();
                            }

                            mProgressDialog.dismiss();


                        } else if (status == 0) {
                            mProgressDialog.dismiss();
                            Toast.makeText(getActivity(), "null", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressDialog.dismiss();
                    try {
                        Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        }else {
            Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
        }

    }


    private void dateFormat(long date) {

        //milliseconds
        long different = date;
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        if (different >= 86400000) {
            long str = different / daysInMilli;
            if (str > 365) {
                elapsed = str / 365;
                time = String.valueOf(elapsed + "yr ago");
            } else if (str > 31) {
                elapsed = str / 31;
                time = String.valueOf(elapsed + "mon ago");
            } else {
                elapsed = str;
                time = String.valueOf(elapsed + "d ago");
            }
        } else if (different >= 3600000) {
            elapsed = different / hoursInMilli;
            time = String.valueOf(elapsed + "h ago");
        } else if (different >= 60000) {
            elapsed = different / minutesInMilli;
            time = String.valueOf(elapsed + "min ago");
        } else if (different >= 6000) {
            elapsed = different / secondsInMilli;
            time = String.valueOf(elapsed + "s ago");
        } else {
            time = String.valueOf("0");
        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
