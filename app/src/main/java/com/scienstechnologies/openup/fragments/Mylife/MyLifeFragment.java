package com.scienstechnologies.openup.fragments.Mylife;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.MylifeAdapter;

/**
 * Created by Home on 3/11/2016.
 */
public class MyLifeFragment extends Fragment {
    LinearLayout write;
    ViewPager viewPager;
    FragmentTransaction fragmentTransaction;
    RadioGroup rg;
    RadioButton rb1,rb2,rb3,rb4;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mylife_activity, container, false);
        rg = (RadioGroup)v.findViewById(R.id.radiogroup);
        rb1 = (RadioButton)v.findViewById(R.id.radioButton1);
        rb2 = (RadioButton)v.findViewById(R.id.radioButton2);
        rb3 = (RadioButton)v.findViewById(R.id.radioButton3);
        rb4 = (RadioButton)v.findViewById(R.id.radioButton4);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        write = (LinearLayout)v.findViewById(R.id.ll_write);
        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                write.setBackgroundColor(Color.parseColor("#17a553"));
                fragmentTransaction = getFragmentManager().beginTransaction();
             //   fragmentTransaction.replace(R.id.container_mylife, new MylifeWriteFragment());
                fragmentTransaction.commit();
            }
        });

        FragmentManager fragmentManager = getFragmentManager();
        final MylifeAdapter myAdapter = new MylifeAdapter(fragmentManager);
        viewPager.setAdapter(myAdapter);

        final DetailOnPageChangeListener mDetailOnPageChangeListener = new DetailOnPageChangeListener();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position==0){
                    rg.check(R.id.radioButton1);
                }
                else if (position==1){
                    rg.check(R.id.radioButton2);
                }
                else if (position==2){
                    rg.check(R.id.radioButton3);
                }
                else if (position==3){
                    rg.check(R.id.radioButton4);
                }

            }

            public void onPageScrollStateChanged(int state) {

            }

        });

        return v;
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() +i;
    }

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        int position;
        public void run() {
            if( position >= 4){
                position = 0;
            }else{
                position = position+1;
            }
            viewPager.setCurrentItem(position, true);
            handler.postDelayed(runnable, 2000);
        }
    };
    public void onPause() {
        super.onPause();
        if (handler!= null) {
            handler.removeCallbacks(runnable);
        }
    }
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        handler.postDelayed(runnable, 2000);
    }



    public class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        private int currentPage;

        @Override
        public void onPageSelected(int position) {

        }

        public final int getCurrentPage() {
            return currentPage;
        }


        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }
    }
}
