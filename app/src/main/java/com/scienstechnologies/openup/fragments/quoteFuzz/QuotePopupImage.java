package com.scienstechnologies.openup.fragments.quoteFuzz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.fragments.other.AdminImagesFragment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Home on 3/17/2016.
 */
public class QuotePopupImage extends Fragment {
    private int PICK_IMAGE_REQUEST = 1;
    FragmentTransaction fragmentTransaction;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    Bitmap bitmap;
    Bundle b;
    ImageView create,upload;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.quote_popup_image, container, false);
        create = (ImageView)v.findViewById(R.id.iv_quote_creare_image);
        upload = (ImageView)v.findViewById(R.id.iv_quote_upload_image);
        b = new Bundle();
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Bundle b = new Bundle();
//                b.putString("imageType","2");
//                AdminImagesFragment adminImagesFragment = new AdminImagesFragment();
//                adminImagesFragment.setArguments(b);
//                fragmentTransaction = getFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.container_quote, adminImagesFragment);
//                fragmentTransaction.commit();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,
                        CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        return v;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                // convert byte array to Bitmap

                bitmap = BitmapFactory.decodeByteArray(byteArray, 0,
                        byteArray.length);

                create.setImageBitmap(bitmap);

                b.putParcelable("Bitmap", bitmap);

                QuoteImageFragment quoteImageFragment = new QuoteImageFragment();
                quoteImageFragment.setArguments(b);
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_quote,quoteImageFragment);
                fragmentTransaction.commit();


                //   Bundle bundle = new Bundle();
                // bundle.putParcelable("Bitmap", bitmap);

            }
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                b.putParcelable("Bitmap", bitmap);
                b.putString("titleId","1");
                // Log.d(TAG, String.valueOf(bitmap));
                upload.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }

            QuoteImageFragment quoteImageFragment = new QuoteImageFragment();
            quoteImageFragment.setArguments(b);
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_quote,quoteImageFragment);
            fragmentTransaction.commit();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
    }

}
