package com.scienstechnologies.openup.fragments.quoteFuzz;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.adapters.CategoriesAdapter;
import com.scienstechnologies.openup.models.CategoriesModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Santhu on 7/2/2016.
 */
public class QuoteListFragment extends Fragment {
    ProgressDialog mProgressDialog;
    ListView listView;
    private CategoriesAdapter categoriesAdapter;
    List<CategoriesModel> categoriesModelsList = new ArrayList<>();
    int id = 1;
    String quoteType;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_list_view, container, false);
        listView = (ListView)v.findViewById(R.id.cat_list);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        quoteType = getArguments().getString("quoteType");

        if (isNetworkConnected()) {
            mProgressDialog.show();

            String URL = StringConstants.BASEURL+"list_of_subcategories?CategoryId=6";

            StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            categoriesModelsList.clear();
                            JSONArray array = jsonObject.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                CategoriesModel categoriesModel = new CategoriesModel();
                                categoriesModel.setName(object.getString("SubcategoryName"));
                                categoriesModel.setId(object.getString("SubcategoryId"));
                                categoriesModel.setColor(object.getString("ColorCode"));

                                categoriesModelsList.add(categoriesModel);
                            }
                            categoriesAdapter = new CategoriesAdapter(getActivity(), categoriesModelsList, id, getFragmentManager(),quoteType);
                            listView.setAdapter(categoriesAdapter);
                            mProgressDialog.dismiss();
                        } else if (status == 0) {
                            Toast.makeText(getActivity(), "null", Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    try {
                        Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mProgressDialog.dismiss();

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        } else {
            Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
        }
        return v;
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
