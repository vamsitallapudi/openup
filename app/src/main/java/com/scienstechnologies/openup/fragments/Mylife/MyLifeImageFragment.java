package com.scienstechnologies.openup.fragments.Mylife;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.fragments.other.AdminImagesFragment;
import com.scienstechnologies.openup.others.StringConstants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dell on 4/23/2016.
 */
public class MyLifeImageFragment extends Fragment {
    ProgressDialog mProgressDialog;
    final static int REQUEST_VIDEO_CAPTURED = 1;
    private int PICK_IMAGE_REQUEST = 1;
    FragmentTransaction fragmentTransaction;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    private static final String IMAGE_UPLOAD_URL = StringConstants.BASEURL + "MyLifesend";
    ImageView create, upload;
    Bitmap bitmap;
    Bundle b;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mylife_image, container, false);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        create = (ImageView) v.findViewById(R.id.iv_create_mylife);
        upload = (ImageView) v.findViewById(R.id.iv_upload_mylife);
        b = new Bundle();
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Bundle b = new Bundle();
//                b.putString("imageType","3");
//                AdminImagesFragment adminImagesFragment = new AdminImagesFragment();
//                adminImagesFragment.setArguments(b);
//                fragmentTransaction = getFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.container_mylife, adminImagesFragment);
//                fragmentTransaction.commit();

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,
                        CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {

            //  public static final int ACTION_REQUEST_GALLERY = 1888;

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
// Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }

        });
        return v;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                // convert byte array to Bitmap

                bitmap = BitmapFactory.decodeByteArray(byteArray, 0,
                        byteArray.length);

                create.setImageBitmap(bitmap);

                b.putParcelable("Bitmap", bitmap);

                Mylifecropimage mylifecropimage = new Mylifecropimage();
                mylifecropimage.setArguments(b);
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_mylife, mylifecropimage);
                fragmentTransaction.commit();


                //   Bundle bundle = new Bundle();
                // bundle.putParcelable("Bitmap", bitmap);

            }
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                b.putParcelable("Bitmap", bitmap);
                // Log.d(TAG, String.valueOf(bitmap));
                upload.setImageBitmap(bitmap);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] byteImage = baos.toByteArray();
                String encodedImage = Base64.encodeToString(byteImage, Base64.DEFAULT);

                postImageToServer(encodedImage);

            } catch (IOException e) {
                e.printStackTrace();
            }

            Mylifecropimage mylifecropimage = new Mylifecropimage();
            mylifecropimage.setArguments(b);
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_mylife, mylifecropimage);
            fragmentTransaction.commit();
        }

    }

    private void postImageToServer(final String encImage) {


        StringRequest sr = new StringRequest(Request.Method.POST, IMAGE_UPLOAD_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> paramMap = new HashMap<>();
                paramMap.put("image", encImage);
                paramMap.put("subcatid", "");
                paramMap.put("catid", "4");
                paramMap.put("userid", "");
                paramMap.put("posttype", "");
                paramMap.put("title", "");

                return paramMap;
            }
        };

        AppController.getInstance().addToRequestQueue(sr);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


}
