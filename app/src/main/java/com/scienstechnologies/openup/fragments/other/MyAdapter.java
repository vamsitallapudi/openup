package com.scienstechnologies.openup.fragments.other;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

/**
 * Created by Sciens on 5/16/2016.
 */
public class MyAdapter extends FragmentStatePagerAdapter{
    public MyAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);

    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        return super.instantiateItem(container, position);
    }

    @Override
    public Fragment getItem(int position) {
        return ProfileSlideFragment.newInstance(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return 2;
    }
}
