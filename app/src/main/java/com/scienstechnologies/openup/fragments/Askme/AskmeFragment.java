package com.scienstechnologies.openup.fragments.Askme;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.others.CustomDialogActivity;

/**
 * Created by Home on 3/14/2016.
 */
public class AskmeFragment extends  android.support.v4.app.Fragment {
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    TextView submit;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_askme, container, false);
        submit = (TextView)v.findViewById(R.id.ask_me);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.askme_container, new AskmeWriteFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return v;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!(getActivity().getSharedPreferences("visiteStatus", Context.MODE_PRIVATE)).getBoolean("ask",false)){
            Intent i = new Intent(getActivity(), CustomDialogActivity.class);
            i.putExtra("title","Ask Me");
            i.putExtra("des",getActivity().getResources().getString(R.string.askdes));
            i.putExtra("modaltype","ask");
            getActivity().startActivity(i);
        }
    }
}
