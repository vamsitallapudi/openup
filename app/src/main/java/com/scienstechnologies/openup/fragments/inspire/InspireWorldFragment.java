package com.scienstechnologies.openup.fragments.inspire;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.scienstechnologies.openup.R;

/**
 * Created by Home on 3/16/2016.
 */
public class InspireWorldFragment extends Fragment {
    FragmentTransaction fragmentTransaction;
    ImageView write,image,video,help;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_inspire, container, false);

        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, new InspireFragment());
        fragmentTransaction.commit();

        write = (ImageView)v.findViewById(R.id.image_write);
        image = (ImageView)v.findViewById(R.id.tv_inspire);
        video = (ImageView)v.findViewById(R.id.video_inspire);
        help = (ImageView)v.findViewById(R.id.iv_help);

        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new InspirePopup());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new InspireImageFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new InspireSelectVideoFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return v;
    }
}
