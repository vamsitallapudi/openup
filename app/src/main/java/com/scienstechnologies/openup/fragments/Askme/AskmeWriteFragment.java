package com.scienstechnologies.openup.fragments.Askme;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.MainActivity;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dell on 4/23/2016.
 */
public class AskmeWriteFragment extends android.support.v4.app.Fragment {
    ProgressDialog mProgressDialog;
    Button button;
    String userId;
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    private static final String URL = StringConstants.BASEURL + "insert_post_data";
    private static final String TAG = AskmeWriteFragment.class.getSimpleName();
    EditText editText;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.ask_me_submit, container, false);
        editText = (EditText) v.findViewById(R.id.et_count);
        button = (Button) v.findViewById(R.id.button_submit);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Write a comment...", Toast.LENGTH_LONG).show();
                    //mEditText.setError("Name cannot be Blank");
                } else {

                    if (isNetworkConnected()) {
                        mProgressDialog.show();
                        StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d(TAG, response);
                                try {
                                    JSONObject jsonobject = new JSONObject(response);
                                    Log.d(TAG, response);
                                    int status = jsonobject.getInt("status");
                                    if (status== 1) {
                                        String message = jsonobject.get("message").toString();
                                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        getActivity().finishAffinity();
                                        mProgressDialog.dismiss();
                                        Intent i =new Intent(getActivity(),MainActivity.class);
                                        startActivity(i);
                                    } else if (status== 2) {
                                        String message = jsonobject.getString("message");
                                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        mProgressDialog.dismiss();

                                    }
                                } catch (JSONException e) {

                                    e.printStackTrace();
                                    Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                                    mProgressDialog.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d(TAG, "Error: " + error.getMessage());
                                mProgressDialog.dismiss();
                            }
                        }) {
                            protected Map<String, String> getParams() {
                                Map<String, String> map = new HashMap<>();

                                map.put("userid", userId);
                                map.put("catid", "3");
                                map.put("posttype", "1");
                                map.put("content", editText.getText().toString());
                                return map;
                            }
                        };
                        AppController.getInstance().addToRequestQueue(sr);
                    } else {
                        Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
                    }
                }

//                fragmentTransaction = getFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.askme_container, new AskmeFragment());
//                fragmentTransaction.commit();
            }
        });
        return v;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
