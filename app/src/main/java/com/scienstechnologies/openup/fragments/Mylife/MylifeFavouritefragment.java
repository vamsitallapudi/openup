package com.scienstechnologies.openup.fragments.Mylife;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.adapters.MylifeFavAdapter;
import com.scienstechnologies.openup.adapters.NotificationAdapter;
import com.scienstechnologies.openup.models.Mylifefavmodel;
import com.scienstechnologies.openup.models.NotificationModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 5/16/2016.
 */
public class MylifeFavouritefragment extends Fragment {
    List<Mylifefavmodel> mylifefavmodels = new ArrayList<>();
    private MylifeFavAdapter mylifeFavAdapter;
    private ListView listView;
    String[] fav = new String[] {"Sruthi"};
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mylife_favourite, container, false);

        listView = (ListView)v.findViewById(R.id.lv_mylife_favourite);
        for (int i=0;i<fav.length;i++){

            Mylifefavmodel mylifefavmodel = new Mylifefavmodel();
            mylifefavmodel.setUserName(fav[i]);
            mylifefavmodels.add(mylifefavmodel);
        }
        mylifeFavAdapter = new MylifeFavAdapter(getActivity(),mylifefavmodels);
        listView.setAdapter(mylifeFavAdapter);
        mylifeFavAdapter.notifyDataSetChanged();
        return v;
    }
}
