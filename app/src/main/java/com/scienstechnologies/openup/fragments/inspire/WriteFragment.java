package com.scienstechnologies.openup.fragments.inspire;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.MainActivity;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Santhu on 3/3/2016.
 */
public class WriteFragment extends Fragment {
    String userId;
    ProgressDialog mProgressDialog;
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    private TextView sms_count, get_category;
    TextInputLayout text;
    private EditText mEditText,title;
    ImageView down;
    private Button submit;
    String catId;


    private static final String URL = StringConstants.BASEURL + "insert_post_data";
    private static final String TAG = WriteFragment.class.getSimpleName();

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen11, container, false);


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);

        mEditText = (EditText) v.findViewById(R.id.et_count);
        title = (EditText)v.findViewById(R.id.et_title);
        text = (TextInputLayout)v.findViewById(R.id.textContainer1);
        get_category = (TextView) v.findViewById(R.id.tv_get_category);
        down = (ImageView)v.findViewById(R.id.iv_down);
        String value = getArguments().getString("life_key");
        String name = getArguments().getString("name_key");
        if (value.equals("mywords")){
            catId = String.valueOf("0");
            get_category.setVisibility(View.GONE);
            down.setVisibility(View.GONE);
            text.setVisibility(View.VISIBLE);

        }else {
            get_category.setVisibility(View.VISIBLE);
            get_category.setText(name);
            text.setVisibility(View.GONE);
            down.setVisibility(View.VISIBLE);
            catId = value;
        }
        submit = (Button) v.findViewById(R.id.button_submit_write);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEditText.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Write a comment...", Toast.LENGTH_LONG).show();
                }
                else {
                    if (isNetworkConnected()) {
                        mProgressDialog.show();
                        StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d(TAG, response);
                                try {
                                    JSONObject jsonobject = new JSONObject(response);
                                    Log.d(TAG, response);
                                    String status = jsonobject.getString("status");
                                    if (status.equals("1")) {
                                        String message = jsonobject.getString("message");
                                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        getActivity().finishAffinity();
                                        mProgressDialog.dismiss();
                                        Intent i =new Intent(getActivity(),MainActivity.class);
                                        startActivity(i);

                                    } else if (status.equals("0")) {
                                        String message = jsonobject.getString("message");
                                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        mProgressDialog.dismiss();
                                        // Toast.makeText(getActivity(), "message", Toast.LENGTH_LONG).show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressDialog.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d(TAG, "Error: " + error.getMessage());
                                mProgressDialog.dismiss();
                            }
                        }) {
                            protected Map<String, String> getParams() {
                                Map<String, String> map = new HashMap<>();
//                                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
//                                String userId = sharedPreferences.getString("userid", "");
                                map.put("userid", userId);
                                map.put("catid", "1");
                                map.put("subcatid", catId);
                                map.put("posttype", "1");
                                //   map.put("title","hi");
                                map.put("content", mEditText.getText().toString());
                                map.put("title",title.getText().toString());
                                return map;
                            }
                        };
                        AppController.getInstance().addToRequestQueue(sr);
                    } else {
                        Toast.makeText(getActivity(), "Please connect to Network!", Toast.LENGTH_LONG).show();
                    }
                }
//                fragmentTransaction = getFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.inspire_container, new InspireFragment());
//                fragmentTransaction.commit();
            }
        });

        return v;
    }

    public void onResume() {
        super.onResume();
        mEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                Log.w("onTextChanged", "Here");
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }

}
