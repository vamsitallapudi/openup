package com.scienstechnologies.openup.fragments.other;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.adapters.GroupItemAdapter;
import com.scienstechnologies.openup.models.GroupModel;
import com.scienstechnologies.openup.others.CustomDialogActivity;
import com.scienstechnologies.openup.others.ScrollViewExt;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home on 3/10/2016.
 */
public class GroupFragment extends Fragment {
    private ListView listView;
    private GroupItemAdapter groupItemAdapter;
    private List<GroupModel> groupModelList = new ArrayList<>();
    ProgressDialog mProgressDialog;
    private static final String URL = StringConstants.BASEURL+"DiamondClub";
    ImageView help;
    ScrollViewExt scrollView;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_group, container, false);
        listView = (ListView) v.findViewById(R.id.lv_group);
        scrollView = (ScrollViewExt)v.findViewById(R.id.group_scrol);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        help = (ImageView) v.findViewById(R.id.iv_help);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), CustomDialogActivity.class);
                i.putExtra("title","Diamond Club");
                i.putExtra("des",getResources().getString(R.string.diamonddes));
                i.putExtra("modaltype","diamond");
                startActivity(i);
            }
        });

        getPostDataFromNetwork();
        return v;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!(getActivity().getSharedPreferences("visiteStatus", Context.MODE_PRIVATE)).getBoolean("diamond",false)){
            Intent i = new Intent(getActivity(), CustomDialogActivity.class);
            i.putExtra("title","Diamond Club");
            i.putExtra("des",getActivity().getResources().getString(R.string.diamonddes));
            i.putExtra("modaltype","diamond");
            getActivity().startActivity(i);
        }
    }

    private void getPostDataFromNetwork() {

        StringRequest sr = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");

                    if (status == 1) {

                        JSONArray array = jsonObject.getJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);

                            GroupModel groupModel = new GroupModel();
                            try{
                                groupModel.setProfilePic(object.getString("profile_pic"));
                            }catch (Exception e){

                            }

                            groupModel.setName(object.getString("Name"));
                            String value = object.getString("GoldMedals");

                            try{

                                if (value.equals("null")){
                                    groupModel.setScore("0");
                                }else {
                                    groupModel.setScore(value);
                                }

                            }catch (Exception e){

                            }

                            groupModel.setUserId(object.getString("UserID"));
                            groupModelList.add(groupModel);
                        }
                        groupItemAdapter = new GroupItemAdapter(getActivity(), groupModelList,getFragmentManager());
                        listView.setAdapter(groupItemAdapter);

                        setListViewHeightBasedOnChildren(listView);
                        scrollView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                    scrollView.fullScroll(View.FOCUS_UP);
                                    scrollView.scrollTo(0, 0);

                                mProgressDialog.dismiss();
                            }
                        }, 1000);
                    } else if (status == 0) {

                        Toast.makeText(getActivity(), "null", Toast.LENGTH_LONG).show();
                        mProgressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    mProgressDialog.dismiss();
                    Toast.makeText(getActivity(), "json exception", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
        AppController.getInstance().addToRequestQueue(sr);
    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + totalHeight;
        listView.setLayoutParams(params);

    }

}
