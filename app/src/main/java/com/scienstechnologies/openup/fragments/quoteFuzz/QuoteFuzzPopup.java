package com.scienstechnologies.openup.fragments.quoteFuzz;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.scienstechnologies.openup.R;
import com.scienstechnologies.openup.activities.AppController;
import com.scienstechnologies.openup.adapters.CategoriesAdapter;
import com.scienstechnologies.openup.models.CategoriesModel;
import com.scienstechnologies.openup.others.StringConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home on 3/10/2016.
 */
public class QuoteFuzzPopup extends Fragment {
    ProgressDialog mProgressDialog;
    FragmentTransaction fragmentTransaction;
    private TextView without_ctegory, category, myQuote, inspireQuote, go;
    ImageView close;
    String catID,quoteType;
    Bundle args;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.quotefuzz_popup, container, false);
        close = (ImageView) v.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading..");
        mProgressDialog.setCancelable(false);
        args = new Bundle();

        go = (TextView)v.findViewById(R.id.inspir_go_tv);
        go.setVisibility(View.GONE);
        without_ctegory = (TextView) v.findViewById(R.id.tv_myquote);
        category = (TextView) v.findViewById(R.id.tv_inspire_quote);
        myQuote = (TextView) v.findViewById(R.id.my_quote_tv);
        inspireQuote = (TextView) v.findViewById(R.id.inspire_quote_tv);

        myQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go.setVisibility(View.GONE);
                quoteType = "0";
                category.setVisibility(View.VISIBLE);
                without_ctegory.setVisibility(View.VISIBLE);
                myQuote.setBackgroundColor(getResources().getColor(R.color.color_quote_fuzz));
                inspireQuote.setBackgroundColor(getResources().getColor(R.color.color_quote_unselected));
            }
        });
        inspireQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go.setVisibility(View.GONE);
                quoteType = "1";
                category.setVisibility(View.VISIBLE);
                without_ctegory.setVisibility(View.VISIBLE);
                inspireQuote.setBackgroundColor(getResources().getColor(R.color.color_quote_fuzz));
                myQuote.setBackgroundColor(getResources().getColor(R.color.color_quote_unselected));
            }
        });
        without_ctegory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go.setVisibility(View.VISIBLE);

            }
        });
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go.setVisibility(View.GONE);
                QuoteListFragment quoteListFragment = new QuoteListFragment();
                args.putString("quoteType",quoteType);
                quoteListFragment.setArguments(args);
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_quote, quoteListFragment);
                fragmentTransaction.commit();
            }
        });

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catID = String.valueOf("0");
                QuoteWriteFragment quoteWriteFragment = new QuoteWriteFragment();
                args.putString("catId", catID);
                args.putString("quoteType",quoteType);
                quoteWriteFragment.setArguments(args);
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_quote, quoteWriteFragment);
                fragmentTransaction.commit();
            }
        });
        return v;
    }
}
